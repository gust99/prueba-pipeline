FROM node:16.13.1-alpine
WORKDIR /usr/src/app
COPY package.json /usr/src/app
COPY tsconfig.json /usr/src/app
COPY src /usr/src/app/src
RUN npm install
RUN npm run build

FROM node:16.13.1-alpine
WORKDIR /usr/src/app
COPY --from=0 /usr/src/app/dist .
COPY --from=0 /usr/src/app/node_modules ./node_modules
ARG ENV_FILE
ENV ENV_FILE=${ENV_FILE}
#COPY .env .env
RUN npm install pm2 -g
EXPOSE 80
CMD ["pm2-runtime","app.js"]
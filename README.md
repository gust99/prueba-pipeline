# backend

# src

Code which will go to production.

## Contexts

This folder will contains one folder per Bounded Context.
Each BC´s folder will have:

1. **Domain**:
   Contains the models of my business.
2. **Services**:
   Contains the domain events.
3. **Repository**:
   Contains the business logic, it won´t change if persisntency layer change.
4. **Persistence**:
   Contains the database relevant to the BC (mongodb, sql, etc); could be an entirely DB or a table of one main DB.
5. **Presentation**:
   Contains the REST API / endpoints relevant to the BC; could be an entirely server or a consolidate route of one main server.

## Shared

This folder contains common elements which will be used by many BC´s.

---

# tests

This folder replies src structure, to have a more clear localization of every component tested.

## Contexts

Replies the same structure of src contexts.

## Shared

Aceptation Test, E2E test, black box test.

Group test, this won´t go to production.

---

# Run de app in development mode

## Local Database with Docker

One way to run the local database is with Docker and the following tools are required:
* Docker
* Docker compose

To run the MySQL container just change the environment variables `MYSQL_ROOT_PASSWORD` and `MYSQL_DATABASE` of the file `local_db.yaml`, also you can configure the local port and container port in the section `ports`
on the same file.


After set up the variable just run the following command in the `local_db.yaml` file location:

```bash
$ docker-compose -f local_db.yaml up -d
```

## Run the project in dev mode

Before to tun the project be sure to set up the variables `DB_NAME`, `DB_USER`, and `DB_PASSWORD` according 
your database instance in the file `config/index.ts`.

To run the project in development mode just enter the following command:

```bash
$ npm run start:dev
```


import 'reflect-metadata';
import { container } from './container';
import { IServer } from './server';
import { IOrm } from './contexts/shared/repository/IOrm';
import { TYPES } from './types';

const start = async () => {
  const server: IServer = container.get<IServer>(TYPES.Server);
  await container.get<IOrm>(TYPES.ORM).initialize();
  return server.start();
};

start();

import { IMAGE_TYPES } from '../shared/cloudinary/types';
import { PROFILE_TYPES } from '../contexts/userManagement/types';
import { PUBLICATION_TYPES } from '../contexts/publicationManagement/types';
import { EMAIL_TYPES } from '../shared/emailSender/types';
import { AUTHENTICATION_TYPES } from '../contexts/accessManagement/types';
import { SEARCH_TYPES } from '../contexts/searchManagement/types';
import { SERVICE_TYPES } from '../contexts/serviceManagement/types';
import { SERVICEPROPERTY_TYPES } from '../contexts/serviceProperty/types';
import { REVIEW_TYPES } from '../contexts/communityManagement/types';

const TYPES = {
  Server: Symbol.for('Server'),
  CryptoService: Symbol.for('CryptoService'),
  Lodash: Symbol.for('Lodash'),
  ORM: Symbol.for('Orm'),
  ...PROFILE_TYPES,
  ...IMAGE_TYPES,
  ...PUBLICATION_TYPES,
  ...EMAIL_TYPES,
  ...AUTHENTICATION_TYPES,
  ...SEARCH_TYPES,
  ...SERVICE_TYPES,
  ...SERVICEPROPERTY_TYPES,
  ...REVIEW_TYPES
};

export { TYPES };

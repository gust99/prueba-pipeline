import path from 'path';
import * as dotenv from 'dotenv';

dotenv.config();
export default {
  API_PORT: process.env.API_PORT || 3000,
  DB_NAME: process.env.DB_NAME || 'iMentor',
  DB_USER: process.env.DB_USER || 'root',
  DB_PASSWORD: process.env.DB_PASSWORD || 'root',
  DB_HOST: process.env.DB_HOST || 'localhost',
  DB_PORT: parseInt(process.env.DB_PORT) || 3306,
  DB_TYPE: process.env.DB_TYPE || 'mysql',
  DB_ENTITIES: [path.join(__dirname, '../contexts/**/repository/*.entity.js')],
  DB_SYNCRO: true,
  EMAIL_CLIENT_ID:
    '1011128691068-0mr19dcloanlucs7bj6g299d7nnd2shb.apps.googleusercontent.com',
  EMAIL_CLIENT_SECRET: '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',
  EMAIL_REDIRECT_URI: 'https://developers.google.com/oauthplayground',
  EMAIL_REFRESH_TOKEN:
    '1//04bF1JHlH1BSFCgYIARAAGAQSNwF-L9Iruk2C5F2ESC1fSwjoLSG0tWbvpgLfvUSBxrioPcUnpJwLodw9Q2bbylE60nKpbDFgEBU',
  EMAIL: 'iMentor <imentorjala@gmail.com>',
  EMAIL_USER: 'imentorjala@gmail.com',
  ELASTIC_SEARCH_HOST: 'ec2-13-59-228-191.us-east-2.compute.amazonaws.com:9200'
};

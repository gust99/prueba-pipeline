const SERVICE_TYPES = {
  ServiceManagementController: Symbol.for('ServiceManagementController'),
  ServiceManagementService: Symbol.for('ServiceManagementService'),
  ServiceRepository: Symbol.for('ServiceRepository'),
  Lodash: Symbol.for('Lodash')
};

export { SERVICE_TYPES };

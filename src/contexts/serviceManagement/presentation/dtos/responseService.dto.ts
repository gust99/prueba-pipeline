export class ResponseServiceDTO {
  id: string;
  serviceTitle: string;
  serviceDescription: string;
  serviceImage?: string;
  mentorId: string;
  categories: string[];
  isDeleted?: boolean;
  serviceRate?: number;
  serviceNumOfReviews?: number;
}

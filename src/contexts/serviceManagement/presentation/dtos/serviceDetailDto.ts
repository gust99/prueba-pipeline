export interface ServiceDetailDTO {
  serviceId: string;
  userId: string;
  publicationId: string;
  serviceTitle: string;
  userName: string;
  userProfileImage: string;
  userRol: string;
  serviceImage: string;
  serviceDescription: string;
  serviceContent?: string;
  aboutTheMentor: string;
  hourlyRate: string;
  userRate: number;
  userNumOfReviews: number;
  serviceRate: number;
  serviceNumOfReviews: number;
}

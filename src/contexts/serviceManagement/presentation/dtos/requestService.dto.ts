class RequestServiceDTO {
  serviceTitle: string;
  serviceDescription: string;
  serviceImage?: string;
  mentorId: string;
  categories: [];
  isDeleted?: boolean;
}

export { RequestServiceDTO };

import {
  controller,
  httpGet,
  BaseHttpController,
  httpPost,
  httpPut,
  httpDelete,
  request,
  response,
  requestParam
} from 'inversify-express-utils';
import { Response, Request } from 'express';

import { inject } from 'inversify';
import { SERVICE_TYPES } from '../types';
import { ServiceManagementService } from '../services/serviceManagementServices';
import { Service } from '../domain/service';
import { RequestServiceDTO } from './dtos/requestService.dto';
import { plainToInstance } from 'class-transformer';
import { ObjectType } from 'typeorm';

@controller('/services')
export class ServiceManagementController extends BaseHttpController {
  private entityType: ObjectType<Service>;
  constructor(
    @inject(SERVICE_TYPES.ServiceManagementService)
    private serviceManagementService: ServiceManagementService
  ) {
    super();
  }

  @httpGet('/')
  private async getServices(req: Request, res: Response) {
    const response = await this.serviceManagementService.findAll();
    res.status(response.statusCode).json(response.responseContent);
  }

  /*@httpGet('/:id')
  private async getServicesByUser(
    @requestParam('id') id: string,
    @request() req: Request,
    @response() res: Response
  ) {
    const response = await this.serviceManagementService.findByUser(id);
    res.status(response.statusCode).json(response.responseContent);
  }*/

  @httpPost('/')
  private async createService(
    @request() req: Request,
    @response() res: Response
  ) {
    const response = await this.serviceManagementService.save(
      plainToInstance(RequestServiceDTO, req.body)
    );
    res.status(response.statusCode).json(response.responseContent);
  }

  // @httpGet('/:id')
  // private async getService(
  //   @requestParam('id') id: string,
  //   @response() res: Response
  // ) {
  //   const response = await this.serviceManagementService.findOneById(id);
  //   res.status(response.statusCode).json(response.responseContent);
  // }

  @httpGet('/user/:id')
  private async getServicesByUser(
    @requestParam('id') id: string,
    @request() req: Request,
    @response() res: Response
  ) {
    const response = await this.serviceManagementService.findByUser(id);
    res.status(response.statusCode).json(response.responseContent);
  }

  @httpPut('/:id')
  private async update(
    @requestParam('id') id: string,
    @request() req: Request,
    @response() res: Response
  ) {
    const response = await this.serviceManagementService.update(
      id,
      plainToInstance(RequestServiceDTO, req.body)
    );
    res.status(response.statusCode).json(response.responseContent);
  }

  @httpDelete('/:id')
  private async delete(
    @requestParam('id') id: string,
    @response() res: Response
  ) {
    const response = await this.serviceManagementService.delete(id);
    res.status(response.statusCode).json(response.responseContent);
  }

  @httpGet('/detail/:id')
  private async getServiceDetail(
    @requestParam('id') id: string,
    @response() res: Response
  ) {
    const response = await this.serviceManagementService.getServiceDetail(id);
    res.status(response.statusCode).json(response.responseContent);
  }
}

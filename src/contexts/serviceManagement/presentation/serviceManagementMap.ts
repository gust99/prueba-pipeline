import { UniqueEntityID } from '../../shared/domain/uniqueEntityID';
import { UserId } from '../../userManagement/domain/userId';
import { ServiceId } from '../domain/serviceId';
import { Image } from '../domain/valueObjects/image';
import { Service } from '../domain/service';
import { ServiceDescription } from '../domain/valueObjects/serviceDescription';
import { ServiceTitle } from '../domain/valueObjects/serviceTitle';
import { ResponseServiceDTO } from './dtos/responseService.dto';

import { ServiceEntity } from '../repository/service.entity';
import { IsDeleted } from '../domain/valueObjects/isDeleted';

export class ServiceMap {
  static toPersistence(service: Service): ServiceEntity {
    const entity = new ServiceEntity();
    entity.serviceId = service.serviceId.id.toString();
    entity.serviceTitle = service.serviceTitle.value;
    entity.serviceDescription = service.serviceDescription.value;
    entity.mentorId = service.mentorId.id.toString();
    entity.serviceImage = service.serviceImage.value;
    entity.isDeleted = service.isDeleted;
    entity.categories = service.categories.toString();
    return entity;
  }
  static toDTO(data: Service): ResponseServiceDTO {
    const service = new ResponseServiceDTO();
    service.id = data.serviceId.id.toString();
    service.serviceTitle = data.serviceTitle.value;
    service.serviceDescription = data.serviceDescription.value;
    service.serviceImage = data.serviceImage.value;
    service.isDeleted = data.isDeleted;
    service.mentorId = data.mentorId.id.toString();
    service.serviceRate = data.serviceRate;
    service.serviceNumOfReviews = data.serviceNumOfReviews;
    return service;
  }
  static toDomain(data: any): Service {
    const title = ServiceTitle.create({ value: data.serviceTitle });
    const description = ServiceDescription.create({
      value: data.serviceDescription
    });
    const mentorId = UserId.create(new UniqueEntityID(data.mentorId));
    const image = Image.create({ value: data.serviceImage });
    const isDeleted = IsDeleted.create({ value: data.isDeleted });

    const service = Service.create(
      {
        serviceTitle: title.getValue() as ServiceTitle,
        serviceDescription: description.getValue() as ServiceDescription,
        mentorId: mentorId.getValue(),
        serviceImage: image.getValue() as Image,
        isDeleted: isDeleted.getValue() as boolean,
        serviceRate: data.serviceRate,
        serviceNumOfReviews: data.serviceNumOfReviews
      },
      new UniqueEntityID(data.serviceId)
    );

    return service.getValue();
  }
}

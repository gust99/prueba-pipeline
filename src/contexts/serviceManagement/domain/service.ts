import { AggregateRoot } from '../../shared/domain/agregateRoot';
import { UniqueEntityID } from '../../shared/domain/uniqueEntityID';
import { Result } from '../../shared/domain/result';
import { Guard, IGuardArgument } from '../../shared/domain/guard';
import { PropertyError } from '../../shared/domain/propertyError';
import { ServiceId } from './serviceId';
import { UserId } from '../../userManagement/domain/userId';
import { Image } from './valueObjects/image';
import { ServiceTitle } from './valueObjects/serviceTitle';
import { ServiceDescription } from './valueObjects/serviceDescription';
import { Categories } from './valueObjects/categories';

interface ServiceProps {
  serviceTitle: ServiceTitle;
  serviceDescription: ServiceDescription;
  serviceImage?: Image;
  categories?: Categories;
  mentorId: UserId;
  isDeleted?: boolean;
  serviceRate?: number;
  serviceNumOfReviews?: number;
}

export class Service extends AggregateRoot<ServiceProps> {
  public static entityName = 'service';

  get serviceId(): ServiceId {
    return ServiceId.create(this._id).getValue();
  }

  get serviceTitle(): ServiceTitle {
    return this.props.serviceTitle;
  }

  get serviceDescription(): ServiceDescription {
    return this.props.serviceDescription;
  }

  get serviceImage(): Image {
    return this.props.serviceImage;
  }
  get categories(): Categories {
    return this.props.categories;
  }

  get mentorId(): UserId {
    return this.props.mentorId;
  }

  get isDeleted(): boolean {
    return this.props.isDeleted;
  }

  get serviceRate(): number {
    return this.props.serviceRate;
  }

  get serviceNumOfReviews(): number {
    return this.props.serviceNumOfReviews;
  }

  public updateTitle(title: ServiceTitle) {
    this.props.serviceTitle = title;
    return Result.ok<void>();
  }
  public updateDescription(description: ServiceDescription) {
    this.props.serviceDescription = description;
    return Result.ok<void>();
  }
  public updateImage(serviceImage: Image) {
    this.props.serviceImage = serviceImage;
    return Result.ok<void>();
  }
  public updateMentorId(mentorId: UserId) {
    this.props.mentorId = mentorId;
    return Result.ok<void>();
  }
  public updateCategories(categories: Categories) {
    this.props.categories = categories;
    return Result.ok<void>();
  }

  public updateIsDeleted(isDeleted: boolean) {
    this.props.isDeleted = isDeleted;
    return Result.ok<void>();
  }

  public updateServiceRate(serviceRate: number) {
    this.props.serviceRate = serviceRate;
    return Result.ok<void>();
  }

  public updateServiceNumOfReviews(serviceNumOfReviews: number) {
    this.props.serviceNumOfReviews = serviceNumOfReviews;
    return Result.ok<void>();
  }

  private constructor(props: ServiceProps, id?: UniqueEntityID) {
    super(props, id);
  }
  public delete(): void {
    if (!this.props.isDeleted) {
      this.props.isDeleted = true;
    }
  }

  public static create(props: ServiceProps, id?: UniqueEntityID): Result<any> {
    const guardArgs: IGuardArgument[] = [
      { argument: props.serviceTitle, argumentName: 'serviceTitle' },
      {
        argument: props.serviceDescription,
        argumentName: 'serviceDescription'
      },
      { argument: props.mentorId, argumentName: 'mentorId' }
    ];
    const guardResult = Guard.againstNullOrUndefinedBulk(guardArgs);

    if (!guardResult.succeeded) {
      //publicationErrors.errors = guardResult.errors;
      return Result.fail(guardResult.errors);
    }
    const defaultValues: ServiceProps = {
      ...props,
      isDeleted: props.isDeleted ? props.isDeleted : false,
      serviceImage: props.serviceImage
        ? props.serviceImage
        : (Image.create({ value: '' }).getValue() as Image),
      categories: props.categories ? props.categories : Categories.create([]),
      serviceRate: props.serviceRate ? props.serviceRate : 0,
      serviceNumOfReviews: props.serviceNumOfReviews
        ? props.serviceNumOfReviews
        : 0
    };

    const isNewService = !!id === false;

    if (isNewService) {
      const service = new Service(defaultValues);
      return Result.ok<Service>(service);
    }

    const service = new Service(defaultValues, id);
    return Result.ok<Service>(service);
  }
}

import { Guard } from '../../../shared/domain/guard';
import {
  PropertyError,
  ErrorHelper
} from '../../../shared/domain/propertyError';
import { Result } from '../../../shared/domain/result';
import { ValueObject } from '../../../shared/domain/valueObject';

interface ServiceDescriptionProps {
  value: string;
}
export class ServiceDescription extends ValueObject<ServiceDescriptionProps> {
  public static minLength = 20;
  public static maxLength = 400;
  public static propName = 'serviceDescription';
  private constructor(props: ServiceDescriptionProps) {
    super(props);
  }

  get value(): string {
    return this.props.value;
  }

  public static create(
    props: ServiceDescriptionProps
  ): Result<ServiceDescription | PropertyError> {
    const descriptionErrors: PropertyError = {
      property: this.propName,
      value: { value: props.value },
      error: []
    };

    const nullGuardResult = Guard.againstNullOrUndefined(
      props.value,
      this.propName
    );

    if (!nullGuardResult.succeeded) {
      descriptionErrors.value = null;
      descriptionErrors.error.push(nullGuardResult.message);
      return Result.fail<PropertyError>(descriptionErrors);
    }

    const minGuardResult = Guard.againstAtLeast(
      this.minLength,
      props.value,
      this.propName
    );
    const maxGuardResult = Guard.againstAtMost(
      this.maxLength,
      props.value,
      this.propName
    );

    if (!minGuardResult.succeeded)
      descriptionErrors.error.push(minGuardResult.message);

    if (!maxGuardResult.succeeded)
      descriptionErrors.error.push(maxGuardResult.message);

    if (ErrorHelper.propertyHasErrors(descriptionErrors))
      return Result.fail<PropertyError>(descriptionErrors);

    return Result.ok<ServiceDescription>(new ServiceDescription(props));
  }
}

import { WatchedList } from '../../../shared/domain/watchedList';
import { Category } from './category';

export class Categories extends WatchedList<Category> {
  private constructor(initialCategories: Category[]) {
    super(initialCategories);
  }

  toString(): string {
    return Categories.parseToString(this);
  }

  public compareItems(a: Category, b: Category): boolean {
    return a.equals(b);
  }
  public static parseToString(categories: Categories): string {
    let result = '';
    for (const iterator of categories.getItems()) {
      result += iterator.value + ',';
    }
    if (result != '') return result.slice(0, -1);
    return result;
  }

  public static parseToArray(categories: Categories): string[] {
    const result = [];
    for (const iterator of categories.getItems()) {
      result.push(iterator.value);
    }
    return result;
  }

  public static create(initialCategories?: Category[]): Categories {
    return new Categories(initialCategories ? initialCategories : []);
  }
}

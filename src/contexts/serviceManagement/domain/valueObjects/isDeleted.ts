import { Guard } from '../../../shared/domain/guard';
import { PropertyError } from '../../../shared/domain/propertyError';
import { Result } from '../../../shared/domain/result';
import { ValueObject } from '../../../shared/domain/valueObject';

export interface IsDeletedProps {
  value: boolean;
}

export class IsDeleted extends ValueObject<IsDeletedProps> {
  public static propName = 'isDeleted';
  public static defaultIsDeleted = false;
  constructor(props: IsDeletedProps) {
    super(props);
  }
  get value(): boolean {
    return this.props.value;
  }

  static create(props: IsDeletedProps): Result<boolean | PropertyError> {
    const nullGuardResult = Guard.againstNullOrUndefined(
      props.value,
      this.propName
    );

    if (!nullGuardResult.succeeded) {
      props.value = this.defaultIsDeleted;
    }

    return Result.ok<boolean>(props.value);
  }
}

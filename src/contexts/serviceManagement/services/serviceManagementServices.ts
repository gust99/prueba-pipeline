import { ServiceProperty } from './../../serviceProperty/domain/serviceProperty';
import { ServicePropertyMap } from './../../serviceProperty/presentation/servicePropertyMap';
import { Publication } from './../../publicationManagement/domain/publication/publication';
import { IPublicationRepository } from './../../publicationManagement/repository/IPublicationRepository';
import { User } from './../../userManagement/domain/user';
import { UserTypeOrmRepository } from './../../userManagement/repository/userRepository';
import { ServiceDetailDTO } from './../presentation/dtos/serviceDetailDto';
import { inject, injectable } from 'inversify';
import { SERVICE_TYPES } from '../types';
import { IRepository, Query } from '../../shared/repository/IRepository';
import { Result } from '../../shared/domain/result';
import { ServiceEntity } from '../repository/service.entity';
import { ServiceMap } from '../presentation/serviceManagementMap';
import { RequestServiceDTO } from '../presentation/dtos/requestService.dto';
import { ServiceTitle } from '../domain/valueObjects/serviceTitle';
import { ServiceDescription } from '../domain/valueObjects/serviceDescription';
import { UserId } from '../../userManagement/domain/userId';
import { UniqueEntityID } from '../../shared/domain/uniqueEntityID';
import { Image } from '../domain/valueObjects/image';
import { Service } from '../domain/service';
import { Response } from '../../shared/domain/response';
import { PROFILE_TYPES } from '../../userManagement/types';
import {
  ErrorHelper,
  ObjectError,
  PropertyError
} from '../../shared/domain/propertyError';
import { Categories } from '../domain/valueObjects/categories';
import { Category } from '../domain/valueObjects/category';
import { IsDeleted } from '../domain/valueObjects/isDeleted';
import { IServiceRepository } from '../repository/IServiceRepository';
import { TYPES } from '../../../types';
import { ServicePropertyEntity } from '../../serviceProperty/repository/serviceProperty.entity';
import { TypeOrmRepository } from '../../../shared/repository/TypeOrmRepository';
import { CurrencyType } from '../../publicationManagement/domain/enums';
import { ReviewRepository } from '../../communityManagement/repository/IReviewRepository';
@injectable()
export class ServiceManagementService {
  constructor(
    @inject(SERVICE_TYPES.ServiceRepository)
    private serviceRepository: IRepository<ServiceEntity> & IServiceRepository,
    @inject(PROFILE_TYPES.UserRepository)
    private userRepository: UserTypeOrmRepository,
    @inject(TYPES.PublicationRepository)
    private publicationRepository: IPublicationRepository,
    @inject(TYPES.ServicePropertyRepository)
    private servicePropertyRepository: TypeOrmRepository<ServicePropertyEntity>,
    @inject(TYPES.REVIEW_REPOSITORY)
    private readonly reviewRepository: ReviewRepository
  ) {}
  async findAll(): Promise<Response<any>> {
    try {
      const servicesRaw = await this.serviceRepository.findAll();
      const services = servicesRaw.map((service) => {
        const serviceFound = ServiceMap.toDomain(service);
        const serviceDTO = ServiceMap.toDTO(serviceFound);
        if (service.categories != '')
          serviceDTO.categories = service.categories.split(',');
        return serviceDTO;
      });
      return Response.ok('Listing alll services', services);
    } catch (error) {
      console.log(error);
      return Response.internalServerError();
    }
  }

  async findByUser(userId: string): Promise<Response<any>> {
    try {
      const user = await this.userRepository.findById(userId);
      if (!user) return Response.notFound('User ID not found');
      const servicesRaw = await this.serviceRepository.getServicesByUser(
        userId
      );
      const services = servicesRaw.map((service: any) => {
        const serviceFound = ServiceMap.toDomain(service);
        const serviceDTO = ServiceMap.toDTO(serviceFound);
        if (service.categories != '')
          serviceDTO.categories = service.categories.split(',');
        return serviceDTO;
      });
      return Response.ok('Listing services', services);
    } catch (error) {
      console.log(error);
      return Response.internalServerError();
    }
  }

  async findOneById(id: string): Promise<Response<any>> {
    try {
      const serviceRaw = await this.serviceRepository.findById(id);

      if (serviceRaw === undefined)
        return Response.notFound('Service not found');
      const serviceFound = ServiceMap.toDomain(serviceRaw);
      const serviceDTO = ServiceMap.toDTO(serviceFound);
      if (serviceRaw.categories != '')
        serviceDTO.categories = serviceRaw.categories.split(',');

      return Response.ok('Service found', serviceDTO);
    } catch (error) {
      console.log(error);
      return Response.internalServerError();
    }
  }

  async save(serviceDTO: RequestServiceDTO): Promise<Response<any>> {
    try {
      const serviceErrors: ObjectError = {
        entity: Service.entityName,
        errors: []
      };
      const titleOrError = ServiceTitle.create({
        value: serviceDTO.serviceTitle
      });
      const descriptionOrError = ServiceDescription.create({
        value: serviceDTO.serviceDescription
      });
      const userIdOrError = UserId.create(
        new UniqueEntityID(serviceDTO.mentorId)
      );

      const imageOrError = Image.create({ value: serviceDTO.serviceImage });

      const isDeletedOrError = IsDeleted.create({
        value: serviceDTO.isDeleted
      });

      const categoriesOrError = this.verifyCategories(serviceDTO.categories);
      const propertyErrors = ErrorHelper.multiplePropertiesHaveErrors([
        titleOrError.error,
        descriptionOrError.error,
        imageOrError.error,
        categoriesOrError.error,
        isDeletedOrError.error
      ]);

      if (ErrorHelper.objectHasErrors(propertyErrors)) {
        serviceErrors.errors = propertyErrors;
        return Response.badRequest('Invalid data', serviceErrors.errors);
      }

      const service = Service.create({
        serviceTitle: titleOrError.getValue() as ServiceTitle,
        serviceDescription: descriptionOrError.getValue() as ServiceDescription,
        mentorId: userIdOrError.getValue(),
        serviceImage: imageOrError.getValue() as Image,
        categories: categoriesOrError.getValue() as Categories,
        isDeleted: isDeletedOrError.getValue() as boolean
      });

      if (!service.isSuccess)
        return Response.badRequest('Invalid data', service.error);

      const serviceEntity = ServiceMap.toPersistence(service.getValue());

      await this.serviceRepository.save(serviceEntity);

      return Response.ok(
        'Service created',
        ServiceMap.toDTO(service.getValue())
      );
    } catch (error) {
      console.log(error);
      return Response.internalServerError();
    }
  }

  async update(id: string, dto: RequestServiceDTO): Promise<Response<any>> {
    try {
      const serviceErrors: ObjectError = {
        entity: Service.entityName,
        errors: []
      };
      const serviceFound = await this.serviceRepository.findById(id);

      if (serviceFound === undefined) {
        return Response.notFound('Service not found');
      }

      const service = ServiceMap.toDomain(serviceFound);
      if ('serviceTitle' in dto) {
        const newTitleOrError = ServiceTitle.create({
          value: dto.serviceTitle
        });
        if (newTitleOrError.isSuccess)
          service.updateTitle(newTitleOrError.getValue() as ServiceTitle);
        else {
          serviceErrors.errors.push(newTitleOrError.error as PropertyError);
        }
      }

      if ('serviceDescription' in dto) {
        const newDescriptionOrError = ServiceDescription.create({
          value: dto.serviceDescription
        });
        if (newDescriptionOrError.isSuccess)
          service.updateDescription(
            newDescriptionOrError.getValue() as ServiceDescription
          );
        else
          serviceErrors.errors.push(
            newDescriptionOrError.error as PropertyError
          );
      }

      if ('serviceImage' in dto) {
        const newImageOrError = Image.create({ value: dto.serviceImage });
        if (newImageOrError.isSuccess)
          service.updateImage(newImageOrError.getValue() as Image);
        else serviceErrors.errors.push(newImageOrError.error as PropertyError);
      }

      if ('categories' in dto) {
        const newCategoriesOrError = this.verifyCategories(dto.categories);
        if (newCategoriesOrError.isSuccess)
          service.updateCategories(
            newCategoriesOrError.getValue() as Categories
          );
        else {
          serviceErrors.errors.push(
            newCategoriesOrError.error as PropertyError
          );
        }
      }

      if ('mentorId' in dto) {
        const userIdOrError = UserId.create(new UniqueEntityID(dto.mentorId));
        if (userIdOrError.isSuccess) {
          service.updateMentorId(userIdOrError.getValue() as UserId);
        } else {
          serviceErrors.errors.push(userIdOrError.error as PropertyError);
        }
      }

      if ('isDeleted' in dto) {
        const isDeletedOrError = IsDeleted.create({
          value: dto.isDeleted
        });
        if (isDeletedOrError.isSuccess) {
          service.updateIsDeleted(isDeletedOrError.getValue() as boolean);
        } else {
          serviceErrors.errors.push(isDeletedOrError.error as PropertyError);
        }
      }

      if (ErrorHelper.objectHasErrors(serviceErrors.errors))
        return Response.badRequest('Invalid data', serviceErrors.errors);

      const serviceEntity = ServiceMap.toPersistence(service);

      const updated = await this.serviceRepository.update(id, serviceEntity);

      if (updated)
        return Response.ok('Service updated', ServiceMap.toDTO(service));

      return Response.internalServerError();
    } catch (error) {
      console.log(error);
      return Response.internalServerError();
    }
  }

  async delete(id: string) {
    try {
      const serviceFound = await this.serviceRepository.findById(id);

      if (serviceFound === undefined) {
        return Response.notFound('Service not found');
      }

      const deleted = await this.serviceRepository.delete(id);

      if (deleted) return Response.ok('Service deleted');

      return Response.internalServerError();
    } catch (error) {
      console.log(error);
      return Response.internalServerError();
    }
  }

  verifyCategories(
    categories: string[]
  ): Result<Categories | string | PropertyError> {
    const categoriesObject = Categories.create([]);
    const categoriesError: PropertyError = {
      property: 'categories',
      error: []
    };
    if (!categories) return Result.ok<Categories>(categoriesObject);
    for (const iterator of categories) {
      //TODO find with categories repository
      const newCategory = Category.create({
        value: iterator
      });
      if (!newCategory.isSuccess) {
        categoriesError.error.push(`${iterator} is not an existing category`);
        return Result.fail(categoriesError);
      }
      categoriesObject.add(newCategory.getValue());
    }

    return Result.ok<Categories>(categoriesObject);
  }

  async getServiceDetail(id: string) {
    try {
      const serviceRetrieved = await this.serviceRepository.findById(id);
      const serviceResult = serviceRetrieved
        ? serviceRetrieved.isDeleted
          ? Result.fail<string>('Service Not Found.')
          : Result.ok<Service>(ServiceMap.toDomain(serviceRetrieved))
        : Result.fail<string>('Service Not Found.');

      if (serviceResult.isFailure) {
        return Response.notFound(serviceResult.errorValue() as string);
      }

      const service = serviceResult.getValue() as Service;

      const userOrError = await this.userRepository.findUser(
        service.mentorId.id.toString()
      );

      if (userOrError.isFailure) {
        return Response.notFound(userOrError.errorValue() as string);
      }

      const user = userOrError.getValue() as User;

      const publicationOrError =
        await this.publicationRepository.getPublicationById(
          user.id.toString(),
          service.id.toString()
        );

      const publication = publicationOrError.isFailure
        ? undefined
        : (publicationOrError.getValue() as Publication);

      const rawServiceContent = await this.servicePropertyRepository
        .getRepository()
        .findOne({ serviceId: service.id.toString(), propertyName: 'content' });

      let serviceContent: ServiceProperty = undefined;

      if (rawServiceContent) {
        serviceContent = ServicePropertyMap.toDomain(rawServiceContent);
      }

      const aboutTheMentor = user.professionalProfile
        ? user.professionalProfile.length > 0
          ? user.professionalProfile[0].aboutMe
          : ''
        : '';
      const hourlyRate = publication
        ? publication.rate.value.toString() +
          ' ' +
          CurrencyType[publication.rate.currency]
        : '-';

      const userServices = await this.serviceRepository.getServicesByUser(
        user.id.toString()
      );

      // const serviceRates: number[] = [];

      // let userNumOfReviews = 0;

      // for (let index = 0; index < userServices.length; index++) {
      //   const service = userServices[index];
      //   const serviceRateResult = await this.reviewRepository.getServiceRate(
      //     service.serviceId
      //   );
      //   const serviceRate = serviceRateResult.getValue();
      //   serviceRates.push(serviceRate.serviceRate);
      //   userNumOfReviews += serviceRate.reviewsNum;
      // }

      // const averageRate = (arr: number[]) =>
      //   arr.reduce((p, c) => p + c, 0) / arr.length;

      // const userRate = Math.round(averageRate(serviceRates));
      // const serviceRateRetrieved = (
      //   await this.reviewRepository.getServiceRate(id)
      // ).getValue();
      // const serviceRate = serviceRateRetrieved.serviceRate;
      // const serviceNumOfReviews = serviceRateRetrieved.reviewsNum;

      const serviceDetail: ServiceDetailDTO = {
        serviceId: service.id.toString(),
        userId: user.id.toString(),
        publicationId: publication ? publication.id.toString() : '',
        serviceTitle: service.serviceTitle.value,
        userName: user.userFirstName.value + ' ' + user.userLastName.value,
        userProfileImage: user.profileImage,
        userRol: user.userRol,
        serviceDescription: service.serviceDescription.value,
        serviceImage: service.serviceImage.value,
        serviceContent: serviceContent ? serviceContent.propertyValue : '',
        aboutTheMentor,
        hourlyRate,
        userRate: user.userRate,
        userNumOfReviews: user.userNumOfReviews,
        serviceRate: service.serviceRate,
        serviceNumOfReviews: service.serviceNumOfReviews
      };
      return Response.ok('Service information found.', serviceDetail);
    } catch (error) {
      console.log(error);
      return Response.internalServerError();
    }
  }
}

import { Column, Entity, PrimaryColumn } from 'typeorm';
@Entity('Service')
export class ServiceEntity {
  @PrimaryColumn()
  serviceId?: string;

  @Column()
  serviceTitle: string;

  @Column()
  serviceDescription: string;

  @Column()
  serviceImage?: string;

  @Column()
  mentorId: string;

  @Column()
  categories: string;

  @Column()
  isDeleted?: boolean;

  @Column({ default: 0 })
  serviceRate?: number;

  @Column({ default: 0 })
  serviceNumOfReviews?: number;
}

import { injectable } from 'inversify';
import { EntityRepository, getRepository } from 'typeorm';
import { Result } from '../../shared/domain/result';
import { TypeOrmRepository } from '../../../shared/repository/TypeOrmRepository';
import { IServiceRepository } from './IServiceRepository';
import { ServiceEntity } from './service.entity';
import { ServiceMap } from '../presentation/serviceManagementMap';
import { Service } from '../domain/service';
import { Query } from '../../shared/repository/IRepository';

@injectable()
@EntityRepository(ServiceEntity)
export class ServiceTypeOrmRepository
  extends TypeOrmRepository<ServiceEntity>
  implements IServiceRepository
{
  constructor() {
    super(getRepository(ServiceEntity));
  }

  async addService(service: ServiceEntity): Promise<Result<Service>> {
    const serviceToSave = this.getRepository().create(service);
    const savedService = await this.save(serviceToSave);
    return Result.ok<Service>(ServiceMap.toDomain(savedService));
  }

  async findService(id: string): Promise<Result<Service> | Result<string>> {
    const service = await this.findById(id);
    return service
      ? service.isDeleted
        ? Result.fail<string>('Service Not Found.')
        : Result.ok<Service>(ServiceMap.toDomain(service))
      : Result.fail<string>('Service Not Found.');
  }

  async deleteService(id: string): Promise<Result<boolean> | Result<string>> {
    const serviceOrError = await this.findService(id);
    if (serviceOrError.isFailure) {
      return serviceOrError as Result<string>;
    }
    await this.getRepository().update(id, { isDeleted: true });
    return Result.ok(true);
  }
  getTrendingServices(): Service[] {
    throw new Error('Method not implemented.');
  }
  getFilteredServices(filter: any): Service[] {
    console.log(filter);
    throw new Error('Method not implemented.');
  }
  getServicesByUser(userId: string): Promise<ServiceEntity[]> {
    const query: Query<ServiceEntity> = {
      mentorId: userId
    };
    return this.findByQuery(query);
  }
}

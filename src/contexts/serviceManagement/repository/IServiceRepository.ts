import { Result } from '../../shared/domain/result';
import { IRepository } from '../../shared/repository/IRepository';
import { Service } from '../domain/service';
import { ServiceEntity } from './service.entity';

export interface IServiceRepository {
  addService(request: ServiceEntity): Promise<Result<Service>>;
  findService(id: string): Promise<Result<Service> | Result<string>>;
  deleteService(id: string): Promise<Result<boolean> | Result<string>>;
  getTrendingServices(): Service[];
  getFilteredServices(filter: any): Service[];
  getServicesByUser(userId: string): Promise<ServiceEntity[]>;
}

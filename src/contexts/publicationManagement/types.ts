const PUBLICATION_TYPES = {
  PublicationController: Symbol.for('PublicationController'),
  PublicationService: Symbol.for('PublicationService'),
  PublicationRepository: Symbol.for('PublicationRepository'),
  Lodash: Symbol.for('Lodash')
};

export { PUBLICATION_TYPES };

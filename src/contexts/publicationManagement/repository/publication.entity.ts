import { Column, Entity, PrimaryColumn } from 'typeorm';
import {
  CurrencyType,
  DiscountType,
  FeeType,
  PackageType,
  PublicationStatus
} from '../domain/enums';

@Entity('Publication', {
  orderBy: {
    date: 'DESC'
  }
})
export class PublicationEntity {
  @PrimaryColumn()
  id?: string;

  @Column()
  userId: string;

  @Column()
  title: string;

  @Column('longtext')
  description: string;

  @Column({ type: 'longtext', nullable: true })
  description2: string;

  @Column()
  date: Date;

  @Column({
    type: 'enum',
    enum: PublicationStatus,
    default: PublicationStatus.Private
  })
  status: PublicationStatus;

  @Column({ default: 0 })
  rate: number;

  @Column({
    type: 'enum',
    enum: CurrencyType,
    default: CurrencyType.USD
  })
  currencyType: CurrencyType;

  @Column({ default: '' })
  image: string;

  @Column({ default: '' })
  categories: string;

  @Column({ default: '' })
  languages: string;

  @Column()
  courseId: string;

  @Column({ default: false })
  online: boolean;

  @Column({ default: false })
  travel: boolean;

  @Column({ default: false })
  house: boolean;

  @Column({
    default: 'Calle IV Centenario, Jaime Zudañez, Cochabamba, Bolivia'
  })
  address: string;

  @Column({ default: '-17.40764943278393' })
  latitude: string;

  @Column({ default: '-66.13803863525392' })
  longitude: string;

  @Column({ default: '' })
  packageValueA: string;
  @Column({ default: 0 })
  rateA: number;

  @Column({
    type: 'enum',
    enum: CurrencyType,
    default: CurrencyType.USD
  })
  currencyTypeA: CurrencyType;
  @Column({
    type: 'enum',
    enum: PackageType,
    default: PackageType.None
  })
  packageTypeA: PackageType;

  @Column({ default: '' })
  packageValueB: string;
  @Column({ default: 0 })
  rateB: number;

  @Column({
    type: 'enum',
    enum: CurrencyType,
    default: CurrencyType.Bs
  })
  currencyTypeB: CurrencyType;
  @Column({
    type: 'enum',
    enum: PackageType,
    default: PackageType.None
  })
  packageTypeB: PackageType;

  @Column({ default: '' })
  packageValueC: string;
  @Column({ default: 0 })
  rateC: number;

  @Column({
    type: 'enum',
    enum: CurrencyType,
    default: CurrencyType.Bs
  })
  currencyTypeC: CurrencyType;
  @Column({
    type: 'enum',
    enum: PackageType,
    default: PackageType.None
  })
  packageTypeC: PackageType;

  @Column({
    type: 'enum',
    enum: DiscountType,
    default: DiscountType.None
  })
  discountType: DiscountType;
  @Column({ default: 0 })
  discountRate: number;

  @Column({
    type: 'enum',
    enum: CurrencyType,
    default: CurrencyType.Bs
  })
  discountCurrency: CurrencyType;

  @Column({
    type: 'enum',
    enum: FeeType,
    default: FeeType.None
  })
  feeType: FeeType;
  @Column({ default: 0 })
  feeRate: number;

  @Column({
    type: 'enum',
    enum: CurrencyType,
    default: CurrencyType.Bs
  })
  feeCurrency: CurrencyType;
}

import { injectable } from 'inversify';
import { EntityRepository, getRepository } from 'typeorm';
import { TypeOrmRepository } from '../../../shared/repository/TypeOrmRepository';
import { Result } from '../../shared/domain/result';
import { Query } from '../../shared/repository/IRepository';
import { Publication } from '../domain/publication/publication';
import { PublicationMap } from '../services/mappers/publication.map';
import { IPublicationRepository } from './IPublicationRepository';
import { PublicationEntity } from './publication.entity';
@injectable()
@EntityRepository(PublicationEntity)
export class TypeORMPublicationRepository
  extends TypeOrmRepository<PublicationEntity>
  implements IPublicationRepository
{
  constructor() {
    super(getRepository(PublicationEntity));
  }
  getTrendingPublications(): Publication[] {
    throw new Error('Method not implemented.');
  }
  getFilteredPublications(filter: any): Publication[] {
    console.log(filter);
    throw new Error('Method not implemented.');
  }

  getPublicationsByUser(userId: string): Promise<PublicationEntity[]> {
    const query: Query<PublicationEntity> = {
      userId: userId
    };
    return this.findByQuery(query);
  }

  async getPublicationById(
    userId: string,
    serviceId: string
  ): Promise<Result<Publication> | Result<string>> {
    const rawPublication = await this.getRepository().findOne({
      userId,
      courseId: serviceId
    });
    return rawPublication
      ? Result.ok<Publication>(PublicationMap.toDomain(rawPublication))
      : Result.fail<string>('Publication not found.');
  }
}

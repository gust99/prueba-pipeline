import { Result } from '../../shared/domain/result';
import { Publication } from '../domain/publication/publication';
import { PublicationEntity } from './publication.entity';
export interface IPublicationRepository {
  getTrendingPublications(): Publication[];
  getFilteredPublications(filter: any): Publication[];
  getPublicationsByUser(userId: string): Promise<PublicationEntity[]>;
  getPublicationById(
    userId: string,
    serviceId: string
  ): Promise<Result<Publication> | Result<string>>;
}

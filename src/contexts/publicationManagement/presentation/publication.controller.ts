import {
  controller,
  httpGet,
  BaseHttpController,
  httpPost,
  httpPut,
  httpDelete,
  request,
  response,
  requestParam
} from 'inversify-express-utils';
import { Response, Request } from 'express';
import { inject } from 'inversify';
import { PUBLICATION_TYPES } from '../types';
import { PublicationService } from '../services/publication.service';
import { Publication } from '../domain/publication/publication';
import { RequestPublicationDTO } from './dto/requestPublication.dto';
import { plainToInstance } from 'class-transformer';
import { ObjectType } from 'typeorm';
import { loggedUserMw } from '../../../shared/presentation/middleware/tokenMiddleware/tokenverifier.middleware';
import { SearchRepository } from '../../searchManagement/repository/index';
import { SEARCH_TYPES } from '../../searchManagement/types';

@controller('/publications')
export class PublicationController extends BaseHttpController {
  private entityType: ObjectType<Publication>;

  constructor(
    @inject(PUBLICATION_TYPES.PublicationService)
    private publicationService: PublicationService,
    @inject(SEARCH_TYPES.searchRepository)
    private searchRepository: SearchRepository
  ) {
    super();
  }

  @httpGet('/')
  private async getPublications(req: Request, res: Response) {
    const response = await this.publicationService.findAll();
    res.status(response.statusCode).json(response.responseContent);
  }
  @httpGet('/user/:id', loggedUserMw)
  private async getPublicationsByUser(
    @requestParam('id') id: string,
    @request() req: Request,
    @response() res: Response
  ) {
    const response = await this.publicationService.findByUser(id);
    res.status(response.statusCode).json(response.responseContent);
  }

  @httpPost('/', loggedUserMw)
  private async createPublication(
    @request() req: Request,
    @response() res: Response
  ) {
    const response = await this.publicationService.save(
      plainToInstance(RequestPublicationDTO, req.body)
    );
    await this.searchRepository.elasticClient.index({
      index: 'publications',
      id: response.responseContent.data.id,
      body: req.body
    });
    res.status(response.statusCode).json(response.responseContent);
  }

  @httpGet('/:id')
  private async getPublication(
    @requestParam('id') id: string,
    @response() res: Response
  ) {
    const response = await this.publicationService.findOneById(id);
    res.status(response.statusCode).json(response.responseContent);
  }

  @httpPut('/:id', loggedUserMw)
  private async update(
    @requestParam('id') id: string,
    @request() req: Request,
    @response() res: Response
  ) {
    const response = await this.publicationService.update(
      id,
      plainToInstance(RequestPublicationDTO, req.body)
    );
    try {
      const query = {
        index: 'publications',
        body: {
          query: {
            multi_match: {
              query: id,
              fields: [
                'id',
                '_id',
                'userId',
                'courseId',
                'title',
                'description',
                'categories'
              ]
            }
          }
        }
      };
      const data = await this.searchRepository.elasticClient.search(query);
      const _id = data.hits.hits[0]._id;
      await this.searchRepository.elasticClient.update({
        index: 'publications',
        id: _id,
        body: {
          doc: req.body
        }
      });
    } catch {
      console.error(
        'No index found to update or something happening updating publication in elastic'
      );
    }

    res.status(response.statusCode).json(response.responseContent);
  }

  @httpDelete('/:id', loggedUserMw)
  private async delete(
    @requestParam('id') id: string,
    @response() res: Response
  ) {
    const response = await this.publicationService.delete(id);
    try {
      const query = {
        index: 'publications',
        body: {
          query: {
            multi_match: {
              query: id,
              fields: [
                'id',
                '_id',
                'userId',
                'courseId',
                'title',
                'description',
                'categories'
              ]
            }
          }
        }
      };
      const data = await this.searchRepository.elasticClient.search(query);
      const _id = data.hits.hits[0]._id;
      await this.searchRepository.elasticClient.delete({
        index: 'publications',
        id: _id
      });
    } catch {
      console.error(
        'No index found to delete or something happening deleting publication in elastic'
      );
    }
    res.status(response.statusCode).json(response.responseContent);
  }
}

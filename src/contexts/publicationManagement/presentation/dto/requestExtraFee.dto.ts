import { CurrencyType, FeeType } from '../../domain/enums';

class RequestExtraFeeDTO {
  rate: number;
  currencyType: CurrencyType;
  extraFeeType: FeeType;
}
export { RequestExtraFeeDTO };

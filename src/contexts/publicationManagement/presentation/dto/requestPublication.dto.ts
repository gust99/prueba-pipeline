import { CurrencyType, PublicationStatus } from '../../domain/enums';
import { RequestExtraFeeDTO } from './requestExtraFee.dto';
import { RequestPackageDTO } from './requestPackage.dto';
import { RequestDiscountDTO } from './requestDiscount.dto';

class RequestPublicationDTO {
  userId: string;
  courseId: string;
  title: string;
  description: string;
  description2?: string;
  date: string;
  image: string;
  status: PublicationStatus;
  rate: number;
  currencyType: CurrencyType;
  categories: [];
  languages: [];
  online: boolean;
  travel: boolean;
  house: boolean;
  address: string;
  latitude: string;
  longitude: string;
  packageA: RequestPackageDTO;
  packageB: RequestPackageDTO;
  packageC: RequestPackageDTO;
  extraFee: RequestExtraFeeDTO;
  discount: RequestDiscountDTO;
}

export { RequestPublicationDTO };

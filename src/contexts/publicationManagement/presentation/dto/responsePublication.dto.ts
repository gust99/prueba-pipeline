import { PublicationStatus } from '../../domain/enums';
import { ResponseDiscountDTO } from './responseDiscount.dto';
import { ResponseExtraFeeDTO } from './responseExtraFee.dto';
import { ResponsePackageDTO } from './responsePackage.dto';

export class ResponsePublicationDTO {
  id: string;
  courseId: string;
  courseTitle: string;
  userId: string;
  title: string;
  description: string;
  description2: string;
  status: PublicationStatus;
  rate: string;
  image: string;
  categories: string[];
  languages: string[];
  online: boolean;
  travel: boolean;
  house: boolean;
  address: string;
  latitude: string;
  longitude: string;
  packageA: ResponsePackageDTO;
  packageB: ResponsePackageDTO;
  packageC: ResponsePackageDTO;
  extraFee: ResponseExtraFeeDTO;
  discount: ResponseDiscountDTO;
  serviceRate: number;
  serviceNumOfReviews: number;
}

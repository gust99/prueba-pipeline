import { CurrencyType, DiscountType } from '../../domain/enums';

class RequestDiscountDTO {
  rate: number;
  currencyType: CurrencyType;
  discountType: DiscountType;
}
export { RequestDiscountDTO };

import { CurrencyType, FeeType } from '../../domain/enums';

class ResponseExtraFeeDTO {
  rate: number;
  currencyType: CurrencyType;
  extraFeeType: FeeType;
}
export { ResponseExtraFeeDTO };

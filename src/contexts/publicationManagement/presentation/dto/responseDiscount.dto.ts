import { CurrencyType, DiscountType } from '../../domain/enums';

class ResponseDiscountDTO {
  rate: number;
  currencyType: CurrencyType;
  discountType: DiscountType;
}
export { ResponseDiscountDTO };

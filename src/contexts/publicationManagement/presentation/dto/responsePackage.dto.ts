import { CurrencyType, PackageType } from '../../domain/enums';

class ResponsePackageDTO {
  value: string;
  rate: number;
  currencyType: CurrencyType;
  type: PackageType;
}
export { ResponsePackageDTO };

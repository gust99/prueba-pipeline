import { CurrencyType, PackageType } from '../../domain/enums';

class RequestPackageDTO {
  value: string;
  rate: number;
  currencyType: CurrencyType;
  type: PackageType;
}
export { RequestPackageDTO };

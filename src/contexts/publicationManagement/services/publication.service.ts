import { inject, injectable } from 'inversify';
import { PUBLICATION_TYPES } from '../types';
import { IRepository } from '../../shared/repository/IRepository';
import { Result } from '../../shared/domain/result';
import { PublicationEntity } from '../repository/publication.entity';
import { PublicationMap } from './mappers/publication.map';
import { RequestPublicationDTO } from '../presentation/dto/requestPublication.dto';
import { PublicationTitle } from '../domain/publication/publicationTitle';
import { PublicationDescription } from '../domain/publication/publicationDescription';
import { Rate } from '../domain/rate';
import { UserId } from '../../userManagement/domain/userId';
import { UniqueEntityID } from '../../shared/domain/uniqueEntityID';
import { Image } from '../domain/image';
import { Publication } from '../domain/publication/publication';
import { Response } from '../../shared/domain/response';
import { Categories } from '../domain/categories';
import { PROFILE_TYPES } from '../../userManagement/types';
import { Category } from '../domain/category';
import {
  ErrorHelper,
  ObjectError,
  PropertyError
} from '../../shared/domain/propertyError';
import { CourseId } from '../domain/courseId';
import { IPublicationRepository } from '../repository/IPublicationRepository';
import { Languages } from '../domain/languages';
import { Language } from '../domain/language';
import LocationOptions from '../domain/locationOptions';
import { Address } from '../domain/address';
import { UserEntity } from '../../userManagement/repository/user.entity';
import {
  DiscountType,
  ExtraType,
  FeeType,
  PublicationStatus
} from '../domain/enums';
import { PublicationPackage } from '../domain/package';
import { ExtraPricing } from '../domain/extraPricing';
import { SERVICE_TYPES } from '../../serviceManagement/types';
import { ServiceEntity } from '../../serviceManagement/repository/service.entity';
import { IServiceRepository } from '../../serviceManagement/repository/IServiceRepository';
import { Service } from '../../serviceManagement/domain/service';
import { ServiceTitle } from '../../serviceManagement/domain/valueObjects/serviceTitle';
import { ServiceDescription } from '../../serviceManagement/domain/valueObjects/serviceDescription';

@injectable()
export class PublicationService {
  constructor(
    @inject(PUBLICATION_TYPES.PublicationRepository)
    private publicationRepository: IRepository<PublicationEntity> &
      IPublicationRepository,
    @inject(PROFILE_TYPES.UserRepository)
    private userRepository: IRepository<UserEntity>,
    @inject(SERVICE_TYPES.ServiceRepository)
    private serviceRepository: IRepository<ServiceEntity> & IServiceRepository
  ) {}

  async findByUser(userId: string): Promise<Response<any>> {
    try {
      const user = await this.userRepository.findById(userId);
      if (!user) return Response.notFound('User ID not found');
      const publicationsRaw =
        await this.publicationRepository.getPublicationsByUser(userId);
      const publications = await Promise.all(
        publicationsRaw.map(async (publication) => {
          const publicationFound = PublicationMap.toDomain(publication);
          const course = await this.serviceRepository.findService(
            publicationFound.courseId.id.toString()
          );

          if (user.userRol === 'mentor') {
            if (!course.isSuccess) {
              return Response.notFound('Course id not found');
            }
            const publicationDTO = PublicationMap.toDTO(
              publicationFound,
              course.getValue() as Service
            );
            if (publication.categories != '')
              publicationDTO.categories = publication.languages.split(',');
            if (publication.languages != '')
              publicationDTO.languages = publication.languages.split(',');
            return publicationDTO;
          } else {
            const course = Service.create({
              serviceTitle: ServiceTitle.create({
                value: 'course title'
              }).getValue() as ServiceTitle,
              serviceDescription: ServiceDescription.create({
                value: 'description description'
              }).getValue() as ServiceDescription,
              mentorId: UserId.create().getValue()
            });
            const publicationDTO = PublicationMap.toDTO(
              publicationFound,
              course.getValue() as Service
            );
            if (publication.categories != '')
              publicationDTO.categories = publication.languages.split(',');
            if (publication.languages != '')
              publicationDTO.languages = publication.languages.split(',');
            return publicationDTO;
          }
        })
      );
      return Response.ok('Listing publications', publications);
    } catch (error) {
      console.log(error);
      return Response.internalServerError();
    }
  }
  async findAll(): Promise<Response<any>> {
    try {
      const publicationsRaw = await this.publicationRepository.findAll();
      const publications = await Promise.all(
        publicationsRaw.map(async (publication) => {
          const publicationFound = PublicationMap.toDomain(publication);
          const course = await this.serviceRepository.findService(
            publicationFound.courseId.id.toString()
          );
          const user = await this.userRepository.findById(
            publicationFound.userId.id.toString()
          );
          if (user.userRol === 'mentor') {
            if (!course.isSuccess) {
              return Response.notFound('Course id not found');
            }
            const publicationDTO = PublicationMap.toDTO(
              publicationFound,
              course.getValue() as Service
            );
            if (publication.categories != '')
              publicationDTO.categories = publication.languages.split(',');
            if (publication.languages != '')
              publicationDTO.languages = publication.languages.split(',');
            return publicationDTO;
          } else {
            const course = Service.create({
              serviceTitle: ServiceTitle.create({
                value: 'course title'
              }).getValue() as ServiceTitle,
              serviceDescription: ServiceDescription.create({
                value: 'description description'
              }).getValue() as ServiceDescription,
              mentorId: UserId.create().getValue()
            });
            const publicationDTO = PublicationMap.toDTO(
              publicationFound,
              course.getValue() as Service
            );
            if (publication.categories != '')
              publicationDTO.categories = publication.languages.split(',');
            if (publication.languages != '')
              publicationDTO.languages = publication.languages.split(',');
            return publicationDTO;
          }
        })
      );
      return Response.ok('Listing publications', publications);
    } catch (error) {
      console.log(error);
      return Response.internalServerError();
    }
  }

  async findOneById(id: string): Promise<Response<any>> {
    try {
      const publicationRaw = await this.publicationRepository.findById(id);

      if (publicationRaw === undefined)
        return Response.notFound('Publication not found');

      const course = await this.serviceRepository.findService(
        publicationRaw.courseId
      );
      const user = await this.userRepository.findById(publicationRaw.userId);
      if (user.userRol === 'mentor') {
        if (!course.isSuccess) {
          return Response.notFound('Course id not found');
        }
        const publicationFound = PublicationMap.toDomain(publicationRaw);
        const publicationDTO = PublicationMap.toDTO(
          publicationFound,
          course.getValue() as Service
        );
        if (publicationRaw.categories != '')
          publicationDTO.categories = publicationRaw.categories.split(',');

        if (publicationRaw.languages != '')
          publicationDTO.languages = publicationRaw.languages.split(',');

        return Response.ok('Publication found', publicationDTO);
      } else {
        const course = Service.create({
          serviceTitle: ServiceTitle.create({
            value: 'course title'
          }).getValue() as ServiceTitle,
          serviceDescription: ServiceDescription.create({
            value: 'description description'
          }).getValue() as ServiceDescription,
          mentorId: UserId.create().getValue()
        });
        const publicationFound = PublicationMap.toDomain(publicationRaw);
        const publicationDTO = PublicationMap.toDTO(
          publicationFound,
          course.getValue() as Service
        );
        if (publicationRaw.categories != '')
          publicationDTO.categories = publicationRaw.categories.split(',');

        if (publicationRaw.languages != '')
          publicationDTO.languages = publicationRaw.languages.split(',');

        return Response.ok('Publication found', publicationDTO);
      }
    } catch (error) {
      console.log(error);
      return Response.internalServerError();
    }
  }

  async save(publicationDTO: RequestPublicationDTO): Promise<Response<any>> {
    try {
      const user = await this.userRepository.findById(publicationDTO.userId);
      if (!user) return Response.notFound('User ID not found');

      let course = await this.serviceRepository.findService(
        publicationDTO.courseId
      );
      if (user.userRol === 'mentor') {
        if (!course.isSuccess) {
          return Response.notFound('Course id not found');
        }
      } else {
        course = Service.create({
          serviceTitle: ServiceTitle.create({
            value: 'course title'
          }).getValue() as ServiceTitle,
          serviceDescription: ServiceDescription.create({
            value: 'description description'
          }).getValue() as ServiceDescription,
          mentorId: UserId.create().getValue()
        });
      }

      const publicationErrors: ObjectError = {
        entity: Publication.entityName,
        errors: []
      };
      const titleOrError = PublicationTitle.create({
        value: publicationDTO.title
      });
      const descriptionOrError = PublicationDescription.create({
        value: publicationDTO.description
      });

      const description2OrError = publicationDTO.description2
        ? PublicationDescription.create({
            value: publicationDTO.description
          })
        : null;
      const userIdOrError = UserId.create(
        new UniqueEntityID(publicationDTO.userId)
      );

      const courseIdOrError = CourseId.create(
        new UniqueEntityID(publicationDTO.courseId)
      );

      const rateOrError = Rate.create({
        value: publicationDTO.rate,
        currencyType: publicationDTO.currencyType
      });
      const imageOrError = Image.create({ value: publicationDTO.image });

      const categoriesOrError = this.verifyCategories(
        publicationDTO.categories
      );
      const languagesOrError = this.verifyLanguages(publicationDTO.languages);

      const locationOptionsOrError = LocationOptions.create({
        online: publicationDTO.online,
        travel: publicationDTO.travel,
        house: publicationDTO.house
      });

      const addressOrError = Address.create({
        value: publicationDTO.address,
        longitude: publicationDTO.longitude,
        latitude: publicationDTO.latitude
      });

      const packageAOrError = PublicationPackage.create({
        value: publicationDTO.packageA.value,
        rate: Rate.create({
          value: publicationDTO.packageA.rate,
          currencyType: publicationDTO.packageA.currencyType
        }).getValue() as Rate,
        type: publicationDTO.packageA.type
      });

      const packageBOrError = PublicationPackage.create({
        value: publicationDTO.packageB.value,
        rate: Rate.create({
          value: publicationDTO.packageB.rate,
          currencyType: publicationDTO.packageB.currencyType
        }).getValue() as Rate,
        type: publicationDTO.packageB.type
      });

      const packageCOrError = PublicationPackage.create({
        value: publicationDTO.packageC.value,
        rate: Rate.create({
          value: publicationDTO.packageC.rate,
          currencyType: publicationDTO.packageC.currencyType
        }).getValue() as Rate,
        type: publicationDTO.packageC.type
      });

      const discountOrError = ExtraPricing.create({
        rate: publicationDTO.discount
          ? (Rate.create({
              value: publicationDTO.discount.rate,
              currencyType: publicationDTO.discount.currencyType
            }).getValue() as Rate)
          : (Rate.create({
              value: 0,
              currencyType: 0
            }).getValue() as Rate),
        feeOrDiscountType: publicationDTO.discount
          ? publicationDTO.discount.discountType
          : DiscountType.None,
        type: ExtraType.Discount
      });

      const extraFeeOrError = ExtraPricing.create({
        rate: publicationDTO.extraFee
          ? (Rate.create({
              value: publicationDTO.extraFee.rate,
              currencyType: publicationDTO.extraFee.currencyType
            }).getValue() as Rate)
          : (Rate.create({
              value: 0,
              currencyType: 0
            }).getValue() as Rate),
        feeOrDiscountType: publicationDTO.extraFee
          ? publicationDTO.extraFee.extraFeeType
          : FeeType.None,
        type: ExtraType.ExtraFee
      });

      const propertyErrors = ErrorHelper.multiplePropertiesHaveErrors([
        titleOrError.error,
        descriptionOrError.error,
        rateOrError.error,
        imageOrError.error,
        categoriesOrError.error,
        languagesOrError.error,
        locationOptionsOrError.error,
        addressOrError.error,
        packageAOrError.error,
        packageBOrError.error,
        packageCOrError.error,
        discountOrError.error,
        extraFeeOrError.error
      ]);

      if (ErrorHelper.objectHasErrors(propertyErrors)) {
        publicationErrors.errors = propertyErrors;
        return Response.badRequest('Invalid data', publicationErrors.errors);
      }

      const status = publicationDTO.status as PublicationStatus;

      const publication = Publication.create({
        title: titleOrError.getValue() as PublicationTitle,
        description: descriptionOrError.getValue() as PublicationDescription,
        status: status,
        rate: rateOrError.getValue() as Rate,
        userId: userIdOrError.getValue(),
        image: imageOrError.getValue() as Image,
        courseId: courseIdOrError.getValue(),
        categories: categoriesOrError.getValue() as Categories,
        languages: languagesOrError.getValue() as Languages,
        locationOptions: locationOptionsOrError.getValue() as LocationOptions,
        address: addressOrError.getValue() as Address,
        packageA: packageAOrError.getValue() as PublicationPackage,
        packageB: packageBOrError.getValue() as PublicationPackage,
        packageC: packageCOrError.getValue() as PublicationPackage,
        discount: discountOrError.getValue() as ExtraPricing,
        extraFee: extraFeeOrError.getValue() as ExtraPricing,
        description2: description2OrError
          ? (description2OrError.getValue() as PublicationDescription)
          : null
      });

      if (!publication.isSuccess)
        return Response.badRequest('Invalid data', publication.error);

      const publicationEntity = PublicationMap.toPersistence(
        publication.getValue()
      );
      await this.publicationRepository.save(publicationEntity);

      return Response.ok(
        'Publication created',
        PublicationMap.toDTO(
          publication.getValue(),
          course.getValue() as Service
        )
      );
    } catch (error) {
      console.log(error);
      return Response.internalServerError();
    }
  }

  async update(id: string, dto: RequestPublicationDTO): Promise<Response<any>> {
    try {
      const publicationErrors: ObjectError = {
        entity: Publication.entityName,
        errors: []
      };
      const publicationFound = await this.publicationRepository.findById(id);

      if (publicationFound === undefined) {
        return Response.notFound('Publication not found');
      }
      const user = await this.userRepository.findById(publicationFound.userId);
      let course = await this.serviceRepository.findService(dto.courseId);
      if (user.userRol === 'mentor') {
        if (!course.isSuccess) {
          return Response.notFound('Course id not found');
        }
      } else {
        course = Service.create({
          serviceTitle: ServiceTitle.create({
            value: 'course title'
          }).getValue() as ServiceTitle,
          serviceDescription: ServiceDescription.create({
            value: 'description description'
          }).getValue() as ServiceDescription,
          mentorId: UserId.create().getValue()
        });
      }
      const publication = PublicationMap.toDomain(publicationFound);
      if ('title' in dto) {
        const newTitleOrError = PublicationTitle.create({ value: dto.title });
        if (newTitleOrError.isSuccess)
          publication.updateTitle(
            newTitleOrError.getValue() as PublicationTitle
          );
        else {
          publicationErrors.errors.push(newTitleOrError.error as PropertyError);
        }
      }

      if ('description' in dto) {
        const newDescriptionOrError = PublicationDescription.create({
          value: dto.description
        });
        if (newDescriptionOrError.isSuccess)
          publication.updateDescription(
            newDescriptionOrError.getValue() as PublicationDescription
          );
        else
          publicationErrors.errors.push(
            newDescriptionOrError.error as PropertyError
          );
      }
      if ('description2' in dto) {
        const newDescriptionOrError = dto.description2
          ? PublicationDescription.create({
              value: dto.description2
            })
          : null;
        if (newDescriptionOrError != null) {
          if (newDescriptionOrError.isSuccess)
            publication.updateDescription2(
              newDescriptionOrError.getValue() as PublicationDescription
            );
          else
            publicationErrors.errors.push(
              newDescriptionOrError.error as PropertyError
            );
        } else {
          publication.updateDescription2(null);
        }
      }

      if ('status' in dto) {
        const newStatusOrError = Publication.isValidStatus(dto.status);
        if (newStatusOrError.isSuccess) publication.updateStatus(dto.status);
        else {
          publicationErrors.errors.push({
            property: 'status',
            value: dto.status,
            error: [newStatusOrError.error as string]
          });
        }
      }

      if ('rate' in dto) {
        const newRateOrError = Rate.create({
          value: dto.rate,
          currencyType: publication.rate.currency
        });
        if (newRateOrError.isSuccess)
          publication.updateRate(newRateOrError.getValue() as Rate);
        else
          publicationErrors.errors.push(newRateOrError.error as PropertyError);
      }

      if ('currencyType' in dto) {
        const newRateOrError = Rate.create({
          value: publication.rate.value,
          currencyType: dto.currencyType
        });
        if (newRateOrError.isSuccess)
          publication.updateRate(newRateOrError.getValue() as Rate);
        else
          publicationErrors.errors.push(newRateOrError.error as PropertyError);
      }

      if ('image' in dto) {
        const newImageOrError = Image.create({ value: dto.image });
        if (newImageOrError.isSuccess)
          publication.updateImage(newImageOrError.getValue() as Image);
        else
          publicationErrors.errors.push(newImageOrError.error as PropertyError);
      }

      if ('categories' in dto) {
        const newCategoriesOrError = this.verifyCategories(dto.categories);
        if (newCategoriesOrError.isSuccess)
          publication.updateCategories(
            newCategoriesOrError.getValue() as Categories
          );
        else {
          publicationErrors.errors.push(
            newCategoriesOrError.error as PropertyError
          );
        }
      }

      if ('languages' in dto) {
        const newLanguagesOrError = this.verifyLanguages(dto.languages);
        if (newLanguagesOrError.isSuccess)
          publication.updateLanguages(
            newLanguagesOrError.getValue() as Categories
          );
        else {
          publicationErrors.errors.push(
            newLanguagesOrError.error as PropertyError
          );
        }
      }

      if ('online' in dto) {
        publication.locationOptions.updateOnline = dto.online;
      }
      if ('travel' in dto) {
        publication.locationOptions.updateTravel = dto.travel;
      }
      if ('house' in dto) {
        publication.locationOptions.updateHouse = dto.house;
      }

      if ('address' in dto) {
        const newAddressOrError = Address.create({
          value: dto.address,
          latitude: publication.address.latitude,
          longitude: publication.address.longitude
        });
        if (newAddressOrError.isSuccess)
          publication.updateAddress(newAddressOrError.getValue() as Address);
        else
          publicationErrors.errors.push(
            newAddressOrError.error as PropertyError
          );
      }

      if ('latitude' in dto) {
        const newAddressOrError = Address.create({
          value: publication.address.value,
          latitude: dto.latitude,
          longitude: publication.address.longitude
        });
        if (newAddressOrError.isSuccess)
          publication.updateAddress(newAddressOrError.getValue() as Address);
        else
          publicationErrors.errors.push(
            newAddressOrError.error as PropertyError
          );
      }

      if ('longitude' in dto) {
        const newAddressOrError = Address.create({
          value: publication.address.value,
          latitude: publication.address.latitude,
          longitude: dto.longitude
        });
        if (newAddressOrError.isSuccess)
          publication.updateAddress(newAddressOrError.getValue() as Address);
        else
          publicationErrors.errors.push(
            newAddressOrError.error as PropertyError
          );
      }
      if ('packageA' in dto) {
        const packageAOrError = PublicationPackage.create({
          value: dto.packageA.value,
          rate: Rate.create({
            value: dto.packageA.rate,
            currencyType: dto.packageA.currencyType
          }).getValue() as Rate,
          type: dto.packageA.type
        });
        if (packageAOrError.isSuccess) {
          publication.updatePackageA(
            packageAOrError.getValue() as PublicationPackage
          );
        } else {
          publicationErrors.errors.push(packageAOrError.error as PropertyError);
        }
      }

      if ('packageB' in dto) {
        const packageBOrError = PublicationPackage.create({
          value: dto.packageB.value,
          rate: Rate.create({
            value: dto.packageB.rate,
            currencyType: dto.packageB.currencyType
          }).getValue() as Rate,
          type: dto.packageB.type
        });
        if (packageBOrError.isSuccess) {
          publication.updatePackageB(
            packageBOrError.getValue() as PublicationPackage
          );
        } else {
          publicationErrors.errors.push(packageBOrError.error as PropertyError);
        }
      }

      if ('packageC' in dto) {
        const packageCOrError = PublicationPackage.create({
          value: dto.packageC.value,
          rate: Rate.create({
            value: dto.packageC.rate,
            currencyType: dto.packageC.currencyType
          }).getValue() as Rate,
          type: dto.packageC.type
        });
        if (packageCOrError.isSuccess) {
          publication.updatePackageC(
            packageCOrError.getValue() as PublicationPackage
          );
        } else {
          publicationErrors.errors.push(packageCOrError.error as PropertyError);
        }
      }

      if ('discount' in dto) {
        const discountOrError = ExtraPricing.create({
          rate: Rate.create({
            value: dto.discount.rate,
            currencyType: dto.discount.currencyType
          }).getValue() as Rate,
          feeOrDiscountType: dto.discount.discountType,
          type: ExtraType.Discount
        });
        if (discountOrError.isSuccess) {
          publication.updateDiscounts(
            discountOrError.getValue() as ExtraPricing
          );
        } else {
          publicationErrors.errors.push(discountOrError.error as PropertyError);
        }
      }

      if ('extraFee' in dto) {
        const extraFeeOrError = ExtraPricing.create({
          rate: Rate.create({
            value: dto.extraFee.rate,
            currencyType: dto.extraFee.currencyType
          }).getValue() as Rate,
          feeOrDiscountType: dto.extraFee.extraFeeType,
          type: ExtraType.ExtraFee
        });

        if (extraFeeOrError.isSuccess) {
          publication.updateExtraFees(
            extraFeeOrError.getValue() as ExtraPricing
          );
        } else {
          publicationErrors.errors.push(extraFeeOrError.error as PropertyError);
        }
      }

      if (ErrorHelper.objectHasErrors(publicationErrors.errors))
        return Response.badRequest('Invalid data', publicationErrors.errors);

      const publicationEntity = PublicationMap.toPersistence(publication);

      const updated = await this.publicationRepository.update(
        id,
        publicationEntity
      );

      if (updated)
        return Response.ok(
          'Publication updated',
          PublicationMap.toDTO(publication, course.getValue() as Service)
        );

      return Response.internalServerError();
    } catch (error) {
      console.log(error);
      return Response.internalServerError();
    }
  }

  async delete(id: string) {
    try {
      const publicationFound = await this.publicationRepository.findById(id);

      if (publicationFound === undefined) {
        return Response.notFound('Publication not found');
      }

      const deleted = await this.publicationRepository.delete(id);

      if (deleted) return Response.ok('Publication deleted');

      return Response.internalServerError();
    } catch (error) {
      console.log(error);
      return Response.internalServerError();
    }
  }

  verifyCategories(
    categories: string[]
  ): Result<Categories | string | PropertyError> {
    const categoriesObject = Categories.create([]);
    const categoriesError: PropertyError = {
      property: 'categories',
      error: []
    };
    if (!categories) return Result.ok<Categories>(categoriesObject);
    for (const iterator of categories) {
      const newCategory = Category.create({
        value: iterator
      });
      if (!newCategory.isSuccess) {
        categoriesError.error.push(`${iterator} is not an existing category`);
        return Result.fail(categoriesError);
      }
      categoriesObject.add(newCategory.getValue());
    }

    return Result.ok<Categories>(categoriesObject);
  }

  verifyLanguages(
    languages: string[]
  ): Result<Languages | string | PropertyError> {
    const languagesObject = Languages.create([]);
    const languagesError: PropertyError = {
      property: 'languages',
      error: []
    };
    if (!languages) return Result.ok<Languages>(languagesObject);
    for (const iterator of languages) {
      //TODO find with languages repository
      const newLanguage = Language.create({
        value: iterator
      });
      if (!newLanguage.isSuccess) {
        languagesError.error.push(`${iterator} is not an existing language`);
        return Result.fail(languagesError);
      }
      languagesObject.add(newLanguage.getValue());
    }

    return Result.ok<Languages>(languagesObject);
  }
}

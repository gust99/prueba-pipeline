import { Service } from '../../../serviceManagement/domain/service';
import { UniqueEntityID } from '../../../shared/domain/uniqueEntityID';
import { UserId } from '../../../userManagement/domain/userId';
import { Address } from '../../domain/address';
import { Categories } from '../../domain/categories';
import { CourseId } from '../../domain/courseId';
import {
  CurrencyType,
  DiscountType,
  ExtraType,
  FeeType
} from '../../domain/enums';
import { ExtraPricing } from '../../domain/extraPricing';
import { Image } from '../../domain/image';
import { Languages } from '../../domain/languages';
import LocationOptions from '../../domain/locationOptions';
import { PublicationPackage } from '../../domain/package';
import { Publication } from '../../domain/publication/publication';
import { PublicationDate } from '../../domain/publication/publicationDate';
import { PublicationDescription } from '../../domain/publication/publicationDescription';
import { PublicationTitle } from '../../domain/publication/publicationTitle';
import { Rate } from '../../domain/rate';
import { RequestDiscountDTO } from '../../presentation/dto/requestDiscount.dto';
import { RequestExtraFeeDTO } from '../../presentation/dto/requestExtraFee.dto';
import { RequestPackageDTO } from '../../presentation/dto/requestPackage.dto';
import { ResponsePublicationDTO } from '../../presentation/dto/responsePublication.dto';

import { PublicationEntity } from '../../repository/publication.entity';

export class PublicationMap {
  static toPersistence(publication: Publication): PublicationEntity {
    const entity = new PublicationEntity();
    entity.id = publication.publicationId.id.toString();
    entity.title = publication.title.value;
    entity.description = publication.description.value;
    entity.description2 = publication.description2
      ? publication.description2.value
      : null;
    entity.date = publication.date.date;
    entity.status = publication.status;
    entity.rate = publication.rate.value;
    entity.currencyType = publication.rate.currency;
    entity.userId = publication.userId.id.toString();
    entity.image = publication.image.value;
    entity.categories = Categories.parseToString(publication.categories);
    entity.languages = Languages.parseToString(publication.languages);
    entity.courseId = publication.courseId.id.toString();
    entity.online = publication.locationOptions.online;
    entity.travel = publication.locationOptions.travel;
    entity.house = publication.locationOptions.house;
    entity.address = publication.address.value;
    entity.longitude = publication.address.longitude;
    entity.latitude = publication.address.latitude;

    entity.packageValueA = publication.packageA.value;
    entity.packageTypeA = publication.packageA.type;
    entity.rateA = publication.packageA.rate.value;
    entity.currencyTypeA = publication.packageA.rate.currency;

    entity.packageValueB = publication.packageB.value;
    entity.packageTypeB = publication.packageB.type;
    entity.rateB = publication.packageB.rate.value;
    entity.currencyTypeB = publication.packageB.rate.currency;

    entity.packageValueC = publication.packageC.value;
    entity.packageTypeC = publication.packageC.type;
    entity.rateC = publication.packageC.rate.value;
    entity.currencyTypeC = publication.packageC.rate.currency;

    entity.discountType = publication.discount
      .feeOrDiscountType as DiscountType;
    entity.discountRate = publication.discount.rate.value;
    entity.discountCurrency = publication.discount.rate.currency;
    entity.feeType = publication.extraFee.feeOrDiscountType as FeeType;
    entity.feeRate = publication.extraFee.rate.value;
    entity.feeCurrency = publication.extraFee.rate.currency;

    return entity;
  }
  static toDTO(data: Publication, course: Service): ResponsePublicationDTO {
    const publication = new ResponsePublicationDTO();
    publication.id = data.publicationId.id.toString();
    publication.courseId = data.courseId.id.toString();
    publication.courseTitle = course.props.serviceTitle.props.value;
    publication.userId = data.userId.id.toString();
    publication.title = data.title.value;
    publication.description = data.description.value;
    publication.description2 = data.description2 ? data.description2.value : '';
    publication.status = data.status;
    publication.rate = data.rate.value + ' ' + CurrencyType[data.rate.currency];
    publication.image = data.image.value;
    publication.categories = Categories.parseToArray(data.categories);
    publication.languages = Languages.parseToArray(data.languages);
    publication.online = data.locationOptions.online;
    publication.travel = data.locationOptions.travel;
    publication.house = data.locationOptions.house;
    publication.address = data.address.value;
    publication.longitude = data.address.longitude;
    publication.latitude = data.address.latitude;
    publication.packageA = {
      value: data.packageA.value,
      type: data.packageA.type,
      rate: data.packageA.rate.value,
      currencyType: data.packageA.rate.currency
    };
    publication.packageB = {
      value: data.packageB.value,
      type: data.packageB.type,
      rate: data.packageB.rate.value,
      currencyType: data.packageB.rate.currency
    };
    publication.packageC = {
      value: data.packageC.value,
      type: data.packageC.type,
      rate: data.packageC.rate.value,
      currencyType: data.packageC.rate.currency
    };

    publication.discount = {
      rate: data.discount.rate.value,
      currencyType: data.discount.rate.currency,
      discountType: data.discount.feeOrDiscountType as DiscountType
    };

    publication.extraFee = {
      rate: data.extraFee.rate.value,
      currencyType: data.extraFee.rate.currency,
      extraFeeType: data.extraFee.feeOrDiscountType as FeeType
    };
    publication.courseId = data.courseId.id.toString();
    publication.userId = data.userId.id.toString();
    publication.serviceRate = course.serviceRate;
    publication.serviceNumOfReviews = course.serviceNumOfReviews;
    return publication;
  }
  static toDomain(data: any): Publication {
    const title = PublicationTitle.create({ value: data.title });
    const description = PublicationDescription.create({
      value: data.description
    });

    const description2 = data.description2
      ? PublicationDescription.create({ value: data.description2 })
      : null;
    const date = PublicationDate.create({
      dateTimePublished: new Date(data.date)
    });
    const status = data.status;
    const rate = Rate.create({
      value: data.rate,
      currencyType: data.currencyType
    });
    const userId = UserId.create(new UniqueEntityID(data.userId));
    const courseId = CourseId.create(new UniqueEntityID(data.courseId));

    const image = Image.create({ value: data.image });
    const categories = Categories.create([]);
    const languages = Languages.create([]);

    const locationOptions = LocationOptions.create({
      online: data.online,
      house: data.house,
      travel: data.travel
    });

    const address = Address.create({
      value: data.address,
      latitude: data.latitude,
      longitude: data.longitude
    });

    const packageA = PublicationPackage.create({
      value: data.packageValueA,
      rate: Rate.create({
        value: data.rateA,
        currencyType: data.currencyTypeA
      }).getValue() as Rate,
      type: data.packageTypeA
    });

    const packageB = PublicationPackage.create({
      value: data.packageValueB,
      rate: Rate.create({
        value: data.rateB,
        currencyType: data.currencyTypeB
      }).getValue() as Rate,
      type: data.packageTypeB
    });

    const packageC = PublicationPackage.create({
      value: data.packageValueC,
      rate: Rate.create({
        value: data.rateC,
        currencyType: data.currencyTypec
      }).getValue() as Rate,
      type: data.packageTypeC
    });

    const discount = ExtraPricing.create({
      rate: Rate.create({
        value: data.discountRate,
        currencyType: data.discountCurrency
      }).getValue() as Rate,
      feeOrDiscountType: data.discountType,
      type: ExtraType.Discount
    });

    const extraFee = ExtraPricing.create({
      rate: Rate.create({
        value: data.feeRate,
        currencyType: data.feeCurrency
      }).getValue() as Rate,
      feeOrDiscountType: data.feeType,
      type: ExtraType.ExtraFee
    });

    const publication = Publication.create(
      {
        title: title.getValue() as PublicationTitle,
        description: description.getValue() as PublicationDescription,
        description2: description2
          ? (description2.getValue() as PublicationDescription)
          : null,
        date: date.getValue() as PublicationDate,
        status: status,
        rate: rate.getValue() as Rate,
        userId: userId.getValue(),
        image: image.getValue() as Image,
        categories: categories,
        languages: languages,
        courseId: courseId.getValue(),
        locationOptions: locationOptions.getValue(),
        address: address.getValue() as Address,
        packageA: packageA.getValue() as PublicationPackage,
        packageB: packageB.getValue() as PublicationPackage,
        packageC: packageC.getValue() as PublicationPackage,
        discount: discount.getValue() as ExtraPricing,
        extraFee: extraFee.getValue() as ExtraPricing
      },
      new UniqueEntityID(data.id)
    );

    return publication.getValue();
  }

  static toDomainFromDTO(data: any): Publication {
    const title = PublicationTitle.create({ value: data.title });
    const description = PublicationDescription.create({
      value: data.description
    });
    const description2 = PublicationDescription.create({
      value: data.description2
    });
    const date = PublicationDate.create({
      dateTimePublished: new Date(data.date)
    });
    const status = data.status;
    const rate = Rate.create({
      value: data.rate,
      currencyType: data.currencyType
    });
    const userId = UserId.create(new UniqueEntityID(data.userId));
    const courseId = CourseId.create(new UniqueEntityID(data.courseId));

    const image = Image.create({ value: data.image });
    const categories = Categories.create([]);
    const languages = Languages.create([]);

    const locationOptions = LocationOptions.create({
      online: data.online,
      house: data.house,
      travel: data.travel
    });

    const address = Address.create({
      value: data.address,
      latitude: data.latitude,
      longitude: data.longitude
    });

    const packageAData: RequestPackageDTO = data.packageA;
    const packageA = PublicationPackage.create({
      value: packageAData.value,
      rate: Rate.create({
        value: packageAData.rate,
        currencyType: packageAData.currencyType
      }).getValue() as Rate,
      type: packageAData.type
    });

    const packageBData: RequestPackageDTO = data.packageB;
    const packageB = PublicationPackage.create({
      value: packageBData.value,
      rate: Rate.create({
        value: packageBData.rate,
        currencyType: packageBData.currencyType
      }).getValue() as Rate,
      type: packageBData.type
    });

    const packageCData: RequestPackageDTO = data.packageC;
    const packageC = PublicationPackage.create({
      value: packageCData.value,
      rate: Rate.create({
        value: packageCData.rate,
        currencyType: packageCData.currencyType
      }).getValue() as Rate,
      type: packageCData.type
    });
    const discountData: RequestDiscountDTO = data.discount;
    const discount = ExtraPricing.create({
      rate: Rate.create({
        value: discountData.rate,
        currencyType: discountData.currencyType
      }).getValue() as Rate,
      feeOrDiscountType: discountData.discountType,
      type: ExtraType.Discount
    });

    const extraFeeData: RequestExtraFeeDTO = data.extraFee;
    const extraFee = ExtraPricing.create({
      rate: Rate.create({
        value: extraFeeData.rate,
        currencyType: extraFeeData.currencyType
      }).getValue() as Rate,
      feeOrDiscountType: extraFeeData.extraFeeType,
      type: ExtraType.ExtraFee
    });

    const publication = Publication.create(
      {
        title: title.getValue() as PublicationTitle,
        description: description.getValue() as PublicationDescription,
        date: date.getValue() as PublicationDate,
        status: status,
        rate: rate.getValue() as Rate,
        userId: userId.getValue(),
        image: image.getValue() as Image,
        categories: categories,
        languages: languages,
        courseId: courseId.getValue(),
        locationOptions: locationOptions.getValue(),
        address: address.getValue() as Address,
        packageA: packageA.getValue() as PublicationPackage,
        packageB: packageB.getValue() as PublicationPackage,
        packageC: packageC.getValue() as PublicationPackage,
        discount: discount.getValue() as ExtraPricing,
        extraFee: extraFee.getValue() as ExtraPricing,
        description2: description2.getValue() as PublicationDescription
      },
      new UniqueEntityID(data.id)
    );

    return publication.getValue();
  }
}

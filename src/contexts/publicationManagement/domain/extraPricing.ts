import { Guard } from '../../shared/domain/guard';
import { Result } from '../../shared/domain/result';
import { Rate } from './rate';
import { DiscountType, ExtraType, FeeType } from './enums';
import { ErrorHelper, PropertyError } from '../../shared/domain/propertyError';
import { ValueObject } from '../../shared/domain/valueObject';

export interface ExtraProps {
  rate: Rate;
  feeOrDiscountType: FeeType | DiscountType;
  type: ExtraType;
}

export class ExtraPricing extends ValueObject<ExtraProps> {
  public static propName = 'extraPricing';
  constructor(props: ExtraProps) {
    super(props);
  }
  get rate(): Rate {
    return this.props.rate;
  }

  get feeOrDiscountType(): FeeType | DiscountType {
    return this.props.feeOrDiscountType;
  }

  get type(): ExtraType {
    return this.props.type;
  }
  static create(props: ExtraProps): Result<ExtraPricing | PropertyError> {
    const extraPricingErrors: PropertyError = {
      property: this.propName,
      value: {
        extraType: props.type,
        rate: props.rate.value,
        currencyType: props.rate.currency,
        feeOrDiscountType: props.feeOrDiscountType
      },
      error: []
    };

    const rateOrError = Rate.create(props.rate.props);
    if (!rateOrError.isSuccess) {
      const errors = rateOrError.error as PropertyError;
      for (const iterator of errors.error) {
        extraPricingErrors.error.push(iterator);
      }
    }
    const validExtraTypes = Object.values(ExtraType);
    const isValidExtraType = Guard.isOneOf(
      props.type,
      validExtraTypes,
      'extraType'
    );
    if (!isValidExtraType.succeeded) {
      extraPricingErrors.error.push(isValidExtraType.message);
    }

    if (props.type === ExtraType.Discount) {
      const validDiscountTypes = Object.values(DiscountType);
      const isValidDiscountType = Guard.isOneOf(
        props.feeOrDiscountType,
        validDiscountTypes,
        'extraType'
      );
      if (!isValidDiscountType.succeeded) {
        extraPricingErrors.error.push(isValidDiscountType.message);
      }
    }

    if (props.type === ExtraType.ExtraFee) {
      const validFeeTypes = Object.values(FeeType);
      const isValidFeeType = Guard.isOneOf(
        props.feeOrDiscountType,
        validFeeTypes,
        'feeType'
      );
      if (!isValidFeeType.succeeded) {
        extraPricingErrors.error.push(isValidFeeType.message);
      }
    }

    if (ErrorHelper.propertyHasErrors(extraPricingErrors))
      return Result.fail<PropertyError>(extraPricingErrors);

    const extraPricing = new ExtraPricing(props);
    return Result.ok<ExtraPricing>(extraPricing);
  }
}

import { Guard } from '../../shared/domain/guard';
import { Result } from '../../shared/domain/result';
import { Entity } from '../../shared/domain/entity';

export interface LanguageProps {
  value: string;
}

export class Language extends Entity<LanguageProps> {
  public static propName = 'language';
  public static defaultLanguage = 'NA';
  constructor(props: LanguageProps) {
    super(props);
  }
  get value(): string {
    return this.props.value;
  }

  static create(props: LanguageProps): Result<Language> {
    const nullGuardResult = Guard.againstNullOrUndefined(
      props.value,
      this.propName
    );

    if (!nullGuardResult.succeeded) {
      props.value = this.defaultLanguage;
    }

    const language = new Language(props);
    return Result.ok<Language>(language);
  }
}

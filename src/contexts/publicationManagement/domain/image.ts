import { Guard } from '../../shared/domain/guard';
import { PropertyError } from '../../shared/domain/propertyError';
import { Result } from '../../shared/domain/result';
import { ValueObject } from '../../shared/domain/valueObject';

export interface ImageProps {
  value: string;
}

export class Image extends ValueObject<ImageProps> {
  public static propName = 'image';
  public static defaultImage = '';
  constructor(props: ImageProps) {
    super(props);
  }
  get value(): string {
    return this.props.value;
  }

  static create(props: ImageProps): Result<Image | PropertyError> {
    const nullGuardResult = Guard.againstNullOrUndefined(
      props.value,
      this.propName
    );

    if (!nullGuardResult.succeeded) {
      props.value = this.defaultImage;
    }

    const image = new Image(props);
    return Result.ok<Image>(image);
  }
}

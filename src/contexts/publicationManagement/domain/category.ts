import { Guard } from '../../shared/domain/guard';
import { Result } from '../../shared/domain/result';
import { Entity } from '../../shared/domain/entity';

export interface CategoryProps {
  value: string;
}

export class Category extends Entity<CategoryProps> {
  public static propName = 'category';
  public static defaultCategory = 'NA';
  constructor(props: CategoryProps) {
    super(props);
  }
  get value(): string {
    return this.props.value;
  }

  static create(props: CategoryProps): Result<Category> {
    const nullGuardResult = Guard.againstNullOrUndefined(
      props.value,
      this.propName
    );

    if (!nullGuardResult.succeeded) {
      props.value = this.defaultCategory;
    }

    const category = new Category(props);
    return Result.ok<Category>(category);
  }
}

import { Result } from '../../../shared/domain/result';
import { AggregateRoot } from '../../../shared/domain/agregateRoot';
import { UniqueEntityID } from '../../../shared/domain/uniqueEntityID';
import { UserId } from '../../../userManagement/domain/userId';
import { DiscountType, ExtraType, FeeType, PublicationStatus } from '../enums';
import { Image } from '../image';
import { PublicationDate } from './publicationDate';
import { PublicationDescription } from './publicationDescription';
import { PublicationTitle } from './publicationTitle';
import { Rate } from '../rate';
import { PublicationId } from './publicationId';
import { Guard, IGuardArgument } from '../../../shared/domain/guard';
import { CourseId } from '../courseId';
import { Categories } from '../categories';
import { Languages } from '../languages';
import LocationOptions from '../locationOptions';
import { Address } from '../address';
import { PublicationPackage } from '../package';
import { ExtraPricing } from '../extraPricing';
import { ServiceId } from '../../../serviceManagement/domain/serviceId';

export interface PublicationProps {
  title: PublicationTitle;
  description: PublicationDescription;
  description2?: PublicationDescription;
  date?: PublicationDate;
  status?: PublicationStatus;
  rate?: Rate;
  userId: UserId;
  categories?: Categories;
  courseId: ServiceId;
  image?: Image;
  languages?: Languages;
  locationOptions?: LocationOptions;
  address?: Address;
  packageA?: PublicationPackage;
  packageB?: PublicationPackage;
  packageC?: PublicationPackage;
  extraFee?: ExtraPricing;
  discount?: ExtraPricing;
}

export class Publication extends AggregateRoot<PublicationProps> {
  public static entityName = 'publication';
  constructor(props: PublicationProps, id?: UniqueEntityID) {
    super(props, id);
  }

  get courseId(): CourseId {
    return this.props.courseId;
  }
  get publicationId(): PublicationId {
    return PublicationId.create(this._id).getValue();
  }

  get title(): PublicationTitle {
    return this.props.title;
  }

  get description(): PublicationDescription {
    return this.props.description;
  }

  get description2(): PublicationDescription {
    return this.props.description2;
  }

  get date(): PublicationDate {
    return this.props.date;
  }

  get status(): PublicationStatus {
    return this.props.status;
  }

  get rate(): Rate {
    return this.props.rate;
  }

  get userId(): UserId {
    return this.props.userId;
  }

  get image(): Image {
    return this.props.image;
  }

  get categories(): Categories {
    return this.props.categories;
  }

  get languages(): Languages {
    return this.props.languages;
  }

  get locationOptions(): LocationOptions {
    return this.props.locationOptions;
  }

  get address(): Address {
    return this.props.address;
  }

  get packageA(): PublicationPackage {
    return this.props.packageA;
  }

  get packageB(): PublicationPackage {
    return this.props.packageB;
  }

  get packageC(): PublicationPackage {
    return this.props.packageC;
  }

  get extraFee(): ExtraPricing {
    return this.props.extraFee;
  }

  get discount(): ExtraPricing {
    return this.props.discount;
  }

  public updateTitle(title: PublicationTitle) {
    this.props.title = title;
    return Result.ok<void>();
  }
  public updateDescription(description: PublicationDescription) {
    this.props.description = description;
    return Result.ok<void>();
  }
  public updateDescription2(description: PublicationDescription) {
    this.props.description2 = description;
    return Result.ok<void>();
  }
  public updateStatus(status: PublicationStatus) {
    this.props.status = status;
    return Result.ok<void>();
  }

  public updateRate(rate: Rate) {
    this.props.rate = rate;
    return Result.ok<void>();
  }

  public updateImage(image: Image) {
    this.props.image = image;
    return Result.ok<void>();
  }

  public updateCategories(categories: Categories) {
    this.props.categories = categories;
    return Result.ok<void>();
  }

  public updateLanguages(languages: Languages) {
    this.props.languages = languages;
    return Result.ok<void>();
  }

  public updateLocationOptions(locationOptions: LocationOptions) {
    this.props.locationOptions = locationOptions;
    return Result.ok<void>();
  }

  public updateAddress(address: Address) {
    this.props.address = address;
    return Result.ok<void>();
  }

  public updatePackageA(pubPackage: PublicationPackage) {
    this.props.packageA = pubPackage;
    return Result.ok<void>();
  }

  public updatePackageB(pubPackage: PublicationPackage) {
    this.props.packageB = pubPackage;
    return Result.ok<void>();
  }
  public updatePackageC(pubPackage: PublicationPackage) {
    this.props.packageC = pubPackage;
    return Result.ok<void>();
  }

  public updateExtraFees(extraFees: ExtraPricing) {
    this.props.extraFee = extraFees;
    return Result.ok<void>();
  }

  public updateDiscounts(discounts: ExtraPricing) {
    this.props.discount = discounts;
    return Result.ok<void>();
  }

  public static isValidStatus(status: PublicationStatus) {
    const validPublicationStatus = Object.values(PublicationStatus);
    const isValidPublicationStatus = Guard.isOneOf(
      status,
      validPublicationStatus,
      'status'
    );
    if (!isValidPublicationStatus.succeeded)
      return Result.fail(isValidPublicationStatus.message);
    return Result.ok<void>();
  }

  public static create(
    props: PublicationProps,
    id?: UniqueEntityID
  ): Result<any> {
    const guardArgs: IGuardArgument[] = [
      { argument: props.title, argumentName: 'title' },
      { argument: props.description, argumentName: 'description' },
      { argument: props.userId, argumentName: 'userId' },
      { argument: props.courseId, argumentName: 'courseId' }
    ];
    const guardResult = Guard.againstNullOrUndefinedBulk(guardArgs);

    if (!guardResult.succeeded) {
      return Result.fail(guardResult.errors);
    }

    const defaultValues: PublicationProps = {
      ...props,
      date: PublicationDate.create({
        dateTimePublished: new Date()
      }).getValue() as PublicationDate,
      rate: props.rate
        ? props.rate
        : (Rate.create({ value: 0, currencyType: 0 }).getValue() as Rate),
      status: props.status ? props.status : 0,
      image: props.image
        ? props.image
        : (Image.create({ value: '' }).getValue() as Image),
      categories: props.categories ? props.categories : Categories.create([]),
      languages: props.languages ? props.languages : Languages.create([]),
      locationOptions: props.locationOptions
        ? props.locationOptions
        : LocationOptions.create({
            online: true,
            travel: false,
            house: false
          }).getValue(),
      address: props.address
        ? props.address
        : (Address.create({
            value: Address.defaultAddress,
            latitude: Address.defaultLatitude,
            longitude: Address.defaultLongitude
          }).getValue() as Address),
      packageA: props.packageA
        ? props.packageA
        : (PublicationPackage.create({
            value: '0',
            rate: Rate.create({ value: 0, currencyType: 0 }).getValue() as Rate,
            type: 1
          }).getValue() as PublicationPackage),
      packageB: props.packageB
        ? props.packageB
        : (PublicationPackage.create({
            value: '0',
            rate: Rate.create({ value: 0, currencyType: 0 }).getValue() as Rate,
            type: 1
          }).getValue() as PublicationPackage),
      packageC: props.packageC
        ? props.packageC
        : (PublicationPackage.create({
            value: '0',
            rate: Rate.create({ value: 0, currencyType: 0 }).getValue() as Rate,
            type: 1
          }).getValue() as PublicationPackage),
      discount: props.discount
        ? props.discount
        : (ExtraPricing.create({
            rate: Rate.create({ value: 0, currencyType: 0 }).getValue() as Rate,
            feeOrDiscountType: DiscountType.None,
            type: ExtraType.Discount
          }).getValue() as ExtraPricing),
      extraFee: props.extraFee
        ? props.extraFee
        : (ExtraPricing.create({
            rate: Rate.create({ value: 0, currencyType: 0 }).getValue() as Rate,
            feeOrDiscountType: FeeType.None,
            type: ExtraType.ExtraFee
          }).getValue() as ExtraPricing)
    };

    const isNewPublication = !!id === false;

    if (isNewPublication) {
      const publication = new Publication(defaultValues);
      return Result.ok<Publication>(publication);
    }

    const publication = new Publication(defaultValues, id);
    return Result.ok<Publication>(publication);
  }
}

import { Guard } from '../../../shared/domain/guard';
import {
  PropertyError,
  ErrorHelper
} from '../../../shared/domain/propertyError';
import { Result } from '../../../shared/domain/result';
import { ValueObject } from '../../../shared/domain/valueObject';

interface PublicationTitleProps {
  value: string;
}
export class PublicationTitle extends ValueObject<PublicationTitleProps> {
  public static propName = 'title';

  public static minLength = 10;
  public static maxLength = 50;

  private constructor(props: PublicationTitleProps) {
    super(props);
  }

  get value(): string {
    return this.props.value;
  }

  public static create(
    props: PublicationTitleProps
  ): Result<PublicationTitle | PropertyError> {
    const titleErrors: PropertyError = {
      property: this.propName,
      value: { value: props.value },
      error: []
    };
    const nullGuardResult = Guard.againstNullOrUndefined(
      props.value,
      this.propName
    );

    if (!nullGuardResult.succeeded) {
      titleErrors.value = null;
      titleErrors.error.push(nullGuardResult.message);
      return Result.fail<PropertyError>(titleErrors);
    }

    const minGuardResult = Guard.againstAtLeast(
      this.minLength,
      props.value,
      this.propName
    );
    const maxGuardResult = Guard.againstAtMost(
      this.maxLength,
      props.value,
      this.propName
    );

    if (!minGuardResult.succeeded)
      titleErrors.error.push(minGuardResult.message);

    if (!maxGuardResult.succeeded)
      titleErrors.error.push(maxGuardResult.message);

    if (ErrorHelper.propertyHasErrors(titleErrors))
      return Result.fail<PropertyError>(titleErrors);

    return Result.ok<PublicationTitle>(new PublicationTitle(props));
  }
}

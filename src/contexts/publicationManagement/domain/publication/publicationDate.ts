import { Guard } from '../../../shared/domain/guard';
import { PropertyError } from '../../../shared/domain/propertyError';
import { Result } from '../../../shared/domain/result';
import { ValueObject } from '../../../shared/domain/valueObject';

interface PublicationDateProps {
  dateTimePublished: Date;
}
export class PublicationDate extends ValueObject<PublicationDateProps> {
  public static propName = 'date';

  constructor(props: PublicationDateProps) {
    super(props);
  }

  get date(): Date {
    return this.props.dateTimePublished;
  }

  public static create(
    props: PublicationDateProps
  ): Result<PublicationDate | PropertyError> {
    const dateErrors: PropertyError = {
      property: this.propName,
      value: { date: props.dateTimePublished },
      error: []
    };
    const nullGuardResult = Guard.againstNullOrUndefined(
      props.dateTimePublished,
      this.propName
    );

    if (!nullGuardResult.succeeded) {
      props.dateTimePublished = new Date();
    }

    const isDateResult = Guard.isValidDate(
      props.dateTimePublished,
      this.propName
    );

    if (!isDateResult.succeeded) {
      dateErrors.error.push(isDateResult.message);
      return Result.fail<PropertyError>(dateErrors);
    }
    return Result.ok<PublicationDate>(new PublicationDate(props));
  }
}

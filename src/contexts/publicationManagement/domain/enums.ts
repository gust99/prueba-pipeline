enum CurrencyType {
  USD,
  Bs
}
enum LocationType {
  Online,
  Travel,
  House
}
enum PublicationStatus {
  Public,
  Private
}

enum PackageType {
  None,
  PerHour,
  PerDescription
}

enum FeeType {
  None,
  Transportation
}

enum DiscountType {
  None,
  FirstClassFree
}

enum ExtraType {
  None,
  ExtraFee,
  Discount
}

export {
  CurrencyType,
  LocationType,
  PublicationStatus,
  PackageType,
  FeeType,
  DiscountType,
  ExtraType
};

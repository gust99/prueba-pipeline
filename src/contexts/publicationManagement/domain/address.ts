import { Guard } from '../../shared/domain/guard';
import { PropertyError } from '../../shared/domain/propertyError';
import { Result } from '../../shared/domain/result';
import { ValueObject } from '../../shared/domain/valueObject';

export interface AddressProps {
  value: string;
  latitude: string;
  longitude: string;
}
export class Address extends ValueObject<AddressProps> {
  public static propName = 'address';
  public static defaultAddress =
    'Calle IV Centenario, Jaime Zudañez, Cochabamba, Bolivia';
  public static defaultLatitude = '-17.40764943278393';
  public static defaultLongitude = '-66.13803863525392';

  constructor(props: AddressProps) {
    super(props);
  }

  get value(): string {
    return this.props.value;
  }

  get latitude(): string {
    return this.props.latitude;
  }

  get longitude(): string {
    return this.props.longitude;
  }

  static create(props: AddressProps): Result<Address | PropertyError> {
    const nullGuardResultValue = Guard.againstNullOrUndefined(
      props.value,
      'address'
    );

    if (!nullGuardResultValue.succeeded) {
      props.value = this.defaultAddress;
    }
    const nullGuardResultLatitude = Guard.againstNullOrUndefined(
      props.latitude,
      'latitude'
    );

    if (!nullGuardResultLatitude.succeeded) {
      props.latitude = this.defaultLatitude;
    }

    const nullGuardResultLongitude = Guard.againstNullOrUndefined(
      props.longitude,
      'longitude'
    );

    if (!nullGuardResultLongitude.succeeded) {
      props.longitude = this.defaultLongitude;
    }

    const image = new Address(props);
    return Result.ok<Address>(image);
  }
}

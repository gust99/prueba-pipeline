import { Result } from '../../shared/domain/result';
import { Entity } from '../../shared/domain/entity';
import { UniqueEntityID } from '../../shared/domain/uniqueEntityID';

export class CourseId extends Entity<any> {
  get id(): UniqueEntityID {
    return this._id;
  }

  private constructor(id?: UniqueEntityID) {
    super(null, id);
  }

  public static create(id?: UniqueEntityID): Result<CourseId> {
    return Result.ok<CourseId>(new CourseId(id));
  }
}

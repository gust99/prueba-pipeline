import { Guard } from '../../shared/domain/guard';
import { PropertyError, ErrorHelper } from '../../shared/domain/propertyError';
import { Result } from '../../shared/domain/result';
import { ValueObject } from '../../shared/domain/valueObject';
import { CurrencyType } from './enums';

interface RateProps {
  value: number;
  currencyType: CurrencyType;
}
export class Rate extends ValueObject<RateProps> {
  public static propName = 'rate';

  public static defaultRate = 0;
  public static defaultCurrency = CurrencyType.USD;
  constructor(props: RateProps) {
    super(props);
  }

  get value(): number {
    return this.props.value;
  }

  get currency(): CurrencyType {
    return this.props.currencyType;
  }

  public static create(props: RateProps): Result<Rate | PropertyError> {
    const rateErrors: PropertyError = {
      property: this.propName,
      value: { value: props.value, currencyType: props.currencyType },
      error: []
    };
    const nullGuardResultForValue = Guard.againstNullOrUndefined(
      props.value,
      'value'
    );
    const nullGuardResultForCurrency = Guard.againstNullOrUndefined(
      props.currencyType,
      'currencyType'
    );

    if (!nullGuardResultForValue.succeeded) props.value = this.defaultRate;

    if (!nullGuardResultForCurrency.succeeded)
      props.currencyType = this.defaultCurrency;

    const validCurrencyTypes = Object.values(CurrencyType);
    const isValidCurrencyType = Guard.isOneOf(
      props.currencyType,
      validCurrencyTypes,
      'currencyType'
    );
    if (!isValidCurrencyType.succeeded) {
      rateErrors.error.push(isValidCurrencyType.message);
    }

    if (ErrorHelper.propertyHasErrors(rateErrors))
      return Result.fail<PropertyError>(rateErrors);

    //return Result.fail<PropertyError>({});
    const rate = new Rate(props);

    return Result.ok<Rate>(rate);
  }
}

import { WatchedList } from '../../shared/domain/watchedList';
import { Language } from './language';

export class Languages extends WatchedList<Language> {
  private constructor(initialLanguages: Language[]) {
    super(initialLanguages);
  }

  public compareItems(a: Language, b: Language): boolean {
    return a.equals(b);
  }
  public static parseToString(languages: Languages): string {
    let result = '';
    for (const iterator of languages.getItems()) {
      result += iterator.value + ',';
    }
    if (result != '') return result.slice(0, -1);
    return result;
  }

  public static parseToArray(languages: Languages): string[] {
    const result = [];
    for (const iterator of languages.getItems()) {
      result.push(iterator.value);
    }
    return result;
  }

  public static create(initialLanguages?: Language[]): Languages {
    return new Languages(initialLanguages ? initialLanguages : []);
  }
}

import { Guard } from '../../shared/domain/guard';
import { Result } from '../../shared/domain/result';
import { ValueObject } from '../../shared/domain/valueObject';

export interface LocationOptionsProps {
  online: boolean;
  travel: boolean;
  house: boolean;
}
export default class LocationOptions extends ValueObject<LocationOptionsProps> {
  public static propName = 'locationOptions';
  public static defaultOnline = false;
  public static defaultTravel = false;
  public static defaultHouse = false;

  get online() {
    return this.props.online;
  }
  get travel() {
    return this.props.travel;
  }

  get house() {
    return this.props.house;
  }

  set updateOnline(value: boolean) {
    this.props.online = value;
  }
  set updateTravel(value: boolean) {
    this.props.travel = value;
  }
  set updateHouse(value: boolean) {
    this.props.house = value;
  }

  constructor(props: LocationOptionsProps) {
    super(props);
  }
  static create(props: LocationOptionsProps): Result<LocationOptions> {
    const nullGuardResultForOnline = Guard.againstNullOrUndefined(
      props.online,
      'online'
    );
    const nullGuardResultForTravel = Guard.againstNullOrUndefined(
      props.travel,
      'travel'
    );
    const nullGuardResultForHouse = Guard.againstNullOrUndefined(
      props.house,
      'house'
    );

    if (!nullGuardResultForOnline.succeeded) props.online = this.defaultOnline;
    if (!nullGuardResultForTravel.succeeded) props.travel = this.defaultTravel;
    if (!nullGuardResultForHouse.succeeded) props.house = this.defaultHouse;

    const locationOptions = new LocationOptions(props);
    return Result.ok<LocationOptions>(locationOptions);
  }
}

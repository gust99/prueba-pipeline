import { Guard } from '../../shared/domain/guard';
import { ErrorHelper, PropertyError } from '../../shared/domain/propertyError';
import { Result } from '../../shared/domain/result';
import { ValueObject } from '../../shared/domain/valueObject';
import { PackageType } from './enums';
import { Rate } from './rate';

export interface PackageProps {
  value: string;
  rate: Rate;
  type: PackageType;
}

export class PublicationPackage extends ValueObject<PackageProps> {
  public static propName = 'PublicationPackage';
  public static defaultValue = '';
  public static defaultRate = Rate.create({ value: 0, currencyType: 0 });
  public static defaultPackageType = 0;
  constructor(props: PackageProps) {
    super(props);
  }
  get value(): string {
    return this.props.value;
  }
  get rate(): Rate {
    return this.props.rate;
  }

  get type(): PackageType {
    return this.props.type;
  }

  static create(
    props: PackageProps
  ): Result<PublicationPackage | PropertyError> {
    const packageErrors: PropertyError = {
      property: this.propName,
      value: {
        value: props.value,
        rate: props.rate.value,
        currencyType: props.rate.currency,
        packageType: props.type
      },
      error: []
    };
    const nullGuardResultForValue = Guard.againstNullOrUndefined(
      props.value,
      'value'
    );
    const nullGuardResultForCurrency = Guard.againstNullOrUndefined(
      props.type,
      'packageType'
    );

    if (!nullGuardResultForValue.succeeded) props.value = this.defaultValue;

    if (!nullGuardResultForCurrency.succeeded)
      props.type = this.defaultPackageType;

    const rateOrError = Rate.create(props.rate.props);
    if (!rateOrError.isSuccess) {
      const errors = rateOrError.error as PropertyError;
      for (const iterator of errors.error) {
        packageErrors.error.push(iterator);
      }
    }
    if (ErrorHelper.propertyHasErrors(packageErrors))
      return Result.fail<PropertyError>(packageErrors);

    const publicationPackage = new PublicationPackage(props);
    return Result.ok<PublicationPackage>(publicationPackage);
  }
}

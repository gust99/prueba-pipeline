import { inject, injectable } from 'inversify';
import { Result } from '../../shared/domain/result';
import { TYPES } from '../../../types';
import { User, UserRole } from '../domain/user';
import { UserDateOfBirth } from '../domain/valueObjects/userDateOfBirth';
import { UserEmail } from '../domain/valueObjects/userEmail';
import { UserFirstName } from '../domain/valueObjects/userFirstName';
import { UserPassword } from '../domain/valueObjects/userPassword';
import { CreateUserRequestDto } from '../presentation/dtos/createUserRequestDto';
import { UpdateUserRequestDto } from '../presentation/dtos/updateUserRequestDto';
import { UserMap } from '../presentation/userMap';
import { UserLastName } from '../domain/valueObjects/userLastName';
import { Response } from '../../shared/domain/response';
import { ICryptoService } from '../../shared/repository/ICryptoService';
import { UserTypeOrmRepository } from '../repository/userRepository';
import { UpdateUserRepositoryRequest } from '../repository/updateUserRepositoryRequest';
import { Guard, IGuardArgument } from '../../shared/domain/guard';
import { PropertyError } from '../../shared/domain/propertyError';
import { EmailService } from '../../../shared/emailSender/service/emailService';
import { UserEntity } from '../repository/user.entity';
import { getConnectionManager, getRepository } from 'typeorm';

@injectable()
export class UserService {
  constructor(
    @inject(TYPES.UserRepository)
    private readonly userRepository: UserTypeOrmRepository,
    @inject(TYPES.CryptoService)
    private readonly cryptoService: ICryptoService,
    @inject(TYPES.EmailService) private emailService: EmailService
  ) {}

  async findMentors(): Promise<any> {
    try {
      const usersRaw = await this.userRepository.findByQuery({
        userRol: UserRole.MENTOR
      });
      const users = usersRaw.map((user) => {
        const userFound = UserMap.toDomain(user);
        const userDTO = UserMap.toResponseUserDto(userFound);
        return userFound;
      });
      return Response.ok('Listing all mentors', users);
    } catch (error) {
      console.log(error);
      return Response.internalServerError();
    }
  }

  async findAll(): Promise<Response<any>> {
    try {
      const usersRaw = await this.userRepository.findAll();
      const users = usersRaw.map((user) => {
        const userFound = UserMap.toDomain(user);
        const userDTO = UserMap.toResponseUserDto(userFound);
        return userFound;
      });
      return Response.ok('Listing all users', users);
    } catch (error) {
      console.log(error);
      return Response.internalServerError();
    }
  }
  async createUser(request: CreateUserRequestDto): Promise<Response<any>> {
    const userFirstNameOrError = UserFirstName.create({
      value: request.firstName
    });
    const userLastNameOrError = UserLastName.create({
      value: request.lastName
    });
    const userDateOfBirthOrError = UserDateOfBirth.create({
      value: request.dateOfBirth
    });
    const emailOrError = UserEmail.create({ value: request.email });
    const userPasswordOrError = UserPassword.create(
      {
        value: request.password
      },
      this.cryptoService.getHashedValue
    );

    const dtoResult = Result.combine([
      userFirstNameOrError,
      userLastNameOrError,
      userDateOfBirthOrError,
      emailOrError,
      userPasswordOrError
    ]);
    if (dtoResult.isFailure) {
      return Response.badRequest('Invalid data.', dtoResult.errorValue());
    }

    const userFirstName = userFirstNameOrError.getValue() as UserFirstName;
    const userLastName = userLastNameOrError.getValue() as UserLastName;
    const userDateOfBirth =
      userDateOfBirthOrError.getValue() as UserDateOfBirth;
    const userEmail = emailOrError.getValue() as UserEmail;
    const userPassword = userPasswordOrError.getValue() as UserPassword;

    const userOrError = User.create(
      {
        userFirstName,
        userLastName,
        userDateOfBirth,
        userEmail,
        latitude: request.address?.latitude,
        longitude: request.address?.longitude,
        totalAddress: request.address?.totalAddress,
        building: request.address?.building,
        street: request.address?.street,
        neighbourhood: request.address?.neighbourhood,
        city: request.address?.city,
        state: request.address?.state,
        country: request.address?.country,
        phoneNumber: request.phone,
        userPassword,
        facebook: request.social?.facebook,
        linkedin: request.social?.linkedin,
        website: request.social?.website,
        youtube: request.social?.youtube,
        googlehangouts: request.social?.googlehangouts,
        twitter: request.social?.twitter,
        profileImage: request.profileImage
      },
      request.userId
    );
    if (userOrError.isFailure) {
      return Response.badRequest('Invalid data', userOrError.errorValue());
    }
    const user: User = userOrError.getValue();
    try {
      const userEmailExist: boolean = await this.userRepository.existEmail(
        userEmail.value
      );
      if (userEmailExist) {
        return Response.conflict(
          `User with email '${userEmail.value}' already registered.`
        );
      }
      const userResult = await this.userRepository.addUser(
        UserMap.toRepositoryEntity(user)
      );
      const userSaved = userResult.getValue();
      const userInformation = {
        email: userSaved.email.value,
        name: userSaved.userFirstName.value
      };
      this.emailService.sendMail(
        userInformation,
        'welcome.ejs',
        '👋  ¡Welcome to iMentor!'
      );
      return Response.ok(
        'User registered.',
        UserMap.toResponseUserDto(userResult.getValue())
      );
    } catch (error) {
      console.log(error);
      return Response.internalServerError();
    }
  }

  public async getUser(id: string): Promise<Response<any>> {
    try {
      const userOrError = await this.userRepository.findUser(id);
      if (userOrError.isFailure) {
        return Response.notFound(userOrError.errorValue() as string);
      }
      return Response.ok(
        'User found.',
        UserMap.toResponseUserDto(userOrError.getValue() as User)
      );
    } catch (error) {
      console.log(error);
      return Response.internalServerError();
    }
  }

  public async updateUser(
    id: string,
    data: UpdateUserRequestDto
  ): Promise<Response<any>> {
    console.log(id);
    console.log(data);
    const userOrError = await this.userRepository.findUser(id);
    if (userOrError.isFailure) {
      return Response.notFound(userOrError.errorValue() as string);
    }
    const infoToUpdate = UserMap.toPersistenceFromUpdateRequest(data);
    Object.keys(infoToUpdate).forEach((key) => {
      if (
        infoToUpdate[key as keyof UpdateUserRepositoryRequest] === undefined
      ) {
        delete infoToUpdate[key as keyof UpdateUserRepositoryRequest];
      }
    });

    const valueObjectValidationResults = [];
    const propsValidationResult: IGuardArgument[] = [];

    if (UserFirstName.propertyName in infoToUpdate) {
      valueObjectValidationResults.push(
        UserFirstName.create({
          value: infoToUpdate.firstName
        })
      );
    }

    if (UserLastName.propertyName in infoToUpdate) {
      valueObjectValidationResults.push(
        UserLastName.create({
          value: infoToUpdate.lastName
        })
      );
    }

    if (UserDateOfBirth.propertyName in infoToUpdate) {
      valueObjectValidationResults.push(
        UserDateOfBirth.create({
          value: infoToUpdate.dateOfBirth
        })
      );
    }

    if (UserEmail.propertyName in infoToUpdate) {
      valueObjectValidationResults.push(
        UserEmail.create({
          value: infoToUpdate.email
        })
      );
    }

    if (UserPassword.propertyName in infoToUpdate) {
      valueObjectValidationResults.push(
        UserPassword.create(
          {
            value: infoToUpdate.password
          },
          this.cryptoService.getHashedValue
        )
      );
    }

    if ('latitude' in infoToUpdate) {
      propsValidationResult.push({
        argument: infoToUpdate.latitude,
        argumentName: 'latitude',
        types: ['string']
      });
    }

    if ('longitude' in infoToUpdate) {
      propsValidationResult.push({
        argument: infoToUpdate.longitude,
        argumentName: 'longitude',
        types: ['string']
      });
    }

    if ('totalAddress' in infoToUpdate) {
      propsValidationResult.push({
        argument: infoToUpdate.totalAddress,
        argumentName: 'totalAddress',
        types: ['string']
      });
    }

    if ('building' in infoToUpdate) {
      propsValidationResult.push({
        argument: infoToUpdate.building,
        argumentName: 'building',
        types: ['string']
      });
    }

    if ('street' in infoToUpdate) {
      propsValidationResult.push({
        argument: infoToUpdate.street,
        argumentName: 'street',
        types: ['string']
      });
    }

    if ('neighbourhood' in infoToUpdate) {
      propsValidationResult.push({
        argument: infoToUpdate.neighbourhood,
        argumentName: 'neighbourhood',
        types: ['string']
      });
    }

    if ('city' in infoToUpdate) {
      propsValidationResult.push({
        argument: infoToUpdate.city,
        argumentName: 'city',
        types: ['string']
      });
    }

    if ('state' in infoToUpdate) {
      propsValidationResult.push({
        argument: infoToUpdate.state,
        argumentName: 'state',
        types: ['string']
      });
    }

    if ('country' in infoToUpdate) {
      propsValidationResult.push({
        argument: infoToUpdate.country,
        argumentName: 'country',
        types: ['string']
      });
    }

    if ('phone' in infoToUpdate) {
      propsValidationResult.push({
        argument: infoToUpdate.phone,
        argumentName: 'phone',
        types: ['string']
      });
    }

    if ('facebook' in infoToUpdate) {
      propsValidationResult.push({
        argument: infoToUpdate.facebook,
        argumentName: 'facebook',
        types: ['string']
      });
    }

    if ('linkedin' in infoToUpdate) {
      propsValidationResult.push({
        argument: infoToUpdate.linkedin,
        argumentName: 'linkedin',
        types: ['string']
      });
    }

    if ('website' in infoToUpdate) {
      propsValidationResult.push({
        argument: infoToUpdate.website,
        argumentName: 'website',
        types: ['string']
      });
    }

    if ('youtube' in infoToUpdate) {
      propsValidationResult.push({
        argument: infoToUpdate.youtube,
        argumentName: 'youtube',
        types: ['string']
      });
    }

    if ('googlehangouts' in infoToUpdate) {
      propsValidationResult.push({
        argument: infoToUpdate.googlehangouts,
        argumentName: 'googlehangouts',
        types: ['string']
      });
    }

    if ('twitter' in infoToUpdate) {
      propsValidationResult.push({
        argument: infoToUpdate.twitter,
        argumentName: 'twitter',
        types: ['string']
      });
    }

    if ('profileImage' in infoToUpdate) {
      propsValidationResult.push({
        argument: infoToUpdate.profileImage,
        argumentName: 'profileImage',
        types: ['string']
      });
    }

    const guardPropsResult = Guard.againstValueDifferentTypeBulk(
      propsValidationResult
    );

    if (!guardPropsResult.succeeded) {
      guardPropsResult.errors.forEach((error) => {
        valueObjectValidationResults.push(Result.fail<PropertyError>(error));
      });
    }

    const valueObjectValidationResult = Result.combine(
      valueObjectValidationResults
    );

    if (valueObjectValidationResult.isFailure) {
      return Response.badRequest(
        'Invalid data.',
        valueObjectValidationResult.errorValue()
      );
    }

    if ('email' in infoToUpdate) {
      const user = userOrError.getValue() as User;
      const actualEmail = user.email.value;
      infoToUpdate.email = infoToUpdate.email.trim().toLowerCase();
      try {
        const userEmailExist: boolean = await this.userRepository.existEmail(
          infoToUpdate.email
        );
        if (userEmailExist && infoToUpdate.email !== actualEmail) {
          return Response.conflict(
            `User with email '${infoToUpdate.email}' already registered.`
          );
        }
        infoToUpdate.isEmailVerified = false;
      } catch (error) {
        console.log(error);
        return Response.internalServerError();
      }
    }

    if ('password' in infoToUpdate) {
      infoToUpdate.password = this.cryptoService.getHashedValue(
        infoToUpdate.password
      );
    }

    try {
      await this.userRepository.update(id, infoToUpdate);
    } catch (error) {
      console.log(error);
      return Response.internalServerError();
    }

    return Response.ok('User updated.');
  }

  public async deleteUser(id: string) {
    try {
      const result = await this.userRepository.deleteUser(id);
      if (result.isFailure) {
        return Response.notFound(result.errorValue() as string);
      }
      return Response.ok('User deleted.');
    } catch (error) {
      console.log(error);
      return Response.internalServerError();
    }
  }
}

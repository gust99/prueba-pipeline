import { Expose, Type } from 'class-transformer';

class CertificationProfile {
  @Expose()
  nameCertification: string;
  @Expose()
  issuer: string;
  @Expose()
  issueDate: Date;
}
class WorkExperienceProfile {
  @Expose()
  title: string;
  @Expose()
  company: string;
  @Expose()
  location: string;
  @Expose()
  startDateWorkExperience: Date;
  @Expose()
  endDateWorkExperience: Date;
}
class EducationProfile {
  @Expose()
  institute: string;
  @Expose()
  degree: string;
  @Expose()
  startDateEducation: Date;
  @Expose()
  endDateEducation: Date;
}

export class ProfessionalProfile {
  @Expose()
  name: string;

  @Expose()
  aboutMe: string;

  @Type(() => CertificationProfile)
  @Expose()
  certification: CertificationProfile[];

  @Type(() => WorkExperienceProfile)
  @Expose()
  workExperience: WorkExperienceProfile[];

  @Expose()
  skills: string[];

  @Type(() => EducationProfile)
  @Expose()
  education: EducationProfile[];

  @Expose()
  languages: string[];
}

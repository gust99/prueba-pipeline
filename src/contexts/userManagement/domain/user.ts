import { UserFirstName } from './valueObjects/userFirstName';
import { UserId } from './userId';
import { UserEmail } from './valueObjects/userEmail';
import { AggregateRoot } from '../../shared/domain/agregateRoot';
import { UniqueEntityID } from '../../shared/domain/uniqueEntityID';
import { Result } from '../../shared/domain/result';
import { UserDateOfBirth } from './valueObjects/userDateOfBirth';
import { UserPassword } from './valueObjects/userPassword';
import { Guard } from '../../shared/domain/guard';
import { PropertyError } from '../../shared/domain/propertyError';
import { ProfessionalProfile } from './professionalProfile';

export enum UserRole {
  ADMIN = 'admin',
  STUDENT = 'student',
  MENTOR = 'mentor'
}

interface UserProps {
  userFirstName: UserFirstName;
  userLastName: UserFirstName;
  userDateOfBirth: UserDateOfBirth;
  userEmail: UserEmail;
  latitude?: string;
  longitude?: string;
  totalAddress?: string;
  building?: string;
  street?: string;
  neighbourhood?: string;
  city?: string;
  state?: string;
  country?: string;
  phoneNumber?: string;
  userPassword: UserPassword;
  facebook?: string;
  linkedin?: string;
  website?: string;
  youtube?: string;
  googlehangouts?: string;
  twitter?: string;
  profileImage?: string;
  isEmailVerified?: boolean;
  isAdminUser?: boolean;
  isDeleted?: boolean;
  lastLogin?: Date;
  userRol?: UserRole;
  professionalProfile?: ProfessionalProfile[];
  userRate?: number;
  userNumOfReviews?: number;
}

export class User extends AggregateRoot<UserProps> {
  get userId(): UserId {
    return UserId.create(this._id).getValue();
  }

  get userFirstName(): UserFirstName {
    return this.props.userFirstName;
  }

  get userLastName(): UserFirstName {
    return this.props.userLastName;
  }

  get dateOfBirth(): UserDateOfBirth {
    return this.props.userDateOfBirth;
  }

  get email(): UserEmail {
    return this.props.userEmail;
  }

  get latitude(): string {
    return this.props.latitude;
  }

  get longitude(): string {
    return this.props.longitude;
  }

  get totalAddress(): string {
    return this.props.totalAddress;
  }

  get building(): string {
    return this.props.building;
  }

  get street(): string {
    return this.props.street;
  }

  get neighbourhood(): string {
    return this.props.neighbourhood;
  }

  get city(): string {
    return this.props.city;
  }

  get state(): string {
    return this.props.state;
  }

  get country(): string {
    return this.props.country;
  }

  get phoneNumber(): string {
    return this.props.phoneNumber;
  }
  get password(): UserPassword {
    return this.props.userPassword;
  }

  get facebook(): string {
    return this.props.facebook;
  }
  get linkedin(): string {
    return this.props.linkedin;
  }
  get website(): string {
    return this.props.website;
  }

  get youtube(): string {
    return this.props.youtube;
  }

  get googlehangouts(): string {
    return this.props.googlehangouts;
  }

  get twitter(): string {
    return this.props.twitter;
  }

  get profileImage(): string {
    return this.props.profileImage;
  }

  get isEmailVerified(): boolean {
    return this.props.isEmailVerified;
  }

  get isAdminUser(): boolean {
    return this.props.isAdminUser;
  }

  get isDeleted(): boolean {
    return this.props.isDeleted;
  }

  get lastLogin(): Date {
    return this.props.lastLogin;
  }
  get userRol(): UserRole {
    return this.props.userRol;
  }
  get professionalProfile(): ProfessionalProfile[] {
    return this.props.professionalProfile;
  }
  get userRate(): number {
    return this.props.userRate;
  }
  get userNumOfReviews(): number {
    return this.props.userNumOfReviews;
  }
  public delete(): void {
    if (!this.props.isDeleted) {
      this.props.isDeleted = true;
    }
  }

  private constructor(props: UserProps, id?: UniqueEntityID) {
    super(props, id);
  }

  public static create(props: UserProps, id?: string): Result<any> {
    const guardValueObjectsResult = Guard.againstNullOrUndefinedBulk([
      { argument: props.userFirstName, argumentName: 'firstName' },
      { argument: props.userLastName, argumentName: 'lasName' },
      { argument: props.userEmail, argumentName: 'email' }
    ]);

    if (!guardValueObjectsResult.succeeded) {
      return Result.multipleFail<string>(guardValueObjectsResult.errors);
    }
    const arrayToValidate = [
      {
        argument: props.latitude,
        argumentName: 'latitude',
        types: ['undefined', 'string']
      },
      {
        argument: props.longitude,
        argumentName: 'longitude',
        types: ['undefined', 'string']
      },
      {
        argument: props.totalAddress,
        argumentName: 'totalAddress',
        types: ['undefined', 'string']
      },
      {
        argument: props.building,
        argumentName: 'building',
        types: ['undefined', 'string']
      },
      {
        argument: props.street,
        argumentName: 'street',
        types: ['undefined', 'string']
      },
      {
        argument: props.neighbourhood,
        argumentName: 'neighbourhood',
        types: ['undefined', 'string']
      },
      {
        argument: props.city,
        argumentName: 'city',
        types: ['undefined', 'string']
      },
      {
        argument: props.state,
        argumentName: 'state',
        types: ['undefined', 'string']
      },
      {
        argument: props.country,
        argumentName: 'country',
        types: ['undefined', 'string']
      },
      {
        argument: props.phoneNumber,
        argumentName: 'phoneNumber',
        types: ['undefined', 'string']
      },
      {
        argument: props.userRate,
        argumentName: 'userRate',
        types: ['undefined', 'number']
      },
      {
        argument: props.userNumOfReviews,
        argumentName: 'userNumbOfReviews',
        types: ['undefined', 'number']
      }
    ];

    if (id) {
      arrayToValidate.push({
        argument: id,
        argumentName: 'id',
        types: ['string']
      });
    }

    const guardPropsResult =
      Guard.againstValueDifferentTypeBulk(arrayToValidate);

    if (!guardPropsResult.succeeded) {
      return Result.multipleFail<PropertyError>(guardPropsResult.errors);
    }

    const user = new User(
      {
        ...props,
        latitude: props.latitude ? props.latitude : '-17.40764943278393',
        longitude: props.longitude ? props.longitude : '-66.13803863525392',
        totalAddress: props.totalAddress
          ? props.totalAddress
          : 'JalaSoft, Cochabamba, Bolivia',
        building: props.building ? props.building : 'Edificio Jalasoft',
        street: props.street ? props.street : '',
        neighbourhood: props.neighbourhood ? props.neighbourhood : '',
        city: props.city ? props.city : '',
        state: props.state ? props.state : '',
        country: props.country ? props.country : '',
        phoneNumber: props.phoneNumber ? props.phoneNumber : '',
        facebook: props.facebook ? props.facebook : 'https://www.facebook.com',
        linkedin: props.linkedin ? props.linkedin : 'https://www.linkedin.com',
        website: props.website ? props.website : 'https://www.mysite.com',
        youtube: props.youtube ? props.youtube : 'https://www.youtube.com',
        googlehangouts: props.googlehangouts
          ? props.googlehangouts
          : 'https://www.hangouts.com',
        twitter: props.twitter ? props.twitter : 'https://www.twitter.com',
        profileImage: props.profileImage ? props.profileImage : '',
        lastLogin: props.lastLogin ? props.lastLogin : new Date(),
        isDeleted: props.isDeleted ? props.isDeleted : false,
        isEmailVerified: props.isEmailVerified ? props.isEmailVerified : false,
        isAdminUser: props.isAdminUser ? props.isAdminUser : false,
        userRol: props.userRol ? props.userRol : UserRole.STUDENT,
        userRate: props.userRate ? props.userRate : 0,
        userNumOfReviews: props.userNumOfReviews ? props.userNumOfReviews : 0
      },
      new UniqueEntityID(id)
    );

    return Result.ok<User>(user);
  }
}

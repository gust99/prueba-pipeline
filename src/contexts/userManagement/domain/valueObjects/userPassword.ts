import { PropertyError } from '../../../shared/domain/propertyError';
import { Guard } from '../../../shared/domain/guard';
import { Result } from '../../../shared/domain/result';
import { ValueObject } from '../../../shared/domain/valueObject';

export interface IUserPasswordProps {
  value: string | undefined;
  propertyName?: string;
}

export class UserPassword extends ValueObject<IUserPasswordProps> {
  public static minLength = 8;
  public static propertyName = 'password';

  get value(): string {
    return this.props.value;
  }

  set value(hashedPassword: string) {
    this.props.value = hashedPassword;
  }

  get propertyName(): string {
    return this.props.propertyName;
  }

  private constructor(props: IUserPasswordProps) {
    super(props);
  }

  private static isAppropriateLength(password: string): boolean {
    return password.length >= this.minLength;
  }

  public static create(
    props: IUserPasswordProps,
    hashFunction: (text: string) => string
  ): Result<UserPassword | PropertyError> {
    const error: PropertyError = {
      property: this.propertyName,
      value: props.value,
      error: []
    };

    // const propsResult = Guard.againstNullOrUndefined(
    //   props.value,
    //   this.propertyName
    // );

    // if (!propsResult.succeeded) {
    //   error.error = [propsResult.message];
    //   return Result.fail<PropertyError>(error);
    // }

    const typeResult = Guard.againstValueDifferentType(
      props.value,
      this.propertyName,
      ['undefined', 'string']
    );

    if (!typeResult.succeeded) {
      error.error = [typeResult.message];
      return Result.fail<PropertyError>(error);
    }

    if (props.value) {
      if (!this.isAppropriateLength(props.value)) {
        error.error = ['Password does not meet criteria [8 chars min].'];
        return Result.fail<PropertyError>(error);
      }
    }

    return Result.ok<UserPassword>(
      new UserPassword({
        value: props.value ? hashFunction(props.value) : props.value
      })
    );
  }
}

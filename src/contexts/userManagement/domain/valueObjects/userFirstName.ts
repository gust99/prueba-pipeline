import { Result } from '../../../shared/domain/result';
import { Guard } from '../../../shared/domain/guard';
import { ValueObject } from '../../../shared/domain/valueObject';
import { PropertyError } from '../../../shared/domain/propertyError';

interface UserFirstNameProps {
  value: string;
  propertyName?: string;
}

export class UserFirstName extends ValueObject<UserFirstNameProps> {
  public static maxLength = 30;
  public static minLength = 2;
  public static propertyName = 'firstName';
  get value(): string {
    return this.props.value;
  }

  get propertyName(): string {
    return this.props.propertyName;
  }

  private constructor(props: UserFirstNameProps) {
    super(props);
  }

  public static create(
    props: UserFirstNameProps
  ): Result<UserFirstName | PropertyError> {
    const userNameResult = Guard.againstNullOrUndefined(
      props.value,
      this.propertyName
    );

    if (!userNameResult.succeeded) {
      return Result.fail<PropertyError>({
        property: this.propertyName,
        value: props.value,
        error: [userNameResult.message]
      });
    }

    const typeResult = Guard.againstValueDifferentType(
      props.value,
      this.propertyName,
      ['string']
    );

    if (!typeResult.succeeded) {
      return Result.fail<PropertyError>({
        property: this.propertyName,
        value: props.value,
        error: [typeResult.message]
      });
    }

    const minLengthResult = Guard.againstAtLeast(
      this.minLength,
      props.value,
      this.propertyName
    );
    if (!minLengthResult.succeeded) {
      return Result.fail<PropertyError>({
        property: this.propertyName,
        value: props.value,
        error: [minLengthResult.message]
      });
    }

    const maxLengthResult = Guard.againstAtMost(
      this.maxLength,
      props.value,
      this.propertyName
    );
    if (!maxLengthResult.succeeded) {
      return Result.fail<PropertyError>({
        property: this.propertyName,
        value: props.value,
        error: [maxLengthResult.message]
      });
    }
    props.propertyName = this.propertyName;
    return Result.ok<UserFirstName>(new UserFirstName(props));
  }
}

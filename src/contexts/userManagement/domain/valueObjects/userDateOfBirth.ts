import { Result } from '../../../shared/domain/result';
import { Guard } from '../../../shared/domain/guard';
import { ValueObject } from '../../../shared/domain/valueObject';
import { PropertyError } from '../../../shared/domain/propertyError';

interface DateOfBirthProps {
  value: Date | undefined | string;
  propertyName?: string;
}

export class UserDateOfBirth extends ValueObject<DateOfBirthProps> {
  public static propertyName = 'dateOfBirth';

  get value() {
    return this.props.value ? new Date(this.props.value) : this.props.value;
  }

  get propertyName(): string {
    return this.props.propertyName;
  }

  private constructor(props: DateOfBirthProps) {
    super(props);
  }

  private static isValidDate(date: Date): boolean {
    return date instanceof Date && !isNaN(date.getTime());
  }

  public static create(
    props: DateOfBirthProps
  ): Result<UserDateOfBirth | PropertyError> {
    // const dateOfBirthResult = Guard.againstNullOrUndefined(
    //   props.value,
    //   this.propertyName
    // );

    // if (!dateOfBirthResult.succeeded) {
    //   return Result.fail<PropertyError>({
    //     property: this.propertyName,
    //     value: props.value.toString(),
    //     error: [dateOfBirthResult.message]
    //   });
    // }
    const typeResult = Guard.againstValueDifferentType(
      props.value,
      this.propertyName,
      ['undefined', 'string']
    );

    if (!typeResult.succeeded) {
      return Result.fail<PropertyError>({
        property: this.propertyName,
        value: String(props.value),
        error: [typeResult.message]
      });
    }

    if (props.value) {
      if (!this.isValidDate(new Date(props.value))) {
        return Result.fail<PropertyError>({
          property: this.propertyName,
          value: String(props.value),
          error: ['Invalid dateOfBirth.']
        });
      }

      const validDateResult = Guard.againstAtGreaterDate(
        props.value,
        this.propertyName
      );
      if (!validDateResult.succeeded) {
        return Result.fail<PropertyError>({
          property: this.propertyName,
          value: props.value.toString(),
          error: [validDateResult.message]
        });
      }
    }

    return Result.ok<UserDateOfBirth>(
      new UserDateOfBirth({
        value: props.value ? new Date(props.value) : props.value
      })
    );
  }
}

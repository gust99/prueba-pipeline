import { Result } from '../../../shared/domain/result';
import { Guard } from '../../../shared/domain/guard';
import { ValueObject } from '../../../shared/domain/valueObject';
import { PropertyError } from '../../../shared/domain/propertyError';
import { ProfessionalProfile } from '../professionalProfile';

export interface UserProfessionalProfileProps {
  value: ProfessionalProfile[];
  propertyName?: string;
}

export class UserProfessionalProfile extends ValueObject<UserProfessionalProfileProps> {
  public static propertyName = 'professionalProfile';
  get value(): ProfessionalProfile[] {
    return this.props.value;
  }

  get propertyName(): string {
    return this.props.propertyName;
  }

  private constructor(props: UserProfessionalProfileProps) {
    super(props);
  }

  public static create(
    props: UserProfessionalProfileProps
  ): Result<UserProfessionalProfile | PropertyError> {
    const userNameResult = Guard.againstNullOrUndefined(
      props.value,
      this.propertyName
    );

    if (!userNameResult.succeeded) {
      return Result.fail<PropertyError>({
        property: this.propertyName,
        value: props.value,
        error: [userNameResult.message]
      });
    }

    const typeResult = Guard.againstValueDifferentType(
      props.value,
      this.propertyName,
      ['ProfessionalProfile']
    );

    if (!typeResult.succeeded) {
      return Result.fail<PropertyError>({
        property: this.propertyName,
        value: props.value,
        error: [typeResult.message]
      });
    }

    props.propertyName = this.propertyName;
    return Result.ok<UserProfessionalProfile>(
      new UserProfessionalProfile(props)
    );
  }
}

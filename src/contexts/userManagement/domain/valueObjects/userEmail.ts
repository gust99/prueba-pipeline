import { PropertyError } from '../../../shared/domain/propertyError';
import { Guard } from '../../../shared/domain/guard';
import { Result } from '../../../shared/domain/result';
import { ValueObject } from '../../../shared/domain/valueObject';

export interface UserEmailProps {
  value: string;
  propertyName?: string;
}

export class UserEmail extends ValueObject<UserEmailProps> {
  private static regex =
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  public static propertyName = 'email';

  get value(): string {
    return this.props.value;
  }

  get propertyName(): string {
    return this.props.propertyName;
  }

  private constructor(props: UserEmailProps) {
    super(props);
  }

  private static isValidEmail(email: string) {
    return this.regex.test(email);
  }

  private static format(email: string): string {
    return email.trim().toLowerCase();
  }

  public static create(
    props: UserEmailProps
  ): Result<UserEmail | PropertyError> {
    const error: PropertyError = {
      property: this.propertyName,
      value: props.value,
      error: []
    };

    const emailResult = Guard.againstNullOrUndefined(
      props.value,
      this.propertyName
    );

    if (!emailResult.succeeded) {
      error.error = [emailResult.message];
      return Result.fail<PropertyError>(error);
    }

    const typeResult = Guard.againstValueDifferentType(
      props.value,
      this.propertyName,
      ['string']
    );

    if (!typeResult.succeeded) {
      error.error = [typeResult.message];

      return Result.fail<PropertyError>(error);
    }

    if (!this.isValidEmail(props.value)) {
      error.error = ['Email address not valid.'];

      return Result.fail<PropertyError>(error);
    }
    return Result.ok<UserEmail>(
      new UserEmail({ value: this.format(props.value) })
    );
  }
}

import { Result } from '../../shared/domain/result';
import { IRepository } from '../../shared/repository/IRepository';
import { User } from '../domain/user';
import { UserEntity } from './user.entity';

export interface IUserRepository extends IRepository<UserEntity> {
  addUser(request: UserEntity): Promise<Result<User>>;
  findUser(id: string): Promise<Result<User> | Result<string>>;
  existEmail(userEmail: string): Promise<boolean>;
  deleteUser(id: string): Promise<Result<boolean> | Result<string>>;
}

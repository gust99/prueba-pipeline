import { Column, Entity, PrimaryColumn } from 'typeorm';
import { UserRole } from '../domain/user';
import { ProfessionalProfile } from '../domain/professionalProfile';
@Entity('User')
export class UserEntity {
  @PrimaryColumn()
  id!: string;

  @Column()
  firstName!: string;

  @Column()
  lastName!: string;

  @Column({ nullable: true })
  dateOfBirth?: Date | undefined;

  @Column()
  email!: string;

  @Column({ default: '' })
  password?: string;

  @Column({ default: '' })
  latitude?: string;

  @Column({ default: '' })
  longitude?: string;

  @Column({ default: '' })
  totalAddress?: string;

  @Column({ default: '' })
  building?: string;

  @Column({ default: '' })
  street?: string;

  @Column({ default: '' })
  neighbourhood?: string;

  @Column({ default: '' })
  city?: string;

  @Column({ default: '' })
  state?: string;

  @Column({ default: '' })
  country?: string;

  @Column({ default: '' })
  phone?: string;

  @Column({ default: '' })
  facebook?: string;

  @Column({ default: '' })
  linkedin?: string;

  @Column({ default: '' })
  website?: string;

  @Column({ default: '' })
  youtube?: string;

  @Column({ default: '' })
  googlehangouts?: string;

  @Column({ default: '' })
  twitter?: string;

  @Column({ default: '' })
  profileImage?: string;

  @Column()
  isEmailVerified: boolean;

  @Column()
  isAdminUser: boolean;

  @Column()
  isDeleted: boolean;

  @Column({ nullable: true })
  lastLogin: Date;

  @Column('json', { nullable: true })
  professionalProfile?: ProfessionalProfile[];

  @Column({
    type: 'enum',
    enum: UserRole,
    default: UserRole.STUDENT
  })
  userRol: UserRole;

  @Column({ default: 0 })
  userRate?: number;

  @Column({ default: 0 })
  userNumOfReviews?: number;
}

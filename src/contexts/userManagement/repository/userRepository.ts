import { injectable } from 'inversify';
import { EntityRepository, getRepository } from 'typeorm';
import { Result } from '../../shared/domain/result';
import { TypeOrmRepository } from '../../../shared/repository/TypeOrmRepository';
import { IUserRepository } from './IUserRepository';
import { UserEntity } from './user.entity';
import { UserMap } from '../presentation/userMap';
import { User } from '../domain/user';

@injectable()
@EntityRepository(UserEntity)
export class UserTypeOrmRepository
  extends TypeOrmRepository<UserEntity>
  implements IUserRepository
{
  constructor() {
    super(getRepository(UserEntity));
  }

  async addUser(user: UserEntity): Promise<Result<User>> {
    const userToSave = this.getRepository().create(user);
    const savedUser = await this.save(userToSave);
    return Result.ok<User>(UserMap.toDomain(savedUser));
  }

  async findUser(id: string): Promise<Result<User> | Result<string>> {
    const user = await this.findById(id);
    return user
      ? user.isDeleted
        ? Result.fail<string>('User Not Found.')
        : Result.ok<User>(UserMap.toDomain(user))
      : Result.fail<string>('User Not Found.');
  }

  async existEmail(userEmail: string): Promise<boolean> {
    try {
      await this.getRepository().findOneOrFail({
        email: userEmail
      });
      return true;
    } catch (error) {
      return false;
    }
  }

  async deleteUser(id: string): Promise<Result<boolean> | Result<string>> {
    const userOrError = await this.findUser(id);
    if (userOrError.isFailure) {
      return userOrError as Result<string>;
    }
    await this.getRepository().update(id, { isDeleted: true });
    return Result.ok(true);
  }
}

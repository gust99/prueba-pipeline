import { ProfessionalProfile } from '../domain/professionalProfile';
export class UpdateUserRepositoryRequest {
  isEmailVerified?: boolean;
  constructor(
    public firstName?: string,
    public lastName?: string,
    public dateOfBirth?: Date,
    public email?: string,
    public latitude?: string,
    public longitude?: string,
    public totalAddress?: string,
    public building?: string,
    public street?: string,
    public neighbourhood?: string,
    public city?: string,
    public state?: string,
    public country?: string,
    public phone?: string,
    public password?: string,
    public facebook?: string,
    public linkedin?: string,
    public website?: string,
    public youtube?: string,
    public googlehangouts?: string,
    public twitter?: string,
    public profileImage?: string,
    public userRol?: string,
    public professionalProfile?: ProfessionalProfile[]
  ) {}
}

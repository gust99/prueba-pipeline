const PROFILE_TYPES = {
  UserController: Symbol.for('UserController'),
  UserService: Symbol.for('UserService'),
  UserRepository: Symbol.for('UserRepository')
};

export { PROFILE_TYPES };

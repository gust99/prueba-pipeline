import { User, UserRole } from '../domain/user';
import { UserEntity } from '../repository/user.entity';
import { UserFirstName } from '../domain/valueObjects/userFirstName';
import { UserLastName } from '../domain/valueObjects/userLastName';
import { UserDateOfBirth } from '../domain/valueObjects/userDateOfBirth';
import { UserEmail } from '../domain/valueObjects/userEmail';
import { UserPassword } from '../domain/valueObjects/userPassword';
import { ResponseUserDto } from './dtos/responseUserDto';
import { UpdateUserRepositoryRequest } from '../repository/updateUserRepositoryRequest';
import { UniqueEntityID } from '../../shared/domain/uniqueEntityID';
import { UserId } from '../domain/userId';

export class UserMap {
  static toDomain(raw: UserEntity): User {
    const userFirstNameOrError = UserFirstName.create({
      value: raw.firstName
    });
    const userLastNameOrError = UserLastName.create({
      value: raw.lastName
    });
    const userDateOfBirthOrError = UserDateOfBirth.create({
      value: raw.dateOfBirth ? raw.dateOfBirth.toString() : undefined
    });
    const emailOrError = UserEmail.create({
      value: raw.email
    });
    const userPasswordOrError = UserPassword.create(
      {
        value: raw.password
      },
      (param) => param
    );

    const userOrError = User.create(
      {
        userFirstName: userFirstNameOrError.getValue() as UserFirstName,
        userLastName: userLastNameOrError.getValue() as UserLastName,
        userDateOfBirth: userDateOfBirthOrError.getValue() as UserDateOfBirth,
        userEmail: emailOrError.getValue() as UserEmail,
        latitude: raw.latitude,
        longitude: raw.longitude,
        totalAddress: raw.totalAddress,
        building: raw.building,
        street: raw.street,
        neighbourhood: raw.neighbourhood,
        city: raw.city,
        state: raw.state,
        country: raw.country,
        phoneNumber: raw.phone,
        userPassword: userPasswordOrError.getValue() as UserPassword,
        facebook: raw.facebook,
        linkedin: raw.linkedin,
        website: raw.website,
        youtube: raw.youtube,
        googlehangouts: raw.googlehangouts,
        twitter: raw.twitter,
        profileImage: raw.profileImage,
        userRol: raw.userRol,
        professionalProfile: raw.professionalProfile,
        userRate: raw.userRate,
        userNumOfReviews: raw.userNumOfReviews
      },
      raw.id
    );
    return userOrError.getValue();
  }

  public static toRepositoryEntity(entity: User): UserEntity {
    return {
      id: entity.userId.id.toString(),
      firstName: entity.userFirstName.value,
      lastName: entity.userLastName.value,
      dateOfBirth: entity.dateOfBirth.value as any,
      email: entity.email.value,
      latitude: entity.latitude,
      longitude: entity.longitude,
      totalAddress: entity.totalAddress,
      building: entity.building,
      street: entity.street,
      neighbourhood: entity.neighbourhood,
      city: entity.city,
      state: entity.state,
      country: entity.country,
      phone: entity.phoneNumber,
      password: entity.password.value,
      facebook: entity.facebook,
      linkedin: entity.linkedin,
      website: entity.website,
      youtube: entity.youtube,
      googlehangouts: entity.googlehangouts,
      twitter: entity.twitter,
      profileImage: entity.profileImage,
      isEmailVerified: entity.isEmailVerified,
      isAdminUser: entity.isAdminUser,
      isDeleted: entity.isDeleted,
      lastLogin: entity.lastLogin,
      userRol: entity.userRol,
      professionalProfile: entity.professionalProfile
    };
  }

  static toPersistenceFromUpdateRequest(
    data: any
  ): UpdateUserRepositoryRequest {
    return {
      firstName: data.firstName,
      lastName: data.lastName,
      dateOfBirth: data.dateOfBirth,
      email: data.email,
      password: data.password,
      latitude: data.address?.latitude,
      longitude: data.address?.longitude,
      totalAddress: data.address?.totalAddress,
      building: data.address?.building,
      street: data.address?.street,
      neighbourhood: data.address?.neighbourhood,
      city: data.address?.city,
      state: data.address?.state,
      country: data.address?.country,
      phone: data.phone,
      facebook: data.social?.facebook,
      linkedin: data.social?.linkedin,
      website: data.social?.website,
      youtube: data.social?.youtube,
      googlehangouts: data.social?.googlehangouts,
      twitter: data.social?.twitter,
      profileImage: data.profileImage,
      userRol: data.userRol,
      professionalProfile: data?.professionalProfile
    };
  }

  static toMentorsDomain(data: any): User {
    const firstName = UserFirstName.create({ value: data.userFirstName });
    const lastName = UserLastName.create({ value: data.userLastName });
    const profileImage = data.profileImage;
    const userRol = UserRole.MENTOR;
    const userId = UserId.create(new UniqueEntityID(data.userId));

    const mentor = User.create({
      userFirstName: firstName.getValue() as UserFirstName,
      userLastName: lastName.getValue() as UserLastName,
      profileImage: profileImage.getValue() as string,
      userDateOfBirth: null,
      userEmail: null,
      userPassword: null
    });

    return mentor.getValue();
  }

  static toMentorsDTO(data: User): ResponseUserDto {
    const mentor = new ResponseUserDto();
    mentor.id = data.id.toString();
    mentor.firstName = data.userFirstName.value.toString();
    mentor.lastName = data.userLastName.value.toString();
    mentor.userRol = data.userRol.toString();
    mentor.profileImage = data.profileImage.toString();
    return mentor;
  }

  public static toResponseUserDto(user: User): ResponseUserDto {
    return {
      id: user.id.toString(),
      firstName: user.userFirstName.value,
      lastName: user.userLastName.value,
      dateOfBirth: user.dateOfBirth.value,
      email: user.email.value,
      phone: user.phoneNumber,
      profileImage: user.profileImage,
      userRol: user.userRol,
      address: {
        latitude: user.latitude,
        longitude: user.longitude,
        totalAddress: user.totalAddress,
        building: user.building,
        street: user.street,
        neighbourhood: user.neighbourhood,
        city: user.city,
        state: user.state,
        country: user.country
      },
      social: {
        facebook: user.facebook,
        linkedin: user.linkedin,
        website: user.website,
        youtube: user.youtube,
        googlehangouts: user.googlehangouts,
        twitter: user.twitter
      },
      professionalProfile: user.professionalProfile,
      userRate: user.userRate,
      userNumOfReviews: user.userNumOfReviews
    };
  }
}

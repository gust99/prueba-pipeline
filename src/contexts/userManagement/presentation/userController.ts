import {
  controller,
  httpGet,
  BaseHttpController,
  httpPost,
  httpPut,
  httpDelete,
  request,
  response,
  requestParam
} from 'inversify-express-utils';
import { inject } from 'inversify';
import { PROFILE_TYPES } from '../types';
import { UserService } from '../services/userService';
import { Request, Response } from 'express';
import { CreateUserRequestDto } from './dtos/createUserRequestDto';
import { plainToInstance } from 'class-transformer';
import { loggedUserMw } from '../../../shared/presentation/middleware/tokenMiddleware/tokenverifier.middleware';
import { User } from '../domain/user';
import {
  CustomRepositoryCannotInheritRepositoryError,
  getConnectionManager,
  getRepository
} from 'typeorm';
import { UpdateUserRequestDto } from './dtos/updateUserRequestDto';

@controller('/users')
export class UserController extends BaseHttpController {
  constructor(
    @inject(PROFILE_TYPES.UserService) private userService: UserService
  ) {
    super();
  }

  @httpPost('/')
  private async createUser(@request() req: Request, @response() res: Response) {
    const data = plainToInstance(CreateUserRequestDto, req.body, {
      excludeExtraneousValues: true
    });
    const response = await this.userService.createUser(data);
    res.status(response.statusCode).json(response.responseContent);
  }

  @httpGet('/:id')
  private async getUser(
    @requestParam('id') id: string,
    @response() res: Response
  ) {
    const response = await this.userService.getUser(id);
    res.status(response.statusCode).json(response.responseContent);
  }

  @httpGet('-mentor')
  private async getMentor(@request() req: Request, @response() res: Response) {
    const response = await this.userService.findMentors();
    res.status(response.statusCode).json(response.responseContent);
  }

  @httpPut('/:id', loggedUserMw)
  private async updateUser(
    @requestParam('id') id: string,
    @request() req: Request,
    @response() res: Response
  ) {
    const data = plainToInstance(CreateUserRequestDto, req.body, {
      //excludeExtraneousValues: true,
      //strategy: 'excludeAll'
    });
    const response = await this.userService.updateUser(id, data);
    res.status(response.statusCode).json(response.responseContent);
  }

  @httpDelete('/:id', loggedUserMw)
  private async deleteUser(
    @requestParam('id') id: string,
    @response() res: Response
  ) {
    const result = await this.userService.deleteUser(id);
    res.status(result.statusCode).json(result.responseContent);
  }
}

import { Expose, Type } from 'class-transformer';
import { ProfessionalProfile } from '../../domain/professionalProfile';
import { Address } from './addressDto';
import { Social } from './socialsDto';

export class UpdateUserRequestDto {
  @Expose()
  firstName?: string;

  @Expose()
  lastName?: string;

  @Type(() => Date)
  @Expose()
  dateOfBirth?: string;

  @Expose()
  email?: string;

  @Expose()
  phone?: string;

  @Expose()
  password?: string;

  @Type(() => Address)
  @Expose()
  address?: Address;

  @Type(() => Social)
  @Expose()
  social?: Social;

  @Expose()
  profileImage?: string;
  @Type(() => ProfessionalProfile)
  @Expose()
  professionalProfile?: ProfessionalProfile[];
}

import { Address } from './addressDto';
import { Social } from './socialsDto';
import { ProfessionalProfile } from '../../domain/professionalProfile';

export class ResponseUserDto {
  id: string;

  firstName: string;

  lastName: string;

  dateOfBirth?: Date | string;

  email: string;

  phone?: string;

  address?: Address;

  social?: Social;

  profileImage?: string;

  userRol?: string;

  professionalProfile?: ProfessionalProfile[];

  userRate: number;

  userNumOfReviews: number;
}

import { Expose } from 'class-transformer';

export class Social {
  @Expose()
  facebook?: string;

  @Expose()
  linkedin?: string;

  @Expose()
  website?: string;

  @Expose()
  youtube?: string;

  @Expose()
  googlehangouts?: string;

  @Expose()
  twitter?: string;
}

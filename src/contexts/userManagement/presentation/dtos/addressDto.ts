import { Expose } from 'class-transformer';

export class Address {
  @Expose()
  latitude?: string;

  @Expose()
  longitude?: string;

  @Expose()
  totalAddress?: string;

  @Expose()
  building?: string;

  @Expose()
  street?: string;

  @Expose()
  neighbourhood?: string;

  @Expose()
  city?: string;

  @Expose()
  state?: string;

  @Expose()
  country?: string;
}

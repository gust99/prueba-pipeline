import { Expose, Type } from 'class-transformer';
import { Address } from './addressDto';
import { Social } from './socialsDto';
import { ProfessionalProfile } from '../../domain/professionalProfile';

export class CreateUserRequestDto {
  @Expose()
  userId?: string;

  @Expose()
  firstName: string;

  @Expose()
  lastName: string;

  @Expose()
  dateOfBirth?: string;

  @Expose()
  email: string;

  @Expose()
  phone?: string;

  @Expose()
  password?: string;

  @Type(() => Address)
  @Expose()
  address?: Address;

  @Type(() => Social)
  @Expose()
  social?: Social;

  @Expose()
  profileImage?: string;

  @Type(() => ProfessionalProfile)
  @Expose()
  professionalProfile?: ProfessionalProfile[];
}

import { injectable } from 'inversify';
import { EntityRepository, getRepository } from 'typeorm';
import { Result } from '../../shared/domain/result';
import { TypeOrmRepository } from '../../../shared/repository/TypeOrmRepository';
import { IServicePropertyRepository } from './IServicePropertyRepository';
import { ServicePropertyEntity } from './serviceProperty.entity';
import { ServicePropertyMap } from '../presentation/servicePropertyMap';
import { ServiceProperty } from '../domain/serviceProperty';

@injectable()
@EntityRepository(ServicePropertyEntity)
export class ServicePropertyTypeOrmRepository
  extends TypeOrmRepository<ServicePropertyEntity>
  implements IServicePropertyRepository
{
  constructor() {
    super(getRepository(ServicePropertyEntity));
  }

  async addServiceProperty(
    serviceProperty: ServicePropertyEntity
  ): Promise<Result<ServiceProperty>> {
    const propertyToSave = this.getRepository().create(serviceProperty);
    const savedServiceProperty = await this.save(propertyToSave);
    return Result.ok<ServiceProperty>(
      ServicePropertyMap.toDomain(savedServiceProperty)
    );
  }

  async findServiceProperty(
    id: string
  ): Promise<Result<ServiceProperty> | Result<string>> {
    const serviceProperty = await this.findById(id);
    return serviceProperty
      ? Result.fail<string>('Property Not Found.')
      : Result.ok<ServiceProperty>(
          ServicePropertyMap.toDomain(serviceProperty)
        );
  }
}

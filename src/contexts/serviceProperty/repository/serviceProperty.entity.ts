import { Column, Entity, PrimaryColumn } from 'typeorm';
import { ServiceType } from '../domain/serviceProperty';
@Entity('ServiceProperty')
export class ServicePropertyEntity {
  @PrimaryColumn()
  servicePropertyId: string;

  @Column({
    type: 'enum',
    enum: ServiceType,
    default: ServiceType.LEARNING
  })
  serviceType: ServiceType;

  @Column()
  serviceId: string;

  @Column()
  propertyName?: string;

  @Column()
  propertyValue?: string;
}

import { Result } from '../../shared/domain/result';
import { IRepository } from '../../shared/repository/IRepository';
import { ServiceProperty } from '../domain/serviceProperty';
import { ServicePropertyEntity } from './serviceProperty.entity';

export interface IServicePropertyRepository
  extends IRepository<ServicePropertyEntity> {
  addServiceProperty(
    request: ServicePropertyEntity
  ): Promise<Result<ServiceProperty>>;
  findServiceProperty(
    id: string
  ): Promise<Result<ServiceProperty> | Result<string>>;
}

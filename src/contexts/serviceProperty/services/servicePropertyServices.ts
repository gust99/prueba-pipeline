import { inject, injectable } from 'inversify';
import { SERVICEPROPERTY_TYPES } from '../types';
import { IRepository, Query } from '../../shared/repository/IRepository';
import { Result } from '../../shared/domain/result';
import { ServicePropertyEntity } from '../repository/serviceProperty.entity';
import { ServicePropertyMap } from '../presentation/servicePropertyMap';
import { RequestServicePropertyDTO } from '../presentation/dtos/requestServiceProperty.dto';
import { ServiceId } from '../../serviceManagement/domain/serviceId';
import { UniqueEntityID } from '../../shared/domain/uniqueEntityID';
import { ServiceProperty, ServiceType } from '../domain/serviceProperty';
import { Response } from '../../shared/domain/response';
import { SERVICE_TYPES } from '../../serviceManagement/types';
import {
  ErrorHelper,
  ObjectError,
  PropertyError
} from '../../shared/domain/propertyError';
import { ServiceEntity } from '../../serviceManagement/repository/service.entity';

@injectable()
export class ServicePropertyService {
  constructor(
    @inject(SERVICEPROPERTY_TYPES.ServicePropertyRepository)
    private servicePropertyRepository: IRepository<ServicePropertyEntity>,
    @inject(SERVICE_TYPES.ServiceRepository)
    private serviceRepository: IRepository<ServiceEntity>
  ) {}
  async findAll(): Promise<Response<any>> {
    try {
      const servicePropertiessRaw =
        await this.servicePropertyRepository.findAll();
      const serviceProperties = servicePropertiessRaw.map((serviceProperty) => {
        const servicePropertyFound =
          ServicePropertyMap.toDomain(serviceProperty);
        const servicePropertyDTO =
          ServicePropertyMap.toDTO(servicePropertyFound);
        return servicePropertyDTO;
      });
      return Response.ok('Listing all properties', serviceProperties);
    } catch (error) {
      console.log(error);
      return Response.internalServerError();
    }
  }

  async save(
    servicePropertyDTO: RequestServicePropertyDTO
  ): Promise<Response<any>> {
    try {
      const servicePropertyErrors: ObjectError = {
        entity: ServiceProperty.entityName,
        errors: []
      };

      const serviceIdOrError = ServiceId.create(
        new UniqueEntityID(servicePropertyDTO.serviceId)
      );
      const propertyErrors = ErrorHelper.multiplePropertiesHaveErrors([
        serviceIdOrError.error
      ]);

      if (ErrorHelper.objectHasErrors(propertyErrors)) {
        servicePropertyErrors.errors = propertyErrors;
        return Response.badRequest(
          'Invalid data',
          servicePropertyErrors.errors
        );
      }

      const serviceProperty = ServiceProperty.create({
        serviceType: ServiceType.LEARNING,
        serviceId: serviceIdOrError.getValue(),
        propertyName: servicePropertyDTO.propertyName,
        propertyValue: servicePropertyDTO.propertyValue
      });

      if (!serviceProperty.isSuccess)
        return Response.badRequest('Invalid data', serviceProperty.error);

      const servicePropertyEntity = ServicePropertyMap.toPersistence(
        serviceProperty.getValue()
      );
      await this.servicePropertyRepository.save(servicePropertyEntity);

      return Response.ok(
        'Service property created',
        ServicePropertyMap.toDTO(serviceProperty.getValue())
      );
    } catch (error) {
      console.log(error);
      return Response.internalServerError();
    }
  }

  async findByService(serviceId: string): Promise<Response<any>> {
    try {
      const query: Query<ServiceEntity> = {
        serviceId: serviceId
      };
      const servicePropertyRaw =
        await this.servicePropertyRepository.findByQuery(query);
      if (!servicePropertyRaw)
        return Response.notFound('Service property not found');
      return Response.ok('Service property found', servicePropertyRaw);
    } catch (error) {
      console.log(error);
      return Response.internalServerError();
    }
  }

  async update(
    propertyId: string,
    dto: RequestServicePropertyDTO
  ): Promise<Response<any>> {
    try {
      const servicePropertyRaw = await this.servicePropertyRepository.findById(
        propertyId
      );
      if (!servicePropertyRaw)
        return Response.notFound('Service property not found');

      const serviceProperty = ServicePropertyMap.toDomain(servicePropertyRaw);

      if ('propertyName' in dto) {
        const newServicePropertyName = dto.propertyName;
        serviceProperty.updateName(newServicePropertyName);
      }

      if ('propertyValue' in dto) {
        const newServicePropertyValue = dto.propertyValue;
        serviceProperty.updateValue(newServicePropertyValue);
      }

      const servicePropertyEntity =
        ServicePropertyMap.toPersistence(serviceProperty);

      const updated = this.servicePropertyRepository.update(
        propertyId,
        servicePropertyEntity
      );

      if (updated)
        return Response.ok(
          'Service updated',
          ServicePropertyMap.toDTO(serviceProperty)
        );

      return Response.internalServerError();
    } catch (error) {
      console.log(error);
      return Response.internalServerError();
    }
  }
}

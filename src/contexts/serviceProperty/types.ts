const SERVICEPROPERTY_TYPES = {
  ServicePropertyController: Symbol.for('ServicePropertyController'),
  ServicePropertyService: Symbol.for('ServicePropertyService'),
  ServicePropertyRepository: Symbol.for('ServicePropertyRepository'),
  Lodash: Symbol.for('Lodash')
};

export { SERVICEPROPERTY_TYPES };

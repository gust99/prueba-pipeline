import { AggregateRoot } from '../../shared/domain/agregateRoot';
import { UniqueEntityID } from '../../shared/domain/uniqueEntityID';
import { Result } from '../../shared/domain/result';
import { Guard, IGuardArgument } from '../../shared/domain/guard';
import { PropertyError } from '../../shared/domain/propertyError';
import { ServiceId } from '../../serviceManagement/domain/serviceId';
import { ServicePropertyId } from './servicePropertyId';

export enum ServiceType {
  LEARNING = 'learning'
}

interface ServicePropertyProps {
  serviceType: ServiceType;
  serviceId: ServiceId;
  propertyName?: string;
  propertyValue?: string;
}

export class ServiceProperty extends AggregateRoot<ServicePropertyProps> {
  public static entityName = 'serviceProperty';

  get servicePropertyId(): ServiceId {
    return ServicePropertyId.create(this._id).getValue();
  }

  get serviceType(): ServiceType {
    return this.props.serviceType;
  }

  get serviceId(): ServiceId {
    return this.props.serviceId;
  }

  get propertyName(): string {
    return this.props.propertyName;
  }
  get propertyValue(): string {
    return this.props.propertyValue;
  }
  public updateName(propertyName: string) {
    this.props.propertyName = propertyName;
    return Result.ok<void>();
  }
  public updateValue(propertyValue: string) {
    this.props.propertyValue = propertyValue;
    return Result.ok<void>();
  }

  private constructor(props: ServicePropertyProps, id?: UniqueEntityID) {
    super(props, id);
  }

  public static create(
    props: ServicePropertyProps,
    id?: UniqueEntityID
  ): Result<any> {
    const guardArgs: IGuardArgument[] = [
      { argument: props.serviceType, argumentName: 'serviceType' },
      { argument: props.serviceId, argumentName: 'serviceId' }
    ];
    const guardResult = Guard.againstNullOrUndefinedBulk(guardArgs);

    if (!guardResult.succeeded) {
      //publicationErrors.errors = guardResult.errors;
      return Result.fail(guardResult.errors);
    }

    const defaultValues: ServicePropertyProps = {
      ...props,
      serviceType: props.serviceType ? props.serviceType : ServiceType.LEARNING
    };

    const isNewServiceProperty = !!id === false;

    if (isNewServiceProperty) {
      const serviceProperty = new ServiceProperty(defaultValues);
      return Result.ok<ServiceProperty>(serviceProperty);
    }

    const serviceProperty = new ServiceProperty(defaultValues, id);
    return Result.ok<ServiceProperty>(serviceProperty);
  }
}

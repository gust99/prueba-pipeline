export class ResponseServicePropertyDTO {
  id: string;
  serviceType: string;
  serviceId: string;
  propertyName?: string;
  propertyValue?: string;
}

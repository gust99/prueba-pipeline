class RequestServicePropertyDTO {
  serviceType: string;
  serviceId: string;
  propertyName?: string;
  propertyValue?: string;
}

export { RequestServicePropertyDTO };

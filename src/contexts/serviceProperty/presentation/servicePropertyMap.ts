import { ServiceId } from '../../serviceManagement/domain/serviceId';
import { UniqueEntityID } from '../../shared/domain/uniqueEntityID';
import { ServiceProperty, ServiceType } from '../domain/serviceProperty';
import { ServicePropertyEntity } from '../repository/serviceProperty.entity';
import { ResponseServicePropertyDTO } from './dtos/responseServiceProperty.dto';

export class ServicePropertyMap {
  static toPersistence(
    serviceProperty: ServiceProperty
  ): ServicePropertyEntity {
    const entity = new ServicePropertyEntity();
    entity.servicePropertyId = serviceProperty.servicePropertyId.id.toString();
    entity.serviceId = serviceProperty.serviceId.id.toString();
    entity.serviceType = serviceProperty.serviceType;
    entity.propertyName = serviceProperty.propertyName.toString();
    entity.propertyValue = serviceProperty.propertyValue.toString();
    return entity;
  }
  static toDTO(data: ServiceProperty): ResponseServicePropertyDTO {
    const serviceProperty = new ResponseServicePropertyDTO();
    serviceProperty.id = data.servicePropertyId.id.toString();
    serviceProperty.serviceId = data.serviceId.id.toString();
    serviceProperty.serviceType = data.serviceType.toString();
    serviceProperty.propertyName = data.propertyName.toString();
    serviceProperty.propertyValue = data.propertyValue.toString();
    return serviceProperty;
  }
  static toDomain(data: any): ServiceProperty {
    const serviceId = ServiceId.create(new UniqueEntityID(data.serviceId));

    const serviceProperty = ServiceProperty.create(
      {
        serviceId: serviceId.getValue() as ServiceId,
        serviceType: ServiceType.LEARNING,
        propertyName: data.propertyName as string,
        propertyValue: data.propertyValue as string
      },
      new UniqueEntityID(data.id)
    );

    return serviceProperty.getValue();
  }
}

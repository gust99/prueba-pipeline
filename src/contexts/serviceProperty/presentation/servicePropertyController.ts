import {
  controller,
  httpGet,
  BaseHttpController,
  httpPost,
  httpPut,
  httpDelete,
  request,
  response,
  requestParam
} from 'inversify-express-utils';
import { Response, Request } from 'express';

import { inject } from 'inversify';
import { SERVICEPROPERTY_TYPES } from '../types';
import { ServicePropertyService } from '../services/servicePropertyServices';
import { ServiceProperty } from '../domain/serviceProperty';
import { RequestServicePropertyDTO } from './dtos/requestServiceProperty.dto';
import { plainToInstance } from 'class-transformer';
import { ObjectType } from 'typeorm';

@controller('/property')
export class ServicePropertyController extends BaseHttpController {
  private entityType: ObjectType<ServiceProperty>;
  constructor(
    @inject(SERVICEPROPERTY_TYPES.ServicePropertyService)
    private servicePropertyService: ServicePropertyService
  ) {
    super();
  }

  @httpPost('/')
  private async createServiceProperty(
    @request() req: Request,
    @response() res: Response
  ) {
    const response = await this.servicePropertyService.save(
      plainToInstance(RequestServicePropertyDTO, req.body)
    );
    res.status(response.statusCode).json(response.responseContent);
  }

  @httpGet('/service/:id')
  private async getServicePropertiesByService(
    @requestParam('id') id: string,
    @request() req: Request,
    @response() res: Response
  ) {
    const response = await this.servicePropertyService.findByService(id);
    res.status(response.statusCode).json(response.responseContent);
  }

  @httpPut('/:id')
  private async updateServiceProperty(
    @requestParam('id') id: string,
    @request() req: Request,
    @response() res: Response
  ) {
    const response = await this.servicePropertyService.update(
      id,
      plainToInstance(RequestServicePropertyDTO, req.body)
    );
    res.status(response.statusCode).json(response.responseContent);
  }
}

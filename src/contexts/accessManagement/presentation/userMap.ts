import { LoggedUserDto } from './dtos/loggedUserDto';

export class UserMap {
  public static toLoggedUserDto(user: any): LoggedUserDto {
    return {
      uid: user.id.value,
      name: user.userFirstName.value + ' ' + user.userLastName.value,
      email: user.email.value,
      rol: user.userRol
    };
  }
}

import {
  controller,
  httpPost,
  request,
  response
} from 'inversify-express-utils';
import { Request, Response } from 'express';
import { loggedUserMw } from '../../../shared/presentation/middleware/tokenMiddleware/tokenverifier.middleware';
import { AUTHENTICATION_TYPES } from '../types';
import { AuthService } from '../services/auth.service';
import { inject } from 'inversify';

@controller('/auth')
export class AuthController {
  constructor(
    @inject(AUTHENTICATION_TYPES.AuthService)
    private authService: AuthService
  ) {}

  @httpPost('/login')
  private async loginUser(@request() req: Request, @response() res: Response) {
    const response = await this.authService.loginUser(req.body.uid);
    res.status(response.statusCode).json(response.responseContent);
  }

  @httpPost('/refresh', loggedUserMw)
  private async refreshToken(
    @request() req: Request,
    @response() res: Response
  ) {
    const refreshToken = req.body.refreshToken;
    const response = await this.authService.refreshSession(
      refreshToken,
      req.headers.authorization
    );
    res.status(response.statusCode).json(response.responseContent);
  }
}

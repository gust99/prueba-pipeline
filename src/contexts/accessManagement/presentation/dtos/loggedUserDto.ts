export class LoggedUserDto {
  uid: string;

  name: string;

  email: string;

  rol: string;
}

const AUTHENTICATION_TYPES = {
  AuthController: Symbol.for('AuthController'),
  AuthService: Symbol.for('AuthService')
};

export { AUTHENTICATION_TYPES };

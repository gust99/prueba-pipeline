import { inject, injectable } from 'inversify';
import { Response } from '../../shared/domain/response';
import { TYPES } from '../../../types';
import { UserTypeOrmRepository } from '../../userManagement/repository/userRepository';
import { UserMap } from '../presentation/userMap';
import jwt, { TokenExpiredError } from 'jsonwebtoken';
import { IAuthDecodedToken } from '../../../shared/presentation/middleware/tokenMiddleware/tokenverifier.middleware';

@injectable()
export class AuthService {
  constructor(
    @inject(TYPES.UserRepository)
    private readonly userRepository: UserTypeOrmRepository
  ) {}

  public async loginUser(id: string): Promise<Response<any>> {
    try {
      const userOrError = await this.userRepository.findUser(id);
      if (userOrError.isFailure) {
        return Response.notFound(userOrError.errorValue() as string);
      }

      const token = jwt.sign(
        UserMap.toLoggedUserDto(userOrError.getValue() as any),
        'secret',
        { expiresIn: '1h' }
      );
      const refreshToken = jwt.sign({}, 'secret', {
        expiresIn: '1d'
      });
      const authToken = { token, refreshToken };
      return Response.ok('User found.', authToken);
    } catch (error) {
      console.log(error);
      return Response.internalServerError();
    }
  }

  public async refreshSession(
    refreshToken: string,
    token: string
  ): Promise<Response<any>> {
    try {
      const decodedRefreshToken = jwt.verify(refreshToken, 'secret');
      if (!decodedRefreshToken) {
        return Response.create(401, {
          code: '401',
          message: 'Token invalid or not logged in'
        });
      }

      const newRefreshToken = jwt.sign({}, 'secret', {
        expiresIn: '1d'
      });

      const accessTokenDecoded = jwt.decode(
        token.replace('Bearer ', '')
      ) as IAuthDecodedToken;
      const newAccessToken = jwt.sign(
        {
          uid: accessTokenDecoded.uid,
          name: accessTokenDecoded.name,
          email: accessTokenDecoded.email,
          rol: accessTokenDecoded.rol
        },
        'secret',
        {
          expiresIn: '1h'
        }
      );

      return Response.ok('Token refreshed.', {
        token: newAccessToken,
        refreshToken: newRefreshToken
      });
    } catch (error) {
      if (error instanceof TokenExpiredError) {
        return Response.create(401, {
          code: '401',
          message: 'You need to log in again'
        });
      }
      console.log(error);
      return Response.internalServerError();
    }
  }
}

import { Response } from '../../shared/domain/response';
import Payment from '../repository/paymentModel';

class PaymentService {
  private payment: Payment;

  test() {
    try {
      return Response.ok('Tested', 'data mock');
    } catch (error) {
      console.log(error);
      return Response.internalServerError();
    }
  }

  instancePayment(data: any) {
    try {
      this.payment = new Payment(
        data.id,
        data.idSeller,
        data.idBuyer,
        data.item
      );
      return Response.created('New payment generated', this.payment);
    } catch (error) {
      console.log(error);
      return Response.internalServerError();
    }
  }

  paymentStatus(id: string) {
    try {
      if (this.payment !== undefined) {
        return Response.ok('Payment found successfully', this.payment);
      }
      return Response.badRequest('Please generate a payment', ['No payment']);
    } catch (error) {
      console.log(error);
      return Response.internalServerError();
    }
  }

  acceptAgreement(data: any) {
    try {
      if (data.id !== this.payment.id) {
        return Response.badRequest('No payment found', ['No payment']);
      }
      if (data.idUser === this.payment.idSeller) {
        this.payment.sellerAgree = true;
        return Response.ok('Seller accept agreement', this.payment);
      } else if (data.idUser === this.payment.idBuyer) {
        this.payment.buyerAgree = true;
        return Response.ok('Buyer accept agreement', this.payment);
      }
      return Response.badRequest('No part accept', ['No part accept']);
    } catch (error) {
      return Response.badRequest('Please generate a payment', ['No payment']);
    }
  }

  generatePayment(data: any) {
    try {
      if (data.id !== this.payment.id) {
        return Response.badRequest('No payment found', ['No payment']);
      }
      if (this.payment.sellerAgree && this.payment.buyerAgree) {
        this.payment.paymentMethod = data.paymentMethod;
        return Response.ok('Payment did successfully', this.payment);
      }
      return Response.badRequest('both parties should accept agreement', [
        'No parties accepted'
      ]);
    } catch (error) {
      return Response.badRequest('Please generate a payment', ['No payment']);
    }
  }

  confirmService(data: any) {
    try {
      if (data.id !== this.payment.id) {
        return Response.badRequest('No payment found', ['No payment']);
      }
      if (data.idUser !== this.payment.idSeller) {
        return Response.badRequest('This user cannot confirm the service', [
          'No confirmed'
        ]);
      }
      if (this.payment.paymentMethod !== undefined) {
        this.payment.serviceDelivered = true;
        return Response.ok('Service confirmed successfully', this.payment);
      }
      return Response.badRequest(
        "It's necessary first confirm buyer's payment",
        ['No confirmed']
      );
    } catch (error) {
      return Response.badRequest('Please generate a payment', ['No payment']);
    }
  }

  generateCheckout(data: any) {
    try {
      if (data.id !== this.payment.id) {
        return Response.badRequest('No payment found', ['No payment']);
      }
      if (this.payment.reimbursement) {
        return Response.badRequest('A reimbursement has been asked', [
          'Reimbursement asked'
        ]);
      } else if (this.payment.serviceDelivered) {
        this.payment.approved = true;
        return Response.ok(
          'Payment was sended to Seller successfully',
          this.payment
        );
      }
      return Response.badRequest('Service should be confirmed', [
        'No confirmed'
      ]);
    } catch (error) {
      return Response.badRequest('Please generate a payment', ['No payment']);
    }
  }

  askReimbursement(data: any) {
    try {
      if (data.id !== this.payment.id) {
        return Response.badRequest('No payment found', ['No payment']);
      }
      if (this.payment.approved) {
        return Response.badRequest('Payment was already sended to Seller', [
          'Checkout confirmed'
        ]);
      } else if (this.payment.serviceDelivered) {
        this.payment.reimbursement = true;
        return Response.ok('Reimbursement will be considered', this.payment);
      }
      return Response.badRequest('Service should be confirmed', [
        'No confirmed'
      ]);
    } catch (error) {
      return Response.badRequest('Please generate a payment', ['No payment']);
    }
  }
}

const paymentService = new PaymentService();
export default paymentService;

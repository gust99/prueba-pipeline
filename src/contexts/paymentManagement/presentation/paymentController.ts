import {
  controller,
  httpGet,
  BaseHttpController,
  httpPost,
  httpPut,
  //httpDelete,
  request,
  response,
  requestParam
} from 'inversify-express-utils';
import paymentService from '../services/paymentService';
import { Request, Response } from 'express';

@controller('/payment')
export class PaymentController extends BaseHttpController {
  constructor() {
    super();
  }

  @httpPost('/')
  private async createPayment(
    @request() req: Request,
    @response() res: Response
  ) {
    const response = await paymentService.instancePayment(req.body);
    res.status(response.statusCode).json(response.responseContent);
  }

  @httpGet('/:id')
  private async getPayment(
    @requestParam('id') id: string,
    @response() res: Response
  ) {
    const response = await paymentService.paymentStatus(id);
    res.status(response.statusCode).json(response.responseContent);
  }

  @httpPut('/agreement')
  private async acceptAgreement(
    @request() req: Request,
    @response() res: Response
  ) {
    const response = await paymentService.acceptAgreement(req.body);
    res.status(response.statusCode).json(response.responseContent);
  }

  @httpPut('/method')
  private async generateMethod(
    @request() req: Request,
    @response() res: Response
  ) {
    const response = await paymentService.generatePayment(req.body);
    res.status(response.statusCode).json(response.responseContent);
  }

  @httpPut('/service')
  private async confirmService(
    @request() req: Request,
    @response() res: Response
  ) {
    const response = await paymentService.confirmService(req.body);
    res.status(response.statusCode).json(response.responseContent);
  }

  @httpPut('/checkout')
  private async generateCheckout(
    @request() req: Request,
    @response() res: Response
  ) {
    const response = await paymentService.generateCheckout(req.body);
    res.status(response.statusCode).json(response.responseContent);
  }

  @httpPut('/reimbursement')
  private async askReimbursement(
    @request() req: Request,
    @response() res: Response
  ) {
    const response = await paymentService.askReimbursement(req.body);
    res.status(response.statusCode).json(response.responseContent);
  }
}

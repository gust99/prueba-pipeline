export default class Payment {
  id: string;
  idSeller: string;
  idBuyer: string;
  item: any;
  paymentMethod: any;
  sellerAgree: boolean;
  buyerAgree: boolean;
  serviceDelivered: boolean;
  approved: boolean;
  reimbursement: boolean;

  constructor(id: string, idSeller: string, idBuyer: string, item: any) {
    this.id = id;
    this.idSeller = idSeller;
    this.idBuyer = idBuyer;
    this.item = item;
    this.sellerAgree = false;
    this.buyerAgree = false;
    this.serviceDelivered = false;
    this.approved = false;
    this.reimbursement = false;
  }
}

export interface PropertyError {
  property: string;
  value?: string | any;
  error: string[];
}

export interface ObjectError {
  entity: string;
  errors: PropertyError[];
}

export class ErrorHelper {
  public static propertyHasErrors(propertyError: PropertyError) {
    if (propertyError.error.length === 0) return false;
    return true;
  }
  public static objectHasErrors(objectErrors: PropertyError[]) {
    if (objectErrors.length === 0) return false;
    return true;
  }

  public static multiplePropertiesHaveErrors(
    properties: any[]
  ): PropertyError[] {
    const errors = new Array<PropertyError>();
    for (const p of properties) {
      //if (property.error.length != 0) return true;
      const newObject = p as PropertyError;
      if (newObject != null) {
        errors.push(newObject);
      }
    }
    return errors;
  }

  public static getPropertiesWithErrors2(properties: PropertyError[]) {
    const withErrors = [];
    for (const property of properties) {
      if (property.error.length != 0) withErrors.push(property);
    }
    return withErrors;
  }
}

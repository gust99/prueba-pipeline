import { PropertyError } from './propertyError';

export interface IGuardResult {
  succeeded: boolean;
  message?: string;
}

export interface IGuardArgument {
  argument: any;
  argumentName: string;
  types?: any[];
}

export type GuardArgumentCollection = IGuardArgument[];

export class Guard {
  public static againstNullOrUndefined(
    argument: any,
    argumentName: string
  ): IGuardResult {
    if (argument === null || argument === undefined) {
      return {
        succeeded: false,
        message: `${argumentName} is null or undefined`
      };
    } else {
      return { succeeded: true };
    }
  }

  public static againstNullOrUndefinedBulk(args: GuardArgumentCollection): {
    succeeded: boolean;
    errors?: string[];
  } {
    const errors: string[] = [];

    for (const arg of args) {
      const result = this.againstNullOrUndefined(
        arg.argument,
        arg.argumentName
      );
      if (!result.succeeded) {
        errors.push(result.message);
      }
    }

    if (errors.length > 0) {
      return { succeeded: false, errors };
    }
    return { succeeded: true };
  }
  public static againstAtLeast(
    numChars: number,
    text: string,
    argumentName: string
  ): IGuardResult {
    return text.length >= numChars
      ? { succeeded: true }
      : {
          succeeded: false,
          message: `${argumentName} is not at least ${numChars} chars.`
        };
  }

  public static againstAtMost(
    numChars: number,
    text: string,
    argumentName: string
  ): IGuardResult {
    return text.length <= numChars
      ? { succeeded: true }
      : {
          succeeded: false,
          message: `${argumentName} is greater than ${numChars} chars.`
        };
  }
  public static againstAtGreaterDate(
    argument: string | Date,
    argumentName: string
  ): IGuardResult {
    if (isNaN(Date.parse(argument.toString()))) {
      return {
        succeeded: false,
        message: `${argumentName} must be in a valid format.`
      };
    }
    const actualYear = new Date().getFullYear();
    const yearToCheck = new Date(argument).getFullYear();
    return yearToCheck < actualYear
      ? { succeeded: true }
      : {
          succeeded: false,
          message: `The ${argumentName} is not lower than the actual year.`
        };
  }

  public static againstValueDifferentType(
    argument: any,
    argumentName: string,
    types: any[]
  ): IGuardResult {
    if (!types.includes(typeof argument)) {
      return {
        succeeded: false,
        message: `${argumentName} is not one of this types: ${types}.`
      };
    } else {
      return { succeeded: true };
    }
  }
  public static againstValueDifferentTypeBulk(args: GuardArgumentCollection): {
    succeeded: boolean;
    errors?: PropertyError[];
  } {
    const errors: PropertyError[] = [];
    for (const arg of args) {
      const result = this.againstValueDifferentType(
        arg.argument,
        arg.argumentName,
        arg.types
      );
      if (!result.succeeded) {
        for (const error of errors) {
          if (error.property === arg.argumentName) {
            error.error.push(result.message);
          }
        }
        errors.push({
          property: arg.argumentName,
          value: arg.argument,
          error: [result.message]
        });
      }
    }
    if (errors.length > 0) {
      return { succeeded: false, errors };
    }

    return { succeeded: true };
  }
  public static isValidDate(date: Date, argumentName: string): IGuardResult {
    return date.toString() != 'Invalid Date'
      ? { succeeded: true }
      : {
          succeeded: false,
          message: `${argumentName} is an invalid date`
        };
  }
  public static isOneOf(
    value: any,
    validValues: any[],
    argumentName: string
  ): IGuardResult {
    let isValid = false;
    for (const validValue of validValues) {
      if (value === validValue) {
        isValid = true;
      }
    }

    if (isValid) {
      return { succeeded: true };
    } else {
      return {
        succeeded: false,
        message: `${argumentName} isn't oneOf the correct types in ${JSON.stringify(
          validValues
        )}. Got "${value}".`
      };
    }
  }
}

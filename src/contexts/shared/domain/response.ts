import { PropertyError } from './propertyError';

export class Response<T> {
  private readonly responseCode: number;
  private readonly content: {
    code: string;
    message: string;
    data?: { errors: [string] } | T;
  };
  constructor(
    responseCode: number,
    content: { code: string; message: string; data?: T }
  ) {
    this.responseCode = responseCode;
    this.content = content;
  }

  get statusCode(): number {
    return this.responseCode;
  }
  get responseContent() {
    return this.content;
  }
  public static create<T>(
    responseCode: number,
    content: { code: string; message: string; data?: T }
  ) {
    return new Response(responseCode, content);
  }

  public static ok<T>(message: string, data?: T): Response<T> {
    return new Response<T>(200, { code: '200', message, data });
  }

  public static created<T>(message: string, data?: T): Response<T> {
    return new Response<T>(201, { code: '201', message, data });
  }

  public static conflict<T>(message: string, data?: T): Response<T> {
    return new Response<T>(409, { code: '409', message, data });
  }

  static internalServerError(): Response<any> {
    return new Response(500, {
      code: '500',
      message: 'Internal Server Error',
      data: {}
    });
  }

  static badRequest(
    message: string,
    errors: string[] | PropertyError[]
  ): Response<any> {
    return new Response(400, {
      code: '400',
      message,
      data: { errors }
    });
  }
  static notFound(message: string): Response<any> {
    return new Response(404, {
      code: '404',
      message,
      data: {}
    });
  }
}

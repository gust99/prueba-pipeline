export type Query<T> = {
  [P in keyof T]?: T[P] | { $regex: RegExp };
};
export interface IRepository<T> {
  findAll(): Promise<T[]>;
  findById(id: string): Promise<T>;
  findManyById(ids: string[]): Promise<T[]>;
  findByQuery(query?: Query<T>): Promise<T[]>;
  update(id: string, item: T): Promise<boolean>;
  save(data: T): Promise<boolean>;
  delete(id: string): Promise<boolean>;
}

export interface ICryptoService {
  getHashedValue(text: string): string;
  bcryptCompare(plainText: string, hashed: string): boolean;
}

const REVIEW_TYPES = {
  REVIEW_CONTROLLER: Symbol.for('ReviewController'),
  REVIEW_SERVICE: Symbol.for('ReviewService'),
  REVIEW_REPOSITORY: Symbol.for('ReviewRepository')
};

export { REVIEW_TYPES };

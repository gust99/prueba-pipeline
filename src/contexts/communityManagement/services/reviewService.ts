import { ServiceTypeOrmRepository } from './../../serviceManagement/repository/serviceRepository';
import { Service } from './../../serviceManagement/domain/service';
import { User } from './../../userManagement/domain/user';
import { Guard } from './../../shared/domain/guard';
import { ReviewMap } from '../presentation/reviewMap';
import { Review } from '../domain/review';
import { UniqueEntityID } from '../../shared/domain/uniqueEntityID';
import { ReviewDate } from '../domain/valueObjects/reviewDate';
import { ReviewRate } from '../domain/valueObjects/reviewRate';
import { ReviewContent } from '../domain/valueObjects/reviewContent';
import { CreateReviewRequestDto } from '../presentation/dtos/createReviewRequestDto';
import { ReviewRepository } from '../repository/IReviewRepository';
import { inject, injectable } from 'inversify';
import { TYPES } from '../../../types';
import { Response } from '../../shared/domain/response';
import { Result } from '../../shared/domain/result';
import { UserTypeOrmRepository } from '../../userManagement/repository/userRepository';
import { ResponseReviewsArrayDto } from '../presentation/dtos/responseReviewsArrayDto';

@injectable()
export class ReviewService {
  constructor(
    @inject(TYPES.REVIEW_REPOSITORY)
    private readonly reviewRepository: ReviewRepository,
    @inject(TYPES.UserRepository)
    private readonly userRepository: UserTypeOrmRepository,
    @inject(TYPES.ServiceRepository)
    private serviceRepository: ServiceTypeOrmRepository
  ) {}

  async createReview(request: CreateReviewRequestDto): Promise<Response<any>> {
    const reviewContentOrError = ReviewContent.create({
      value: request.reviewContent
    });

    const reviewRateOrError = ReviewRate.create({
      value: request.reviewRate
    });

    const reviewDateOrError = ReviewDate.create({
      value: request.reviewDate
    });

    const userIdOrError = new UniqueEntityID(request.userId);
    const serviceIdOrError = new UniqueEntityID(request.serviceId);

    const dtoResult = Result.combine([
      reviewContentOrError,
      reviewRateOrError,
      reviewDateOrError
    ]);

    if (dtoResult.isFailure) {
      return Response.badRequest('Invalid data.', dtoResult.errorValue());
    }

    const reviewContent = reviewContentOrError.getValue() as ReviewContent;
    const reviewRate = reviewRateOrError.getValue() as ReviewRate;
    const reviewDate = reviewDateOrError.getValue() as ReviewDate;

    const reviewOrError = Review.create({
      reviewContent,
      reviewRate,
      reviewDate,
      userId: userIdOrError,
      serviceId: serviceIdOrError
    });

    if (reviewOrError.isFailure) {
      return Response.badRequest('Invalid data', reviewOrError.errorValue());
    }

    const review: Review = reviewOrError.getValue();

    try {
      const userOrError = await this.userRepository.findUser(
        review.userId.toString()
      );

      if (userOrError.isFailure) {
        return Response.notFound(userOrError.errorValue() as string);
      }

      const serviceOrError = await this.serviceRepository.findService(
        review.serviceId.toString()
      );

      if (serviceOrError.isFailure) {
        return Response.notFound(serviceOrError.errorValue() as string);
      }

      const reviewResult = await this.reviewRepository.saveReview(
        ReviewMap.toRepositoryEntity(review)
      );

      if (reviewResult.isFailure) {
        return Response.internalServerError();
      }

      const service = serviceOrError.getValue() as Service;

      const serviceRate = (
        await this.reviewRepository.getServiceRate(service.id.toString())
      ).getValue();

      await this.serviceRepository
        .getRepository()
        .update(service.id.toString(), {
          serviceRate: serviceRate.serviceRate,
          serviceNumOfReviews: serviceRate.reviewsNum
        });

      const userToUpdate: User = (
        await this.userRepository.findUser(service.mentorId.id.toString())
      ).getValue() as User;

      const userServices = await this.serviceRepository.getServicesByUser(
        userToUpdate.id.toString()
      );

      const serviceRates: number[] = [];
      let userNumOfReviews = 0;

      for (let index = 0; index < userServices.length; index++) {
        const s = userServices[index];
        serviceRates.push(s.serviceRate);
        userNumOfReviews += s.serviceNumOfReviews;
      }

      const averageRate = (arr: number[]) =>
        arr.reduce((p, c) => p + c, 0) / arr.length;

      await this.userRepository.update(userToUpdate.id.toString(), {
        userRate: Math.round(averageRate(serviceRates)),
        userNumOfReviews
      });

      return Response.ok(
        'Review registered.',
        ReviewMap.toResponseUserDto(reviewResult.getValue())
      );
    } catch (error) {
      console.log(error);
      return Response.internalServerError();
    }
  }

  public async getServiceReviews(id: string): Promise<Response<any>> {
    try {
      const serviceOrError = await this.serviceRepository.findService(id);

      if (serviceOrError.isFailure) {
        return Response.notFound(serviceOrError.errorValue() as string);
      }

      const serviceReviews = await (
        await this.reviewRepository.getServiceReviews(id)
      ).getValue();
      const reviewsResult: ResponseReviewsArrayDto[] = [];
      for (let index = 0; index < serviceReviews.length; index++) {
        const review = serviceReviews[index];
        const user = await this.userRepository
          .getRepository()
          .findOne(review.userId.toString(), {
            select: ['firstName', 'lastName', 'profileImage']
          });
        reviewsResult.push(ReviewMap.toReviewsArrayItem(user, review));
      }
      return Response.ok('Reviews found.', reviewsResult);
    } catch (error) {
      console.log(error);
      return Response.internalServerError();
    }
  }

  async getUserRate(userId: string) {
    const nullOrUndefinedValidation = Guard.againstNullOrUndefinedBulk([
      { argument: userId, argumentName: 'id' }
    ]);

    if (!nullOrUndefinedValidation.succeeded) {
      return Response.badRequest(
        'Invalid data.',
        nullOrUndefinedValidation.errors
      );
    }

    const differentValueValidation = Guard.againstValueDifferentTypeBulk([
      { argument: userId, argumentName: 'id', types: ['string'] }
    ]);

    if (!differentValueValidation.succeeded) {
      return Response.badRequest(
        'Invalid data.',
        differentValueValidation.errors
      );
    }
    try {
      const userOrError = await this.userRepository.findUser(userId);

      if (userOrError.isFailure) {
        return Response.notFound(userOrError.errorValue() as string);
      }

      const userServices = await this.serviceRepository.getServicesByUser(
        userId
      );
      const serviceRates: number[] = [];
      let userNumOfReviews = 0;
      for (let index = 0; index < userServices.length; index++) {
        const service = userServices[index];
        const serviceRateResult = await this.reviewRepository.getServiceRate(
          service.serviceId
        );
        const serviceRate = serviceRateResult.getValue();
        serviceRates.push(serviceRate.serviceRate);
        userNumOfReviews += serviceRate.reviewsNum;
      }

      const averageRate = (arr: number[]) =>
        arr.reduce((p, c) => p + c, 0) / arr.length;

      return Response.ok('User rate found.', {
        userId,
        userRate: Math.round(averageRate(serviceRates)),
        userNumOfReviews
      });
    } catch (error) {
      console.log(error);
      return Response.internalServerError();
    }
  }

  async getServiceRate(id: string) {
    const nullOrUndefinedValidation = Guard.againstNullOrUndefinedBulk([
      { argument: id, argumentName: 'id' }
    ]);

    if (!nullOrUndefinedValidation.succeeded) {
      return Response.badRequest(
        'Invalid data.',
        nullOrUndefinedValidation.errors
      );
    }

    const differentValueValidation = Guard.againstValueDifferentTypeBulk([
      { argument: id, argumentName: 'id', types: ['string'] }
    ]);

    if (!differentValueValidation.succeeded) {
      return Response.badRequest(
        'Invalid data.',
        differentValueValidation.errors
      );
    }

    try {
      const serviceOrError = await this.serviceRepository.findService(id);

      if (serviceOrError.isFailure) {
        return Response.notFound(serviceOrError.errorValue() as string);
      }
      const serviceRate = (
        await this.reviewRepository.getServiceRate(id)
      ).getValue();
      return Response.ok('Service rate found.', serviceRate);
    } catch (error) {
      console.log(error);
      return Response.internalServerError();
    }
  }
}

import { Column, Entity, PrimaryColumn } from 'typeorm';
@Entity('Review')
export class ReviewEntity {
  @PrimaryColumn()
  id!: string;

  @Column()
  reviewContent!: string;

  @Column()
  reviewRate!: number;

  @Column({ nullable: true })
  reviewDate!: Date;

  @Column()
  userId!: string;

  @Column()
  serviceId!: string;
}

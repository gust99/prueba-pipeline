import { injectable } from 'inversify';
import { EntityRepository, getRepository } from 'typeorm';
import { TypeOrmRepository } from '../../../shared/repository/TypeOrmRepository';
import { Result } from '../../shared/domain/result';
import { Review } from '../domain/review';
import { ReviewMap } from '../presentation/reviewMap';
import { ReviewRepository } from './IReviewRepository';
import { ReviewEntity } from './review.entity';

@injectable()
@EntityRepository(ReviewEntity)
export class ReviewTypeOrmRepository
  extends TypeOrmRepository<ReviewEntity>
  implements ReviewRepository
{
  constructor() {
    super(getRepository(ReviewEntity));
  }

  async saveReview(reviewToSave: ReviewEntity): Promise<Result<Review>> {
    const reviewToSAve = this.getRepository().create(reviewToSave);
    const savedReview = await this.save(reviewToSAve);
    return Result.ok<Review>(ReviewMap.toDomain(savedReview));
  }

  async getServiceReviews(serviceId: string): Promise<Result<Review[]>> {
    const rawReviews = await this.getRepository()
      .createQueryBuilder('review')
      .where('review.serviceId = :id', { id: serviceId })
      .orderBy('review.reviewDate', 'DESC')
      .getMany();
    const reviews: Review[] = [];
    rawReviews.forEach((reviewEntity) => {
      reviews.push(ReviewMap.toDomain(reviewEntity));
    });
    return Result.ok<Review[]>(reviews);
  }

  async getServiceRate(
    serviceId: string
  ): Promise<
    Result<{ serviceId: string; serviceRate: number; reviewsNum: number }>
  > {
    const serviceRate = await this.getRepository()
      .createQueryBuilder('review')
      .select('AVG(review.reviewRate)', 'serviceRate')
      .addSelect('COUNT(*)', 'reviewsNum')
      .where('review.serviceId = :serviceId', {
        serviceId
      })
      .getRawOne();
    return Result.ok({
      serviceId,
      serviceRate: Math.round(serviceRate.serviceRate),
      reviewsNum: Number.parseInt(serviceRate.reviewsNum)
    });
  }
}

import { Review } from '../domain/review';
import { ReviewEntity } from './review.entity';
import { IRepository } from '../../shared/repository/IRepository';
import { Result } from '../../shared/domain/result';

export interface ReviewRepository extends IRepository<ReviewEntity> {
  saveReview(reviewToSave: ReviewEntity): Promise<Result<Review>>;
  getServiceReviews(serviceId: string): Promise<Result<Review[]>>;
  getServiceRate(
    serviceId: string
  ): Promise<
    Result<{ serviceId: string; serviceRate: number; reviewsNum: number }>
  >;
}

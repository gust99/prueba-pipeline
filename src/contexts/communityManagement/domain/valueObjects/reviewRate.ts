import { Result } from '../../../shared/domain/result';
import { Guard } from '../../../shared/domain/guard';
import { ValueObject } from '../../../shared/domain/valueObject';
import { PropertyError } from '../../../shared/domain/propertyError';

interface ReviewRateProps {
  value: number;
  propertyName?: string;
}

export class ReviewRate extends ValueObject<ReviewRateProps> {
  public static maValue = 5;
  public static minValue = 0;
  public static propertyName = 'reviewRate';
  get value(): number {
    return this.props.value;
  }

  get propertyName(): string {
    return this.props.propertyName;
  }

  private constructor(props: ReviewRateProps) {
    super(props);
  }

  public static create(
    props: ReviewRateProps
  ): Result<ReviewRate | PropertyError> {
    const reviewContentResult = Guard.againstNullOrUndefined(
      props.value,
      this.propertyName
    );

    if (!reviewContentResult.succeeded) {
      return Result.fail<PropertyError>({
        property: this.propertyName,
        value: props.value,
        error: [reviewContentResult.message]
      });
    }

    const typeResult = Guard.againstValueDifferentType(
      props.value,
      this.propertyName,
      ['number']
    );

    if (!typeResult.succeeded) {
      return Result.fail<PropertyError>({
        property: this.propertyName,
        value: props.value,
        error: [typeResult.message]
      });
    }
    if (props.value > this.maValue || props.value < this.minValue) {
      return Result.fail<PropertyError>({
        property: this.propertyName,
        value: props.value,
        error: ['The rate value must be between 0 and 5.']
      });
    }
    props.propertyName = this.propertyName;
    return Result.ok<ReviewRate>(new ReviewRate(props));
  }
}

import { Result } from '../../../shared/domain/result';
import { Guard } from '../../../shared/domain/guard';
import { ValueObject } from '../../../shared/domain/valueObject';
import { PropertyError } from '../../../shared/domain/propertyError';

interface ReviewDateProps {
  value: string | Date;
  propertyName?: string;
}

export class ReviewDate extends ValueObject<ReviewDateProps> {
  public static propertyName = 'reviewDate';
  get value(): string | Date {
    return this.props.value;
  }

  get propertyName(): string {
    return this.props.propertyName;
  }

  private constructor(props: ReviewDateProps) {
    super(props);
  }
  private static isValidDate(date: Date): boolean {
    return date instanceof Date && !isNaN(date.getTime());
  }
  public static create(
    props: ReviewDateProps
  ): Result<ReviewDate | PropertyError> {
    const reviewContentResult = Guard.againstNullOrUndefined(
      props.value,
      this.propertyName
    );

    if (!reviewContentResult.succeeded) {
      return Result.fail<PropertyError>({
        property: this.propertyName,
        value: props.value,
        error: [reviewContentResult.message]
      });
    }

    const typeResult = Guard.againstValueDifferentType(
      props.value,
      this.propertyName,
      ['string']
    );

    if (!typeResult.succeeded) {
      return Result.fail<PropertyError>({
        property: this.propertyName,
        value: String(props.value),
        error: [typeResult.message]
      });
    }

    if (!this.isValidDate(new Date(props.value))) {
      return Result.fail<PropertyError>({
        property: this.propertyName,
        value: String(props.value),
        error: ['Invalid date for the review.']
      });
    }

    const reviewDate = new Date(props.value);

    if (new Date().getTime() <= reviewDate.getTime()) {
      return Result.fail<PropertyError>({
        property: this.propertyName,
        value: props.value.toString(),
        error: ['The review date must be lower than the actual date']
      });
    }

    return Result.ok<ReviewDate>(
      new ReviewDate({
        value: reviewDate
      })
    );
  }
}

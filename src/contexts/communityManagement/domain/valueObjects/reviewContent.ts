import { Result } from '../../../shared/domain/result';
import { Guard } from '../../../shared/domain/guard';
import { ValueObject } from '../../../shared/domain/valueObject';
import { PropertyError } from '../../../shared/domain/propertyError';

interface ReviewContentProps {
  value: string;
  propertyName?: string;
}

export class ReviewContent extends ValueObject<ReviewContentProps> {
  public static minLength = 2;
  public static propertyName = 'reviewContent';
  get value(): string {
    return this.props.value;
  }

  get propertyName(): string {
    return this.props.propertyName;
  }

  private constructor(props: ReviewContentProps) {
    super(props);
  }

  public static create(
    props: ReviewContentProps
  ): Result<ReviewContent | PropertyError> {
    const reviewContentResult = Guard.againstNullOrUndefined(
      props.value,
      this.propertyName
    );

    if (!reviewContentResult.succeeded) {
      return Result.fail<PropertyError>({
        property: this.propertyName,
        value: props.value,
        error: [reviewContentResult.message]
      });
    }

    const typeResult = Guard.againstValueDifferentType(
      props.value,
      this.propertyName,
      ['string']
    );

    if (!typeResult.succeeded) {
      return Result.fail<PropertyError>({
        property: this.propertyName,
        value: props.value,
        error: [typeResult.message]
      });
    }

    const minLengthResult = Guard.againstAtLeast(
      this.minLength,
      props.value,
      this.propertyName
    );

    if (!minLengthResult.succeeded) {
      return Result.fail<PropertyError>({
        property: this.propertyName,
        value: props.value,
        error: [minLengthResult.message]
      });
    }

    props.propertyName = this.propertyName;
    return Result.ok<ReviewContent>(new ReviewContent(props));
  }
}

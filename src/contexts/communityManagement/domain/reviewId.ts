import { UniqueEntityID } from '../../shared/domain/uniqueEntityID';
import { Entity } from '../../shared/domain/entity';
import { Result } from '../../shared/domain/result';

export class ReviewId extends Entity<any> {
  get id(): UniqueEntityID {
    return this._id;
  }

  private constructor(id?: UniqueEntityID) {
    super(null, id);
  }

  public static create(id?: UniqueEntityID): Result<ReviewId> {
    return Result.ok<ReviewId>(new ReviewId(id));
  }
}

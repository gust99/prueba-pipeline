import { ReviewId } from './reviewId';
import { ReviewDate } from './valueObjects/reviewDate';
import { ReviewContent } from './valueObjects/reviewContent';
import { UniqueEntityID } from '../../shared/domain/uniqueEntityID';
import { ReviewRate } from './valueObjects/reviewRate';
import { AggregateRoot } from '../../shared/domain/agregateRoot';
import { Result } from '../../shared/domain/result';
import { Guard } from '../../shared/domain/guard';
interface ReviewProps {
  reviewContent: ReviewContent;
  reviewRate: ReviewRate;
  reviewDate: ReviewDate;
  userId: UniqueEntityID;
  serviceId: UniqueEntityID;
}

export class Review extends AggregateRoot<ReviewProps> {
  get reviewId(): ReviewId {
    return ReviewId.create(this._id).getValue();
  }
  get reviewContent(): ReviewContent {
    return this.props.reviewContent;
  }

  get reviewRate(): ReviewRate {
    return this.props.reviewRate;
  }

  get reviewDate(): ReviewDate {
    return this.props.reviewDate;
  }

  get userId(): UniqueEntityID {
    return this.props.userId;
  }

  get serviceId(): UniqueEntityID {
    return this.props.serviceId;
  }

  private constructor(props: ReviewProps, id?: UniqueEntityID) {
    super(props, id);
  }

  public static create(props: ReviewProps, id?: string): Result<any> {
    const guardValueObjectsResult = Guard.againstNullOrUndefinedBulk([
      { argument: props.reviewContent, argumentName: 'reviewContent' },
      { argument: props.reviewRate, argumentName: 'reviewRate' },
      { argument: props.reviewDate, argumentName: 'reviewDate' },
      { argument: props.userId, argumentName: 'userId' },
      { argument: props.serviceId, argumentName: 'serviceId' }
    ]);

    if (!guardValueObjectsResult.succeeded) {
      return Result.multipleFail<string>(guardValueObjectsResult.errors);
    }

    const review = new Review(
      {
        ...props
      },
      new UniqueEntityID(id)
    );

    return Result.ok<Review>(review);
  }
}

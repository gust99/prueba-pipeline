import { plainToInstance } from 'class-transformer';
import { Request, Response } from 'express';
import { inject } from 'inversify';
import {
  BaseHttpController,
  controller,
  httpGet,
  httpPost,
  request,
  requestParam,
  response
} from 'inversify-express-utils';
import { TYPES } from '../../../types';
import { ReviewService } from '../services/reviewService';
import { CreateReviewRequestDto } from './dtos/createReviewRequestDto';

@controller('/reviews')
export class ReviewController extends BaseHttpController {
  constructor(
    @inject(TYPES.REVIEW_SERVICE) private reviewService: ReviewService
  ) {
    super();
  }

  @httpPost('/')
  private async createReview(
    @request() req: Request,
    @response() res: Response
  ) {
    const data = plainToInstance(CreateReviewRequestDto, req.body, {
      excludeExtraneousValues: true
    });
    const response = await this.reviewService.createReview(data);
    res.status(response.statusCode).json(response.responseContent);
  }

  @httpGet('/:id')
  private async getServiceReviews(
    @requestParam('id') id: string,
    @response() res: Response
  ) {
    const response = await this.reviewService.getServiceReviews(id);
    res.status(response.statusCode).json(response.responseContent);
  }

  @httpGet('/user-rate/:id')
  private async getUserRate(
    @requestParam('id') id: string,
    @request() req: Request,
    @response() res: Response
  ) {
    const response = await this.reviewService.getUserRate(id);
    res.status(response.statusCode).json(response.responseContent);
  }

  @httpGet('/service-rate/:id')
  private async getServiceRate(
    @requestParam('id') id: string,
    @request() req: Request,
    @response() res: Response
  ) {
    const response = await this.reviewService.getServiceRate(id);
    res.status(response.statusCode).json(response.responseContent);
  }
}

import { UniqueEntityID } from '../../shared/domain/uniqueEntityID';
import { UserEntity } from '../../userManagement/repository/user.entity';
import { Review } from '../domain/review';
import { ReviewContent } from '../domain/valueObjects/reviewContent';
import { ReviewDate } from '../domain/valueObjects/reviewDate';
import { ReviewRate } from '../domain/valueObjects/reviewRate';
import { ReviewEntity } from '../repository/review.entity';
import { ResponseReviewDto } from './dtos/responseReviewDto';
import { ResponseReviewsArrayDto } from './dtos/responseReviewsArrayDto';

export class ReviewMap {
  static toDomain(raw: ReviewEntity): Review {
    const reviewContent = ReviewContent.create({
      value: raw.reviewContent
    }).getValue() as ReviewContent;

    const reviewRate = ReviewRate.create({
      value: raw.reviewRate
    }).getValue() as ReviewRate;

    const reviewDate = ReviewDate.create({
      value: raw.reviewDate.toString()
    }).getValue() as ReviewDate;

    const userId = new UniqueEntityID(raw.userId);
    const serviceId = new UniqueEntityID(raw.serviceId);

    const review = Review.create({
      reviewContent,
      reviewRate,
      reviewDate,
      userId,
      serviceId
    });
    return review.getValue();
  }

  public static toRepositoryEntity(entity: Review): ReviewEntity {
    return {
      id: entity.id.toString(),
      reviewContent: entity.reviewContent.value,
      reviewRate: entity.reviewRate.value,
      reviewDate: entity.reviewDate.value as Date,
      userId: entity.userId.toString(),
      serviceId: entity.serviceId.toString()
    };
  }

  public static toResponseUserDto(review: Review): ResponseReviewDto {
    return {
      id: review.id.toString(),
      reviewContent: review.reviewContent.value,
      reviewRate: review.reviewRate.value,
      reviewDate: review.reviewDate.value as Date,
      userId: review.userId.toString(),
      serviceId: review.serviceId.toString()
    };
  }
  static toReviewsArrayItem(
    user: UserEntity,
    review: Review
  ): ResponseReviewsArrayDto {
    return {
      reviewContent: review.reviewContent.value,
      reviewRate: review.reviewRate.value,
      reviewDate: review.reviewDate.value as Date,
      userName: user.firstName + ' ' + user.lastName,
      userProfileImage: user.profileImage
    };
  }
}

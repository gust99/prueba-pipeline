export class ResponseReviewDto {
  id: string;

  reviewContent: string;

  reviewRate: number;

  reviewDate: Date;

  userId: string;

  serviceId: string;
}

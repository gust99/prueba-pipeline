import { Expose } from 'class-transformer';

export class CreateReviewRequestDto {
  @Expose()
  reviewContent: string;

  @Expose()
  reviewRate: number;

  @Expose()
  reviewDate: string;

  @Expose()
  userId: string;

  @Expose()
  serviceId: string;
}

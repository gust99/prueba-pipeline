export class ResponseReviewsArrayDto {
  reviewContent: string;

  reviewRate: number;

  reviewDate: Date;

  userName: string;

  userProfileImage: string;
}

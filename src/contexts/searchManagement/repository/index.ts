import { Request } from 'express';
import { injectable } from 'inversify';
import config from '../../../config';

@injectable()
export class SearchRepository {
  public elastic: any;
  public elasticClient: any;
  public host = config.ELASTIC_SEARCH_HOST;

  constructor() {
    this.elastic = require('elasticsearch');
    this.elasticClient = this.elastic.Client({ host: this.host });
  }

  public async searchPublication(req: Request) {
    const query = {
      index: 'publications',
      body: {
        query: {
          multi_match: {
            query: req.query.publication,
            fields: ['title', 'description', 'categories'],
            fuzziness: 'AUTO',
            prefix_length: 2
          }
        },
        size: 20
      }
    };
    const data = await this.elasticClient.search(query);
    return data.hits.hits;
  }
}

import { Request, Response } from 'express';
import { inject } from 'inversify';
import {
  BaseHttpController,
  controller,
  httpGet
} from 'inversify-express-utils';
import { SearchService } from '../services';
import { SEARCH_TYPES } from '../types';

@controller('/search')
export class SearchController extends BaseHttpController {
  constructor(
    @inject(SEARCH_TYPES.searchService)
    private searchService: SearchService
  ) {
    super();
  }

  @httpGet('/')
  private async getPublicationsResults(req: Request, res: Response) {
    const data = await this.searchService.searchPublication(req);
    res.json(data);
  }
}

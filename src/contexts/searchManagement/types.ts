const SEARCH_TYPES = {
  searchController: Symbol.for('SearchController'),
  searchService: Symbol.for('SearchService'),
  searchRepository: Symbol.for('SearchRepository')
};

export { SEARCH_TYPES };

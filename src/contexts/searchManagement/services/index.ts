import { Request } from 'express';
import { inject, injectable } from 'inversify';
import { SearchRepository } from '../repository';
import { SEARCH_TYPES } from '../types';

@injectable()
export class SearchService {
  constructor(
    @inject(SEARCH_TYPES.searchRepository)
    private searchRepository: SearchRepository
  ) {}

  public async searchPublication(req: Request): Promise<any> {
    const response = await this.searchRepository.searchPublication(req);
    return response;
  }
}

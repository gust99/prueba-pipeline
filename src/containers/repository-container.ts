import { AsyncContainerModule, interfaces } from 'inversify';
import { ImageRepository } from '../shared/cloudinary/repository';
import { UserTypeOrmRepository } from '../contexts/userManagement/repository/userRepository';
import { TYPES } from '../types';
import { UserEntity } from '../contexts/userManagement/repository/user.entity';
import { IRepository } from '../contexts/shared/repository/IRepository';
import { IPublicationRepository } from '../contexts/publicationManagement/repository/IPublicationRepository';
import { TypeORMPublicationRepository } from '../contexts/publicationManagement/repository/publication.repository';
import { SearchRepository } from '../contexts/searchManagement/repository';

import { IServiceRepository } from '../contexts/serviceManagement/repository/IServiceRepository';
import { ServiceTypeOrmRepository } from '../contexts/serviceManagement/repository/serviceRepository';
import { IServicePropertyRepository } from '../contexts/serviceProperty/repository/IServicePropertyRepository';
import { ServicePropertyTypeOrmRepository } from '../contexts/serviceProperty/repository/servicePropertyRepository';
import { ReviewRepository } from '../contexts/communityManagement/repository/IReviewRepository';
import { ReviewTypeOrmRepository } from '../contexts/communityManagement/repository/reviewRepository';

export const repositoryContainerModule = new AsyncContainerModule(
  async (bind: interfaces.Bind) => {
    bind<ImageRepository>(TYPES.ImageRepository).to(ImageRepository);
    bind<SearchRepository>(TYPES.searchRepository).to(SearchRepository);
    bind<IRepository<UserEntity>>(TYPES.UserRepository).to(
      UserTypeOrmRepository
    );
    bind<IPublicationRepository>(TYPES.PublicationRepository).to(
      TypeORMPublicationRepository
    );
    bind<IServiceRepository>(TYPES.ServiceRepository).to(
      ServiceTypeOrmRepository
    );
    bind<IServicePropertyRepository>(TYPES.ServicePropertyRepository).to(
      ServicePropertyTypeOrmRepository
    );
    bind<ReviewRepository>(TYPES.REVIEW_REPOSITORY).to(ReviewTypeOrmRepository);
  }
);

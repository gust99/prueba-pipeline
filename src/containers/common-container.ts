import { ContainerModule, interfaces } from 'inversify';
import { ICryptoService } from '../contexts/shared/repository/ICryptoService';
import { IOrm } from '../contexts/shared/repository/IOrm';
import { BcryptJsService } from '../shared/repository/bcryptJsService';
import { Lodash } from '../shared/repository/Lodash';
import { TypeOrmConnection } from '../shared/repository/typeOrmConnection';
import { TYPES } from '../types';
import * as _ from 'lodash';

export const commonContainerModule = new ContainerModule(
  async (bind: interfaces.Bind) => {
    bind<ICryptoService>(TYPES.CryptoService).to(BcryptJsService);
    bind<Lodash>(TYPES.Lodash).toConstantValue(_);
    bind<IOrm>(TYPES.ORM).to(TypeOrmConnection);
  }
);

import { ContainerModule, interfaces } from 'inversify';
import { ImageService } from '../shared/cloudinary/services';
import { UserService } from '../contexts/userManagement/services/userService';
import { TYPES } from '../types';
import { PublicationService } from '../contexts/publicationManagement/services/publication.service';
import { EmailService } from '../shared/emailSender/service/emailService';
import { AuthService } from '../contexts/accessManagement/services/auth.service';
import { SearchService } from '../contexts/searchManagement/services';
import { ServiceManagementService } from '../contexts/serviceManagement/services/serviceManagementServices';
import { ServicePropertyService } from '../contexts/serviceProperty/services/servicePropertyServices';
import { ReviewService } from '../contexts/communityManagement/services/reviewService';

export const serviceContainerModule = new ContainerModule(
  (bind: interfaces.Bind) => {
    bind<SearchService>(TYPES.searchService).to(SearchService);
    bind<ImageService>(TYPES.ImageService).to(ImageService);
    bind<UserService>(TYPES.UserService).to(UserService);
    bind<PublicationService>(TYPES.PublicationService).to(PublicationService);
    bind<EmailService>(TYPES.EmailService).to(EmailService);
    bind<AuthService>(TYPES.AuthService).to(AuthService);
    bind<ServiceManagementService>(TYPES.ServiceManagementService).to(
      ServiceManagementService
    );
    bind<ServicePropertyService>(TYPES.ServicePropertyService).to(
      ServicePropertyService
    );
    bind<ReviewService>(TYPES.REVIEW_SERVICE).to(ReviewService);
  }
);

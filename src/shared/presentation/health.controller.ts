import {
  interfaces,
  controller,
  httpGet,
  request,
  response
} from 'inversify-express-utils';
import express from 'express';

@controller('/health')
export class HealthController implements interfaces.Controller {
  @httpGet('/')
  private async checkHealth(
    @request() req: express.Request,
    @response() res: express.Response
  ) {
    res.status(200).json({ successful: true });
  }
}

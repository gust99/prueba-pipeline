import { NextFunction, Request, Response } from 'express';
import { IncomingHttpHeaders } from 'http';
import jwt, { TokenExpiredError } from 'jsonwebtoken';

export interface IAuthDecodedToken {
  uid: string;
  name: string;
  email: string;
  rol: string;
  iat: number;
  exp: number;
}

const authTokenIsSend = (headers: IncomingHttpHeaders) => {
  return headers && headers.authorization ? true : false;
};

export const loggedUserMw = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  if (!authTokenIsSend(req.headers)) {
    return res.status(401).json({
      code: '401',
      message: 'No token provided'
    });
  }

  try {
    const decodedToken = jwt.verify(
      req.headers.authorization.replace('Bearer ', ''),
      'secret'
    ) as IAuthDecodedToken;
    if (!decodedToken) {
      return res.status(401).json({
        code: '401',
        message: 'Token invalid or not logged in'
      });
    }
  } catch (error) {
    if (error instanceof TokenExpiredError) {
      if (req.route.path === '/auth/refresh') {
        next();
      } else {
        return res.status(403).json({
          code: '403',
          message: 'Token has expired'
        });
      }
    } else {
      return res.status(401).json({
        code: '401',
        message: 'Token invalid or not logged in'
      });
    }
  }

  next();
};

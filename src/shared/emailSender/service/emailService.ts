import { google } from 'googleapis';
import nodemailer from 'nodemailer';
import SMTPTransport from 'nodemailer/lib/smtp-transport';
import { injectable } from 'inversify';
import config from '../../../config/index';
import ejs from 'ejs';
import appRoot from 'app-root-path';

@injectable()
export class EmailService {
  oAuth2Client = new google.auth.OAuth2(
    config.EMAIL_CLIENT_ID,
    config.EMAIL_CLIENT_SECRET,
    config.EMAIL_REDIRECT_URI
  );

  public async sendMail(
    userEmail: { email: string; name: string },
    templateRoute: string,
    subject: string
  ) {
    this.oAuth2Client.setCredentials({
      refresh_token: config.EMAIL_REFRESH_TOKEN
    });

    ejs.renderFile(
      appRoot.path + `/src/shared/views/${templateRoute}`,
      { name: userEmail.name },
      async (error, data) => {
        try {
          const accessToken = await this.oAuth2Client.getAccessToken();
          const transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
              type: 'OAuth2',
              user: config.EMAIL_USER,
              clientId: config.EMAIL_CLIENT_ID,
              clientSecret: config.EMAIL_CLIENT_SECRET,
              refreshToken: config.EMAIL_REFRESH_TOKEN,
              accessToken: accessToken
            }
          } as SMTPTransport.Options);
          const mailOptions = {
            from: config.EMAIL,
            to: userEmail.email,
            subject: subject,
            html: data
          };

          await transporter.sendMail(mailOptions);
          // console.log(result);
          // return result;
        } catch (err) {
          console.log(err);
        }
      }
    );
  }
}

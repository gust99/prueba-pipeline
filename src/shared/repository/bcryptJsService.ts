import * as bcrypt from 'bcryptjs';
import { injectable } from 'inversify';
import { ICryptoService } from '../../contexts/shared/repository/ICryptoService';

@injectable()
export class BcryptJsService implements ICryptoService {
  getHashedValue(text: string): string {
    const salt = bcrypt.genSaltSync(10);
    return bcrypt.hashSync(text, salt);
  }
  bcryptCompare(plainText: string, hashed: string): boolean {
    return bcrypt.compareSync(plainText, hashed);
  }
}

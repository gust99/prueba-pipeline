import {
  IRepository,
  Query
} from '../../contexts/shared/repository/IRepository';
import { injectable, unmanaged } from 'inversify';
import { Repository } from 'typeorm';

@injectable()
export abstract class TypeOrmRepository<TEntity>
  implements IRepository<TEntity>
{
  private readonly repository: Repository<TEntity>;

  public constructor(@unmanaged() repository: Repository<TEntity>) {
    this.repository = repository;
  }

  public async findAll(): Promise<TEntity[]> {
    return await this.repository.find();
  }

  public async findById(id: string): Promise<TEntity> {
    return await this.repository.findOne(id);
  }

  public async findManyById(ids: string[]): Promise<TEntity[]> {
    return await this.repository.findByIds(ids);
  }

  public async findByQuery(query: Query<TEntity>): Promise<TEntity[]> {
    return await this.repository.find(query);
  }

  public async update(id: string, data: any): Promise<boolean> {
    const result = await this.repository.update(id, data);
    return !!result;
  }

  public async delete(id: string): Promise<boolean> {
    const result = await this.repository.delete(id);
    return !!result;
  }

  public async save(data: any) {
    const result = await this.repository.save(data);
    return result;
  }
  public getRepository(): Repository<TEntity> {
    return this.repository;
  }
}

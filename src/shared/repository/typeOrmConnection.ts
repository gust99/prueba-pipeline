import { injectable } from 'inversify';
import { createConnection } from 'typeorm';
import config from '../../config';
import { IOrm } from '../../contexts/shared/repository/IOrm';

@injectable()
export class TypeOrmConnection implements IOrm {
  public async initialize(): Promise<void> {
    await createConnection({
      type: 'mysql',
      host: config.DB_HOST,
      port: config.DB_PORT,
      username: config.DB_USER,
      password: config.DB_PASSWORD,
      database: config.DB_NAME,
      entities: config.DB_ENTITIES,
      synchronize: config.DB_SYNCRO
    });
  }
}

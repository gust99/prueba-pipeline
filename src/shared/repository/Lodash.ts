import * as _ from 'lodash';

export type Lodash = typeof _;

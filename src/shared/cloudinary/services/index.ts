import { injectable, inject } from 'inversify';
import { IMAGE_TYPES } from '../types';
import { ImageRepository } from '../repository/index';

interface IRequest {
  id: string;
}

interface IResponse {
  userId: string;
  urlImage: string;
}

@injectable()
export class ImageService {
  constructor(
    @inject(IMAGE_TYPES.ImageRepository)
    private imageRepository: ImageRepository
  ) {}
  public async upload(userId: string, imgBase64: string): Promise<IResponse> {
    const response = await this.imageRepository.uploadImage(userId, imgBase64);
    return response;
  }
  public async delete(imageId: string): Promise<IResponse> {
    const response = await this.imageRepository.deleteImage(imageId);
    return response;
  }
  public async uploadProfileImg(
    userId: string,
    imgUrl: string,
    urlToDelete: string
  ): Promise<IResponse> {
    const response = await this.imageRepository.uploadProfileImage(
      userId,
      imgUrl
    );
    const deleteFrontImg = await this.imageRepository.deleteImage(urlToDelete);
    return response;
  }
}

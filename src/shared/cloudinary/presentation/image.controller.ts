import {
  controller,
  httpGet,
  BaseHttpController,
  httpDelete,
  requestParam,
  response,
  httpPost,
  request
} from 'inversify-express-utils';
import { inject } from 'inversify';
import { IMAGE_TYPES } from '../types';
import { ImageService } from '../services';
import * as express from 'express';
import { example } from '../repository/base64Test';
import { loggedUserMw } from '../../presentation/middleware/tokenMiddleware/tokenverifier.middleware';
import { plainToInstance } from 'class-transformer';
import { CreateUserRequestDto } from '../../../contexts/userManagement/presentation/dtos/createUserRequestDto';
import { PROFILE_TYPES } from '../../../contexts/userManagement/types';
import { UserService } from '../../../contexts/userManagement/services/userService';

@controller('/content/images')
export class ImageController extends BaseHttpController {
  constructor(
    @inject(IMAGE_TYPES.ImageService) private imageService: ImageService,
    @inject(PROFILE_TYPES.UserService) private userService: UserService
  ) {
    super();
  }

  @httpPost('/')
  private async upload(
    @request() req: express.Request,
    @response() res: express.Response
  ) {
    try {
      console.log('llegó al server');
      const { userId, base64Img } = req.body;
      const cloudinaryResponse = await this.imageService.upload(
        userId,
        base64Img
      );
      const response = {
        code: 201,
        message: 'The image is successfully uploaded to the Cloud.',
        data: cloudinaryResponse
      };
      res.status(201).json(response);
    } catch (err) {
      res.status(400).json({
        code: 400,
        error: err.message
      });
    }
  }

  @httpPost('/profile-img', loggedUserMw)
  private async uploadProfileImg(
    // @requestParam('id') id: string,
    @request() req: express.Request,
    @response() res: express.Response
  ) {
    try {
      console.log('llegó al server from profile /:id');
      console.log(req.body);
      const { userId, urlImg, urlToDelete } = req.body;
      const cloudinaryResponse = await this.imageService.uploadProfileImg(
        userId,
        urlImg,
        urlToDelete
      );
      console.log('cloudinary response');
      console.log(cloudinaryResponse);
      const data_to_update = { profileImage: cloudinaryResponse.urlImage };
      const data = plainToInstance(CreateUserRequestDto, data_to_update, {
        excludeExtraneousValues: true,
        strategy: 'excludeAll'
      });
      const response1 = await this.userService.updateUser(userId, data);
      console.log(response1);
      const response = {
        code: 201,
        message: 'The image is successfully uploaded to the Cloud.',
        data: cloudinaryResponse
      };
      res.status(201).json(response);
    } catch (err) {
      res.status(400).json({
        code: 400,
        error: err.message
      });
    }
  }

  // @httpPut('/:id', loggedUserMw)
  // private async updateUser(
  //   @requestParam('id') id: string,
  //   @request() req: Request,
  //   @response() res: Response
  // ) {
  //   const data = plainToInstance(CreateUserRequestDto, req.body, {
  //     excludeExtraneousValues: true,
  //     strategy: 'excludeAll'
  //   });
  //   const response = await this.userService.updateUser(id, data);
  //   res.status(response.statusCode).json(response.responseContent);
  // }

  @httpDelete('/')
  private async delete(
    @request() req: express.Request,
    @response() res: express.Response
  ) {
    try {
      const { cloudinaryImgId } = req.body;
      const cloudinaryResponse = await this.imageService.delete(
        cloudinaryImgId
      );
      const response = {
        code: 201,
        message: 'The image was successfully deleted.',
        data: cloudinaryResponse
      };
      res.status(201).json(response);
    } catch (err) {
      res.status(400).json({ error: err.message });
    }
  }
}

import { injectable } from 'inversify';
import cloudinary from 'cloudinary';
import { example } from './base64Test';
import * as config from './config';

@injectable()
export class ImageRepository {
  public Cloudname = config.Cloudname;
  public API_Key = config.API_Key;
  public API_Secret = config.API_Secret;

  constructor() {
    cloudinary.v2.config({
      cloud_name: this.Cloudname,
      api_key: this.API_Key,
      api_secret: this.API_Secret
    });
  }

  public async uploadImage(userId: string, imgBase64: string) {
    const result = await cloudinary.v2.uploader.upload(
      imgBase64,
      { tags: userId },
      function (error, result) {
        console.log(result);
      }
    );

    const response = {
      userId: userId,
      urlImage: result.secure_url,
      cloudinaryImgId: result.public_id
    };
    return response;
  }

  public async deleteImage(imageId: string) {
    console.log('llegó a deleteImage del repository');
    const result = await cloudinary.v2.uploader.destroy(
      imageId,
      {},
      function (error, result) {
        console.log(result);
      }
    );
    return result.result;
  }

  public async uploadProfileImage(userId: string, imgUrl: string) {
    const result = await cloudinary.v2.uploader.upload(
      imgUrl,
      { tags: userId },
      function (error, result) {
        console.log(result);
      }
    );

    const response = {
      userId: userId,
      urlImage: result.secure_url,
      cloudinaryImgId: result.public_id
    };
    return response;
  }

  // public async createSignature(userId: string, imgUrl: string) {
  //   const result = await cloudinary.v2.uploader.upload(
  //     imgUrl,
  //     { tags: userId },
  //     function (error, result) {
  //       console.log(result);
  //     }
  //   );

  //   const response = {
  //     userId: userId,
  //     urlImage: result.secure_url,
  //     cloudinaryImgId: result.public_id
  //   };
  //   return response;
  // }
}

const IMAGE_TYPES = {
  ImageController: Symbol.for('ImageController'),
  ImageService: Symbol.for('ImageService'),
  ImageRepository: Symbol.for('ImageRepository')
};

export { IMAGE_TYPES };

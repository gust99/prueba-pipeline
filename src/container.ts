import 'reflect-metadata';

import { Container } from 'inversify';
import { repositoryContainerModule } from './containers/repository-container';
import { serviceContainerModule } from './containers/service-container';
import { IServer, Server } from './server';
import { TYPES } from './types';
import { commonContainerModule } from './containers/common-container';

const container = new Container();
container.load(commonContainerModule);

container.load(serviceContainerModule);
container.loadAsync(repositoryContainerModule);

container.bind<IServer>(TYPES.Server).to(Server).inSingletonScope();

export { container };

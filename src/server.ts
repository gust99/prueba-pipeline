import express, { Application } from 'express';
import { injectable } from 'inversify';
import { InversifyExpressServer } from 'inversify-express-utils';
import config from './config';
import { container } from './container';
import './containers/presentation-container';
import swaggerUi from 'swagger-ui-express';
import swaggerDocument from './swagger.json';
import cors from 'cors';
import expressEjsLayouts = require('express-ejs-layouts');

export interface IServer {
  start(): void;
}

@injectable()
export class Server implements IServer {
  server: InversifyExpressServer;
  version = '/api/v1';
  start(): void {
    this.server = new InversifyExpressServer(container, null, {
      rootPath: this.version
    });
    this.server.setConfig((app: Application) => {
      app.use(function (req, res, next) {
        res.header('Access-Control-Allow-Origin', '*');
        res.header(
          'Access-Control-Allow-Headers',
          'Origin, X-Requested-With, Content-Type, Accept'
        );
        next();
      });
      app.set('view engine', 'ejs');
      app.use(expressEjsLayouts);
      app.use(cors());
      app.use(
        express.urlencoded({
          extended: true
        })
      );
      // eslint-disable-next-line prettier/prettier
      app.use(express.json({ limit: '50mb' }));
      // eslint-disable-next-line prettier/prettier
      app.use(express.urlencoded({ limit: '50mb', extended: true }));
      app.use('/swagger', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
    });

    // this.server.setErrorConfig((app: Application) => {
    //   app.use(errorHandler);
    // });

    const apiServer = this.server.build();
    apiServer.listen(config.API_PORT, () =>
      console.log(
        `⚡️[server]: Server is running at http://localhost:${config.API_PORT}${this.version}`
      )
    );
  }
}

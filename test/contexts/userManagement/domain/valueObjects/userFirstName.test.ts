import { PropertyError } from '../../../../../src/contexts/shared/domain/propertyError';
import { UserFirstName } from '../../../../../src/contexts/userManagement/domain/valueObjects/userFirstName';

describe('Create UserFirstName ValueObject', () => {
  it('Should create a userFirstName', () => {
    const newUserFirstName = UserFirstName.create({ value: 'testName' });
    const firstName = newUserFirstName.getValue() as UserFirstName;
    expect(newUserFirstName.isSuccess).toEqual(true);
    expect(firstName.value).toEqual('testName');
    expect(firstName.propertyName).toEqual('firstName');
  });

  it('Should return null or undefined error', () => {
    const newUserFirstName = UserFirstName.create({
      value: undefined
    });
    const error = newUserFirstName.errorValue() as PropertyError;
    expect(newUserFirstName.isFailure).toEqual(true);
    expect(error.error[0]).toEqual('firstName is null or undefined');
  });

  it('Should return min length error', () => {
    const newUserFirstName = UserFirstName.create({ value: 'a' });
    const error = newUserFirstName.errorValue() as PropertyError;
    expect(newUserFirstName.isFailure).toEqual(true);
    expect(error.error[0]).toEqual('firstName is not at least 2 chars.');
  });

  it('Should return max length error', () => {
    const newUserFirstName = UserFirstName.create({
      value: 'testNameWithMoreThanFiftyCharacters'
    });
    const error = newUserFirstName.errorValue() as PropertyError;
    expect(newUserFirstName.isFailure).toEqual(true);
    expect(error.error[0]).toEqual('firstName is greater than 30 chars.');
  });
});

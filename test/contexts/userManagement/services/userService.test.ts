import { Result } from '../../../../src/contexts/shared/domain/result';
import { ICryptoService } from '../../../../src/contexts/shared/repository/ICryptoService';
import { UserRole } from '../../../../src/contexts/userManagement/domain/user';
import { UpdateUserRequestDto } from '../../../../src/contexts/userManagement/presentation/dtos/updateUserRequestDto';
import { UserMap } from '../../../../src/contexts/userManagement/presentation/userMap';
import { UserEntity } from '../../../../src/contexts/userManagement/repository/user.entity';
import { UserTypeOrmRepository } from '../../../../src/contexts/userManagement/repository/userRepository';
import { UserService } from '../../../../src/contexts/userManagement/services/userService';
import { EmailService } from '../../../../src/shared/emailSender/service/emailService';
import { BcryptJsService } from '../../../../src/shared/repository/bcryptJsService';
import { TypeOrmRepository } from '../../../../src/shared/repository/TypeOrmRepository';

jest.mock('../../../../src/shared/repository/bcryptJsService');
jest.mock('../../../../src/contexts/userManagement/repository/userRepository');
jest.mock('../../../../src/shared/emailSender/service/emailService');

describe('User Service Test', () => {
  let cryptoService: ICryptoService;
  let userRepository: UserTypeOrmRepository;
  let userService: UserService;
  let emailService: EmailService;

  beforeEach(() => {
    cryptoService = new BcryptJsService();
    userRepository = new UserTypeOrmRepository();
    emailService = new EmailService();

    userService = new UserService(userRepository, cryptoService, emailService);
    jest
      .spyOn(BcryptJsService.prototype, 'getHashedValue')
      .mockReturnValue('expectedProduct');
  });

  afterAll(() => {
    jest.restoreAllMocks();
  });

  const mockUserEntity: UserEntity = {
    id: 'id',
    firstName: 'firstName',
    lastName: 'lasName',
    dateOfBirth: new Date('01-01-1980'),
    email: 'email@test.com',
    password: 'password',
    isEmailVerified: false,
    isAdminUser: false,
    isDeleted: false,
    lastLogin: new Date(),
    userRol: UserRole.STUDENT
  };

  const mockCreateUserRequest = {
    firstName: 'firstName',
    lastName: 'lastName',
    dateOfBirth: '01-01-1980',
    email: 'email@test.com',
    password: 'password'
  };
  const mockUpdateUserRequest: UpdateUserRequestDto = {
    firstName: 'firstName',
    lastName: 'lastName',
    dateOfBirth: '01-01-1980',
    email: 'email@test.com',
    password: 'password',
    phone: '+59178212331',
    address: {
      street: 'street',
      city: 'city',
      state: 'state',
      country: 'country'
    },
    social: {
      facebook: 'facebook',
      linkedin: 'linkedin',
      website: 'website',
      youtube: 'youtube',
      googlehangouts: 'googlehangouts',
      twitter: 'twitter'
    },
    profileImage: 'profileImage'
  };

  const mockUpdateUserRequestFail: UpdateUserRequestDto = {
    firstName: 'a',
    lastName: 'a',
    dateOfBirth: 'safa',
    email: 'a',
    password: 'pass',
    phone: '123'
  };

  it('Should create a user service.', () => {
    expect(userService).toBeTruthy;
  });

  it('Should create a user', async () => {
    const user = UserMap.toDomain(mockUserEntity);
    jest
      .spyOn(UserTypeOrmRepository.prototype, 'existEmail')
      .mockReturnValue(Promise.resolve(false));
    jest
      .spyOn(UserTypeOrmRepository.prototype, 'addUser')
      .mockReturnValue(Promise.resolve(Result.ok(user)));
    const response = await userService.createUser(mockCreateUserRequest);

    jest
      .spyOn(EmailService.prototype, 'sendMail')
      .mockReturnValue(Promise.resolve());

    expect(response.statusCode).toBe(200);
    expect(response.responseContent.message).toBe('User registered.');
  });

  it('Should get a user', async () => {
    const user = UserMap.toDomain(mockUserEntity);
    jest
      .spyOn(UserTypeOrmRepository.prototype, 'findUser')
      .mockReturnValue(Promise.resolve(Result.ok(user)));

    const response = await userService.getUser('');
    expect(response.statusCode).toBe(200);
    expect(response.responseContent.message).toBe('User found.');
    expect(response.responseContent.data.id).toBe('id');
  });

  it('Should update a user', async () => {
    const user = UserMap.toDomain(mockUserEntity);
    jest
      .spyOn(UserTypeOrmRepository.prototype, 'findUser')
      .mockReturnValue(Promise.resolve(Result.ok(user)));

    jest
      .spyOn(UserTypeOrmRepository.prototype, 'existEmail')
      .mockReturnValue(Promise.resolve(false));

    jest
      .spyOn(TypeOrmRepository.prototype, 'update')
      .mockReturnValue(Promise.resolve(true));

    const response = await userService.updateUser('id', mockUpdateUserRequest);

    expect(response.statusCode).toBe(200);
    expect(response.responseContent.message).toBe('User updated.');
  });

  it('Should throw error at update a user', async () => {
    const user = UserMap.toDomain(mockUserEntity);
    jest
      .spyOn(UserTypeOrmRepository.prototype, 'findUser')
      .mockReturnValue(Promise.resolve(Result.ok(user)));

    jest
      .spyOn(UserTypeOrmRepository.prototype, 'existEmail')
      .mockReturnValue(Promise.resolve(false));

    jest
      .spyOn(TypeOrmRepository.prototype, 'update')
      .mockReturnValue(Promise.resolve(true));

    const response = await userService.updateUser(
      'id',
      mockUpdateUserRequestFail
    );

    const responseData = response.responseContent.data;

    expect(response.statusCode).toBe(400);
    expect(response.responseContent.message).toBe('Invalid data.');
    expect(responseData.errors.length).toBe(5);
  });

  it('Should delete a user', async () => {
    jest
      .spyOn(UserTypeOrmRepository.prototype, 'deleteUser')
      .mockReturnValue(Promise.resolve(Result.ok(true)));

    const response = await userService.deleteUser('');
    expect(response.statusCode).toBe(200);
    expect(response.responseContent.message).toBe('User deleted.');
  });

  it('Should throw and error at delete a user', async () => {
    jest
      .spyOn(UserTypeOrmRepository.prototype, 'deleteUser')
      .mockReturnValue(Promise.resolve(Result.fail('User Not Found.')));

    const response = await userService.deleteUser('');
    expect(response.statusCode).toBe(404);
    expect(response.responseContent.message).toBe('User Not Found.');
  });
});

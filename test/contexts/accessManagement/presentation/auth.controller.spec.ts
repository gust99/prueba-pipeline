import { AuthController } from '../../../../src/contexts/accessManagement/presentation/auth.controller';

describe('AuthController test', () => {
  const authController = new AuthController();

  it('Testing auth controller definition', () => {
    expect(authController).toBeDefined();
  });
});

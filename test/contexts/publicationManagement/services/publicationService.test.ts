import { UserTypeOrmRepository } from '../../../../src/contexts/userManagement/repository/userRepository';
import { TypeORMPublicationRepository } from '../../../../src/contexts/publicationManagement/repository/publication.repository';
import { PublicationService } from '../../../../src/contexts/publicationManagement/services/publication.service';
import { RequestPublicationDTO } from '../../../../src/contexts/publicationManagement/presentation/dto/requestPublication.dto';
import { PublicationMap } from '../../../../src/contexts/publicationManagement/services/mappers/publication.map';
import { ServiceTypeOrmRepository } from '../../../../src/contexts/serviceManagement/repository/serviceRepository';
import { Result } from '../../../../src/contexts/shared/domain/result';
import { Service } from '../../../../src/contexts/serviceManagement/domain/service';
import { ServiceTitle } from '../../../../src/contexts/serviceManagement/domain/valueObjects/serviceTitle';
import { ServiceDescription } from '../../../../src/contexts/serviceManagement/domain/valueObjects/serviceDescription';
import { Image } from '../../../../src/contexts/serviceManagement/domain/valueObjects/image';
import { UserId } from '../../../../src/contexts/userManagement/domain/userId';
import { UserEntity } from '../../../../src/contexts/userManagement/repository/user.entity';

jest.mock('../../../../src/shared/repository/bcryptJsService');
jest.mock('../../../../src/contexts/userManagement/repository/userRepository');
jest.mock(
  '../../../../src/contexts/serviceManagement/repository/serviceRepository'
);

jest.mock(
  '../../../../src/contexts/publicationManagement/repository/publication.repository'
);

describe('Publication Service Test', () => {
  let userRepository: UserTypeOrmRepository;
  let publicationService: PublicationService;
  let publicationRepository: TypeORMPublicationRepository;
  let serviceRepository: ServiceTypeOrmRepository;

  beforeEach(() => {
    userRepository = new UserTypeOrmRepository();
    publicationRepository = new TypeORMPublicationRepository();
    serviceRepository = new ServiceTypeOrmRepository();
    publicationService = new PublicationService(
      publicationRepository,
      userRepository,
      serviceRepository
    );
  });

  afterAll(() => {
    jest.restoreAllMocks();
  });

  const publicationDTORequest: RequestPublicationDTO = {
    userId: 'KjA8VCvza5VeqJrkw66MB0f6jmm2',
    courseId: 'c209c3b6-ffdb-4e0b-b06e-74b5fff65fa2',
    title: 'Python for Data Analysis',
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco',
    description2:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco',
    status: 0,
    rate: 20,
    currencyType: 0,
    image: '',
    categories: [],
    address: '',
    online: true,
    house: false,
    travel: false,
    languages: [],
    date: null,
    latitude: '',
    longitude: '',
    extraFee: {
      rate: 20,
      currencyType: 0,
      extraFeeType: 0
    },
    packageA: {
      value: '20',
      type: 0,
      rate: 20,
      currencyType: 0
    },
    packageB: {
      value: 'Full course',
      type: 0,
      rate: 200,
      currencyType: 0
    },
    packageC: {
      value: '',
      type: 0,
      rate: 0,
      currencyType: 0
    },
    discount: {
      rate: 20,
      currencyType: 0,
      discountType: 0
    }
  };
  const publicationDTORequestError: RequestPublicationDTO = {
    userId: 'KjA8VCvza5VeqJrkw66MB0f6jmm2',
    courseId: 'c209c3b6-ffdb-4e0b-b06e-74b5fff65fa2',
    title: 'Python for Data Analysis',
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco',
    description2:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco',
    status: 4,
    rate: 20,
    currencyType: 0,
    image: '',
    categories: [],
    address: '',
    online: true,
    house: false,
    travel: false,
    languages: [],
    date: null,
    latitude: '',
    longitude: '',
    packageA: {
      value: '20',
      type: 0,
      rate: 20,
      currencyType: 0
    },
    packageB: {
      value: 'Full course',
      type: 0,
      rate: 200,
      currencyType: 0
    },
    packageC: {
      value: '',
      type: 0,
      rate: 0,
      currencyType: 0
    },
    discount: {
      rate: 0,
      currencyType: 0,
      discountType: 0
    },
    extraFee: {
      rate: 20,
      currencyType: 0,
      extraFeeType: 0
    }
  };

  it('should create a publication service.', () => {
    expect(publicationService).toBeTruthy;
  });
  it('should create a publication', async () => {
    jest
      .spyOn(TypeORMPublicationRepository.prototype, 'save')
      .mockReturnValue(Promise.resolve(true));
    jest
      .spyOn(UserTypeOrmRepository.prototype, 'findById')
      .mockReturnValue(Promise.resolve(new UserEntity()));
    jest
      .spyOn(ServiceTypeOrmRepository.prototype, 'findService')
      .mockReturnValue(
        Promise.resolve(
          Result.ok(
            Service.create({
              mentorId: UserId.create().getValue(),
              serviceTitle: ServiceTitle.create({
                value: 'Service title'
              }).getValue() as ServiceTitle,
              serviceDescription: ServiceDescription.create({
                value:
                  'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco'
              }).getValue() as ServiceDescription,
              serviceImage: Image.create({ value: '' }).getValue() as Image
            }).getValue()
          )
        )
      );

    const response = await publicationService.save(publicationDTORequest);
    expect(response.statusCode).toBe(200);
    expect(response.responseContent.message).toBe('Publication created');
  });

  it('should get a publication', async () => {
    const entity = PublicationMap.toPersistence(
      PublicationMap.toDomainFromDTO(publicationDTORequest)
    );
    jest
      .spyOn(TypeORMPublicationRepository.prototype, 'findById')
      .mockReturnValue(Promise.resolve(entity));

    const response = await publicationService.findOneById(entity.id);

    expect(response.statusCode).toBe(200);
    expect(response.responseContent.message).toBe('Publication found');
  });

  it('Should update a publication', async () => {
    const entity = PublicationMap.toPersistence(
      PublicationMap.toDomainFromDTO(publicationDTORequest)
    );
    jest
      .spyOn(TypeORMPublicationRepository.prototype, 'update')
      .mockReturnValue(Promise.resolve(true));
    jest
      .spyOn(TypeORMPublicationRepository.prototype, 'findById')
      .mockReturnValue(Promise.resolve(entity));
    const response = await publicationService.update(
      entity.id,
      publicationDTORequest
    );
    expect(response.statusCode).toBe(200);
    expect(response.responseContent.message).toBe('Publication updated');
  });

  it('Should throw error at update a publication', async () => {
    const entity = PublicationMap.toPersistence(
      PublicationMap.toDomainFromDTO(publicationDTORequestError)
    );
    jest
      .spyOn(TypeORMPublicationRepository.prototype, 'update')
      .mockReturnValue(Promise.resolve(true));
    jest
      .spyOn(TypeORMPublicationRepository.prototype, 'findById')
      .mockReturnValue(Promise.resolve(entity));
    const response = await publicationService.update(
      entity.id,
      publicationDTORequestError
    );
    expect(response.statusCode).toBe(400);
    expect(response.responseContent.message).toBe('Invalid data');
  });

  it('Should delete a publication', async () => {
    jest
      .spyOn(TypeORMPublicationRepository.prototype, 'delete')
      .mockReturnValue(Promise.resolve(true));

    const response = await publicationService.delete('');
    expect(response.statusCode).toBe(200);
    expect(response.responseContent.message).toBe('Publication deleted');
  });

  it('Should throw and error at delete a publication', async () => {
    jest
      .spyOn(TypeORMPublicationRepository.prototype, 'delete')
      .mockReturnValue(Promise.resolve(true));
    jest
      .spyOn(TypeORMPublicationRepository.prototype, 'findById')
      .mockReturnValue(Promise.resolve(undefined));
    const response = await publicationService.delete('');
    expect(response.statusCode).toBe(404);
    expect(response.responseContent.message).toBe('Publication not found');
  });
});

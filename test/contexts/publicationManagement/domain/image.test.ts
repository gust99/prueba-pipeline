import { Image } from '../../../../src/contexts/publicationManagement/domain/image';

describe('Create image', () => {
  it('should create a new image', () => {
    const newImage = Image.create({ value: 'pathToImageOrBlob' }).getValue();
    expect(newImage).toEqual({
      props: {
        value: 'pathToImageOrBlob'
      }
    });
  });

  it('should create an image with default value when null', () => {
    const newImage = Image.create({ value: null }).getValue();
    expect(newImage).toEqual({
      props: {
        value: Image.defaultImage
      }
    });
  });
});

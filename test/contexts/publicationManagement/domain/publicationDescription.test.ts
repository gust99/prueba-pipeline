import { PublicationDescription } from '../../../../src/contexts/publicationManagement/domain/publication/publicationDescription';
import { PropertyError } from '../../../../src/contexts/shared/domain/propertyError';

describe('Create Description', () => {
  it('Should create the description', () => {
    const newDescription = PublicationDescription.create({
      value:
        'descriptiondescriptiondescriptiondescriptiondescriptiondescription'
    }).getValue();
    expect(newDescription).toEqual({
      props: {
        value:
          'descriptiondescriptiondescriptiondescriptiondescriptiondescription'
      }
    });
  });
  it('Should return min length error', () => {
    const newDescription = PublicationDescription.create({ value: '' });
    expect((newDescription.error as PropertyError).error).toContain(
      `${PublicationDescription.propName} is not at least 20 chars.`
    );
  });

  it('Should return max length error', () => {
    const newDescription = PublicationDescription.create({
      value:
        'descriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescription'
    });
    expect((newDescription.error as PropertyError).error).toEqual([
      `${PublicationDescription.propName} is greater than 400 chars.`
    ]);
  });

  it('Should return null or undefined error', () => {
    const newDescription = PublicationDescription.create({
      value: null
    });
    expect((newDescription.error as PropertyError).error).toContain(
      `${PublicationDescription.propName} is null or undefined`
    );
  });
});

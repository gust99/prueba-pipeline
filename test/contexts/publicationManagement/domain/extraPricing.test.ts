import { ExtraPricing } from '../../../../src/contexts/publicationManagement/domain/extraPricing';
import { Rate } from '../../../../src/contexts/publicationManagement/domain/rate';

describe('Create ExtraPricing', () => {
  it('Should create an extra pricing', () => {
    const newPricing = ExtraPricing.create({
      rate: Rate.create({ value: 20, currencyType: 1 }).getValue() as Rate,
      feeOrDiscountType: 0,
      type: 1
    }).getValue();
    expect(newPricing);
  });
  it('Should return default values when null', () => {
    const newPricing = ExtraPricing.create({
      rate: Rate.create({ value: 20, currencyType: 1 }).getValue() as Rate,
      feeOrDiscountType: null,
      type: null
    });
    expect(newPricing);
  });
  it('should return error', () => {
    const newPricing = ExtraPricing.create({
      rate: Rate.create({ value: 20, currencyType: 1 }).getValue() as Rate,
      feeOrDiscountType: 8,
      type: 1
    });
    expect(newPricing);
  });
});

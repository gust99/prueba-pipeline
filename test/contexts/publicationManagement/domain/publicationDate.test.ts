import { PublicationDate } from '../../../../src/contexts/publicationManagement/domain/publication/publicationDate';
describe('Create a date', () => {
  it('should create a valid date', () => {
    const newDate = PublicationDate.create({
      dateTimePublished: new Date('01/01/01')
    }).getValue();

    expect(newDate).toEqual({
      props: {
        dateTimePublished: new Date('01/01/01')
      }
    });
  });

  it('should create date with default value when null', () => {
    const newDate = PublicationDate.create({
      dateTimePublished: null
    }).getValue() as PublicationDate;
    expect(newDate).toEqual({
      props: {
        dateTimePublished: newDate.date
      }
    });
  });
});

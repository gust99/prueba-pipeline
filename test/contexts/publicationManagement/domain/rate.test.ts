import { Rate } from '../../../../src/contexts/publicationManagement/domain/rate';
describe('Create Rate', () => {
  it('Should create a rate', () => {
    const newRate = Rate.create({ value: 20, currencyType: 1 }).getValue();
    expect(newRate).toEqual({ props: { value: 20, currencyType: 1 } });
  });
  it('Should return default values when null', () => {
    const newRate = Rate.create({ value: null, currencyType: null }).getValue();
    expect(newRate).toEqual({
      props: { value: Rate.defaultRate, currencyType: Rate.defaultCurrency }
    });
  });
  it('should return error', () => {
    const newRate = Rate.create({ value: null, currencyType: 5 });
    expect(newRate);
  });
});

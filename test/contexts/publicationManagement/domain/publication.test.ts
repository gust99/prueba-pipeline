import { CourseId } from '../../../../src/contexts/publicationManagement/domain/courseId';
import { Publication } from '../../../../src/contexts/publicationManagement/domain/publication/publication';
import { PublicationDate } from '../../../../src/contexts/publicationManagement/domain/publication/publicationDate';
import { PublicationDescription } from '../../../../src/contexts/publicationManagement/domain/publication/publicationDescription';
import { PublicationTitle } from '../../../../src/contexts/publicationManagement/domain/publication/publicationTitle';
import { Rate } from '../../../../src/contexts/publicationManagement/domain/rate';
import { Image } from '../../../../src/contexts/publicationManagement/domain/image';

import { UserId } from '../../../../src/contexts/userManagement/domain/userId';
import { Categories } from '../../../../src/contexts/publicationManagement/domain/categories';
import { Languages } from '../../../../src/contexts/publicationManagement/domain/languages';
import LocationOptions from '../../../../src/contexts/publicationManagement/domain/locationOptions';
import { Address } from '../../../../src/contexts/publicationManagement/domain/address';
import { PublicationPackage } from '../../../../src/contexts/publicationManagement/domain/package';
import { ExtraPricing } from '../../../../src/contexts/publicationManagement/domain/extraPricing';
import {
  DiscountType,
  ExtraType,
  FeeType
} from '../../../../src/contexts/publicationManagement/domain/enums';

describe('Publication', () => {
  it('should create a new publication', () => {
    const newPublication = Publication.create({
      title: PublicationTitle.create({
        value: 'publication title'
      }).getValue() as PublicationTitle,
      description: PublicationDescription.create({
        value:
          'descriptiondescriptiondescriptiondescriptiondescriptiondescription'
      }).getValue() as PublicationDescription,
      userId: UserId.create().getValue(),
      courseId: CourseId.create().getValue(),
      date: PublicationDate.create({
        dateTimePublished: new Date()
      }).getValue() as PublicationDate,
      rate: Rate.create({ value: 0, currencyType: 0 }).getValue() as Rate,
      status: 0,
      image: Image.create({ value: '' }).getValue() as Image,
      categories: Categories.create([]),
      languages: Languages.create([]),
      locationOptions: LocationOptions.create({
        online: true,
        travel: false,
        house: false
      }).getValue(),
      address: Address.create({
        value: Address.defaultAddress,
        latitude: Address.defaultLatitude,
        longitude: Address.defaultLongitude
      }).getValue() as Address,
      packageA: PublicationPackage.create({
        value: '0',
        rate: Rate.create({ value: 0, currencyType: 0 }).getValue() as Rate,
        type: 1
      }).getValue() as PublicationPackage,
      packageB: PublicationPackage.create({
        value: '0',
        rate: Rate.create({ value: 0, currencyType: 0 }).getValue() as Rate,
        type: 1
      }).getValue() as PublicationPackage,
      packageC: PublicationPackage.create({
        value: '0',
        rate: Rate.create({ value: 0, currencyType: 0 }).getValue() as Rate,
        type: 1
      }).getValue() as PublicationPackage,
      discount: ExtraPricing.create({
        rate: Rate.create({ value: 0, currencyType: 0 }).getValue() as Rate,
        feeOrDiscountType: DiscountType.None,
        type: ExtraType.Discount
      }).getValue() as ExtraPricing,
      extraFee: ExtraPricing.create({
        rate: Rate.create({ value: 0, currencyType: 0 }).getValue() as Rate,
        feeOrDiscountType: FeeType.None,
        type: ExtraType.ExtraFee
      }).getValue() as ExtraPricing
    }).getValue();
    expect(newPublication);
  });
});

import { PublicationTitle } from '../../../../src/contexts/publicationManagement/domain/publication/publicationTitle';
import { PropertyError } from '../../../../src/contexts/shared/domain/propertyError';

describe('Create Publication Title', () => {
  it('should create a title', () => {
    const newTitle = PublicationTitle.create({
      value: 'publication title'
    }).getValue();
    expect(newTitle).toEqual({
      props: {
        value: 'publication title'
      }
    });
  });
  it('Should return min length error', () => {
    const newTitle = PublicationTitle.create({ value: '' });
    expect((newTitle.error as PropertyError).error).toContain(
      `${PublicationTitle.propName} is not at least 10 chars.`
    );
  });

  it('Should return max length error', () => {
    const newTitle = PublicationTitle.create({
      value:
        'descriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescription'
    });
    expect((newTitle.error as PropertyError).error).toContain(
      `${PublicationTitle.propName} is greater than 50 chars.`
    );
  });

  it('Should return null or undefined error', () => {
    const newTitle = PublicationTitle.create({
      value: null
    });
    expect((newTitle.error as PropertyError).error).toContain(
      `${PublicationTitle.propName} is null or undefined`
    );
  });
});
